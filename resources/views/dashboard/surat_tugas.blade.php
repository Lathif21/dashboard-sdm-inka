@extends('layout.main_content')
{{-- @extends('welcome') --}}
@section('title_page', 'Peraturan ke SDM-an')

@section('pageContent')
    @if (session()->has('message'))
        <div class="alert alert-{{ session('alert') }} alert-has-icon  alert-dismissible fade show mx-1 justify-content-center"
            role="alert">
            <div class="alert-icon"><i class="far fa-lightbulb"></i></div>
            <div class="alert-body">
                <button class="close" data-dismiss="alert" aria-label="close">
                    <span aria-hidden="true">&times;</span>
                    <span class="alert-title">{{ session('title') }}</span>
                </button>
                <span class="alert-text">{{ session('message') }}</span>
            </div>
        </div>
    @endif
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h4>Tabel Peraturan</h4>
                </div>
                <div class="card-body">

                    <div class="float-left">
                        <div class="row">
                            @if (auth()->user() !== null)
                                <div class="col">
                                    <button class="btn btn-primary mr-2" id="detailST" data-toggle="modal"
                                        data-target="#modalST">Unggah
                                        Berkas
                                    </button>
                                </div>
                            @endif
                            <div class="dropdown">
                                <a class="btn btn-primary dropdown-toggle" href="#" role="button" id="dropdownMenuLink"
                                    data-toggle="dropdown" aria-expanded="false">
                                    <i class="fas fa-filter text-white"></i>
                                </a>

                                <div class="dropdown-menu" aria-labelledby="dropdownMenuLink" style="margin-left: 1rem;">
                                    <div class="col">
                                        <form action="{{ route('surat.filter') }}" method="post">
                                            @csrf
                                            <div class="form-group">
                                                <label for="mulai_tgl">Mulai</label>
                                                <input type="date" class="form-control" name="mulai_tgl">
                                            </div>
                                            <div class="form-group">
                                                <label for="sampai_tgl">Sampai</label>
                                                <input type="date" class="form-control" name="sampai_tgl">
                                            </div>
                                            <button type="submit" class="btn btn-warning">Cari</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="clearfix mb-3"></div>

                    <div class="table-responsive">
                        <table class="table table-striped" id="st-table">
                            <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>Nomor Surat</th>
                                    <th>Jenis</th>
                                    <th>Judul</th>
                                    <th>Ditetapkan Tanggal</th>
                                    <th>Berlaku Tanggal</th>
                                    <th>Berakhir Tanggal</th>
                                    <th>Status</th>
                                    <th>Sifat</th>
                                    <th>File</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php $i=1 @endphp
                                @foreach ($surat as $s)
                                    <tr>
                                        <td>
                                            {{ $i++ }}
                                        </td>
                                        <td>
                                            {{ $s->nomor_surat }}
                                            <div class="table-links">
                                                <a href="{{ route('detailSurat', ['id_surat' => $s->id_surat]) }}"
                                                    id="detailS">Detail</a>
                                                @if (auth()->user() !== null)
                                                    <div class="bullet"></div>
                                                    <a href="#" id="updateSurat" data-toggle="modal"
                                                        data-target="#modalSurat" data-id="{{ $s->id_surat }}"
                                                        title="Ubah Kategori Surat">Edit</a>
                                                    <div class="bullet"></div>
                                                    <a href="{{ route('removeSurat', ['id' => $s->id_surat]) }}"
                                                        class="text-danger"
                                                        onclick="return confirm('Data akan terhapus permanen. Apakah akan dilanjutkan?')">
                                                        Hapus
                                                    </a>
                                                @endif
                                            </div>
                                        </td>
                                        <td>
                                            {{ $s->jenis_surat }}, {{ $s->sub_jenis }}
                                        </td>

                                        <td>
                                            {{ $s->judul_surat }}
                                        </td>
                                        <td>
                                            {{ $s->tgl_terbit }}
                                        </td>
                                        <td>
                                            {{ $s->tgl_berlaku }}
                                        </td>
                                        <td>
                                            {{ $s->tgl_berakhir }}
                                        </td>
                                        @if ($s->is_active == 0)
                                            <td>
                                                <div class="badge badge-primary">Aktif</div>
                                            </td>
                                        @elseif ($s->is_active == 1)
                                            <td>
                                                <div class="badge badge-danger">Tidak Aktif</div>
                                            </td>
                                        @else
                                            <td>
                                                <div class="badge badge-warning">Akan Berakhir</div>
                                            </td>
                                        @endif
                                        @if ($s->is_public == 0)
                                            <td>
                                                <div class="badge badge-primary">Publik</div>
                                            </td>
                                        @else
                                            <td>
                                                <div class="badge badge-danger">Privat</div>
                                            </td>
                                        @endif
                                        <td>
                                            <a href="/storage/{{ $s->path_surat }}" make>{{ $s->path_surat }}</a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{-- Modal --}}
    @if (auth()->user() !== null)
        <div id="detailST-part">
            <form action="{{ route('addSurat') }}" method="post" enctype="multipart/form-data">
                @csrf
                <input type="hidden" name="id_surat" value="" id="id_surat">
                <div class=" form-group">
                    <label for="nomor_surat">Nomor Surat</label>
                    <input type="text" name="nomor_surat" id="nomor_surat" class="form-control">
                </div>
                <div class="form-group">
                    <label for="judul_surat">Judul Surat</label>
                    <input type="text" name="judul_surat" id="judul_surat" class="form-control">
                </div>
                <div class="form-group">
                    <label for="id_jenis">Kategori Surat</label>
                    <select name="id_jenis" id="id_jenis_add" class="form-control">
                        @foreach ($jenis_surat as $s)
                            <option value="{{ $s->id_jenis }}">{{ $s->jenis_surat }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label for="id_sub_jenis">Sub Kategori Surat</label>
                    <select name="id_sub_jenis" id="id_sub_jenis_add" class="form-control">
                        @foreach ($sub_jenis as $s)
                            <option value="{{ $s->id_sub_jenis }}">{{ $s->sub_jenis }}</option>
                        @endforeach
                    </select>
                </div>

                <div class="form-group">
                    <label for="tgl_terbit">Ditetapkan Tanggal</label>
                    <input type="date" name="tgl_terbit" id="tgl_terbit" class="form-control">
                </div>
                <div class="form-group">
                    <label for="tgl_berlaku">Berlaku Tanggal</label>
                    <input type="date" name="tgl_berlaku" id="tgl_berlaku" class="form-control">
                </div>
                <div class="form-group">
                    <label for="tgl_berakhir">Berakhir Tanggal</label>
                    <input type="date" name="tgl_berakhir" id="tgl_berakhir" class="form-control">
                </div>
                <div class="form-group">
                    <label for="is_active">Status</label>
                    <select class="form-control" id="is_active" name="is_active">
                        <option value="0">Aktif</option>
                        <option value="1">Tidak Aktif</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="is_public">Sifat</label>
                    <select class="form-control" id="is_public" name="is_public">
                        <option value="0">Publik</option>
                        <option value="1">Privat</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="path_surat">Unggah Berkas</label>
                    <input type="file" name="path_surat" id="path_surat" class="form-control-file">
                </div>
                <input type="hidden" name="updated_by" id="" value="#">
            </form>
        </div>
        {{--  --}}
        <div class="modal fade" id="modalSurat" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Edit Surat</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form action="{{ route('updateSurat') }}" method="post" enctype="multipart/form-data">
                            @csrf
                            <input type="hidden" name="id_surat" value="" id="id_surat">
                            <div class=" form-group">
                                <label for="nomor_surat">Nomor Surat</label>
                                <input type="text" name="nomor_surat" id="nomor_surat" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="judul_surat">Judul Surat</label>
                                <input type="text" name="judul_surat" id="judul_surat" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="id_jenis">Kategori Surat</label>
                                <select name="id_jenis" id="id_jenis" class="form-control">
                                    @foreach ($jenis_surat as $s)
                                        <option value="{{ $s->id_jenis }}">{{ $s->jenis_surat }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="id_sub_jenis">Sub Kategori Surat</label>
                                <select name="id_sub_jenis" id="id_sub_jenis" class="form-control">
                                    @foreach ($sub_jenis as $s)
                                        <option value="{{ $s->id_sub_jenis }}">{{ $s->sub_jenis }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="tgl_terbit">Ditetapkan Tanggal</label>
                                <input type="date" name="tgl_terbit" id="tgl_terbit" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="tgl_berlaku">Berlaku Tanggal</label>
                                <input type="date" name="tgl_berlaku" id="tgl_berlaku" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="tgl_berakhir">Berakhir Tanggal</label>
                                <input type="date" name="tgl_berakhir" id="tgl_berakhir" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="is_active">Status</label>
                                <select class="form-control" id="is_active" name="is_active">
                                    <option value="0">Aktif</option>
                                    <option value="1">Tidak Aktif</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="is_public">Sifat</label>
                                <select class="form-control" id="is_public" name="is_public">
                                    <option value="0">Publik</option>
                                    <option value="1">Privat</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="path_surat_edit">Unggah Berkas</label>
                                <input type="file" name="path_surat_edit" id="path_surat_edit" class="form-control-file">
                            </div>
                            <input type="hidden" name="updated_by" id="" value="#">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save changes</button>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    @endif
@endsection
@section('script')
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.js"></script>
    <script>
        $(document).ready(function() {
            $('#st-table').DataTable({
                'paging': true,
                search: {
                    return: true
                },
                'language': {
                    'search': '<i class="fas fa-search"></i>',
                    searchPlaceholder: 'Tekan Enter untuk mencari',
                    "paginate": {
                        "previous": '<i class="ni ni-bold-left"></i>',
                        'next': '<i class="ni ni-bold-right"></i>',
                    },
                }
            });
        })
    </script>
    <script>
        $("#detailST").fireModal({
            title: 'Unggah Berkas',
            footerClass: 'bg-whitesmoke',
            body: $("#detailST-part"),
            center: true,
            buttons: [{
                    text: 'Close',
                    class: 'btn btn-secondary',
                    handler: function(current_modal) {
                        $.destroyModal(current_modal);
                    }
                },
                {
                    text: 'Save',
                    submit: true,
                    class: 'btn btn-primary',
                    handler: function() {}
                }
            ]
        });
    </script>
    <script>
        $('[data-toggle=modal]').on('click', function() {
            $('#modalSurat form').attr('action', "{{ route('updateSurat') }}")

            var id = $(this).data('id')
            var url = `{{ url('/surat/getSurat/${id}') }}`

            if (!id) {
                return;
            }

            $.ajax({
                method: 'GET',
                url: url,
                dataType: 'JSON',
                success: function(data) {
                    // console.log(data);
                    $('#id_surat').val(data.id_surat)
                    $('#id_jenis').val(data.id_jenis)
                    $('#id_sub_jenis').val(data.id_sub_jenis)
                    $('#nomor_surat').val(data.nomor_surat)
                    $('#judul_surat').val(data.judul_surat)
                    $('#tgl_terbit').val(data.tgl_terbit)
                    $('#tgl_berlaku').val(data.tgl_berlaku)
                    $('#tgl_berakhir').val(data.tgl_berakhir)
                    $('#is_active').val(data.is_active)
                    $('#is_public').val(data.is_public)
                    $('#path_surat').val(data.path_surat)
                }
            })
        })
    </script>
    <script>
        $(document).ready(function() {
            $('#id_jenis').on('change', function() {
                let data = $(this).val();
                pupulate("{{ url('/surat/getSubJenisById') }}", data, "id_sub_jenis");
            });
            $('#id_jenis_add').on('change', function() {
                let data = $(this).val();
                pupulate("{{ url('/surat/getSubJenisById') }}", data, "id_sub_jenis_add");
            });
        })

        function pupulate(ajaxUrl, data, selector) {
            let capitalizeSelector = selector[0].toUpperCase() + selector.substring(1);
            const id = 'id_sub_jenis'
            const nama = 'sub_jenis'
            if (data) {
                $.get(`${ajaxUrl}/${data}`, function(response) {
                    console.log(response);
                    response = JSON.parse(response)
                    $('#' + selector).empty();
                    response.forEach(element => {
                        $('#' + selector).append(
                            `<option value="${element[id]}">${element[nama]}</option>`);
                    });
                })
            }
        }
    </script>
@endsection
