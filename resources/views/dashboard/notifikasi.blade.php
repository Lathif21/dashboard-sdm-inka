<li class="dropdown dropdown-list-toggle"><a href="#" data-toggle="dropdown"
        class="nav-link notification-toggle nav-link-lg"><i class="far fa-bell"></i></a>
    <div class="dropdown-menu dropdown-list dropdown-menu-right">
        <div class="dropdown-header">Notifications
            <div class="float-right">
                <a href="#">Mark All As Read</a>
            </div>
        </div>
        <div class="dropdown-list-content dropdown-list-icons h-100">
            @foreach ($surat as $s)
                @if ($s->is_active == 2)
                    <a href="" class="dropdown-item dropdown-item-unread">
                        <div class="dropdown-item-icon bg-info text-white">
                            <i class="fas fa-bell"></i>
                        </div>
                        <div class="dropdown-item-desc">
                            {{ $s->nomor_surat }} Akan berakhir
                        </div>
                    </a>
                @else
                    {{ null }}
                @endif
            @endforeach
        </div>
        <div class="dropdown-footer text-center">

        </div>
    </div>
</li>
