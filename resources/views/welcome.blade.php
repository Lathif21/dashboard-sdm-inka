@extends('layout.main_content')
@section('title_page', 'Selamat Datang')

@section('pageContent')
    <div class="row">
        <div class="col mb-4">
            <div class="hero text-white hero-bg-image hero-bg-parallax"
                style="background-image: url('assets/img/unsplash/andre-benz-1214056-unsplash.jpg');">
                <div class="hero-inner">
                    <h2>Pengarsipan Peraturan dan Surat Penugasan SDM PT.INKA
                    </h2>
                    <p class="lead">Pengarsipan Peraturan SDM digunakan sebagai tools manajemen untuk memonitoring
                        jumlah, peraturan-peraturan ke SDM-an PT.INKA. Dashboard ini juga dapat menjadi sumber informasi
                        bagi
                        para pegawai untuk mengetahui surat keputusan dan surat tugas yang berlaku di PT.INKA.</p>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-3 col-md-6 col-sm-6 col-12">
            <div class="card card-statistic-1">
                <div class="card-icon bg-primary">
                    <i class="far fa-user"></i>
                </div>
                <div class="card-wrap">
                    <div class="card-header">
                        <h4>Surat Tugas</h4>
                    </div>
                    <div class="card-body">
                        16380
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-6 col-sm-6 col-12">
            <div class="card card-statistic-1">
                <div class="card-icon bg-danger">
                    <i class="fas fa-archway"></i>
                </div>
                <div class="card-wrap">
                    <div class="card-header">
                        <h4>Peraturan Direksi</h4>
                    </div>
                    <div class="card-body">
                        2042
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-6 col-sm-6 col-12">
            <div class="card card-statistic-1">
                <div class="card-icon bg-warning">
                    <i class="far fa-file"></i>
                </div>
                <div class="card-wrap">
                    <div class="card-header">
                        <h4>Kontrak PKWT</h4>
                    </div>
                    <div class="card-body">
                        6201
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-6 col-sm-6 col-12">
            <div class="card card-statistic-1">
                <div class="card-icon bg-success">
                    <i class="far fa-comments"></i>
                </div>
                <div class="card-wrap">
                    <div class="card-header">
                        <h4>Pemberitahuan</h4>
                    </div>
                    <div class="card-body">
                        4475
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
