@extends('layout.main_content')
{{-- @extends('welcome') --}}
@section('title_page', 'Detail Surat')

@section('pageContent')

    <div class="card">
        <div class="card-header">
            <h4>Detail Peraturan</h4>
        </div>
        <div class="card-body">
            <ul class="list-group">
                <li class="list-group-item">Nomor :<span> {{ $detail[0]->nomor_surat }}</span></li>
                <li class="list-group-item">Jenis :<span> {{ $detail[0]->jenis_surat }} </span></li>
                <li class="list-group-item">Judul :<span> {{ $detail[0]->judul_surat }}</span></li>
                <li class="list-group-item">Ditetapkan Tanggal :<span>
                        {{ date('d-M-Y', strtotime($detail[0]->tgl_terbit)) }}</span> </li>
                <li class="list-group-item">Berlaku Tanggal :<span>
                        {{ date('d-M-Y', strtotime($detail[0]->tgl_berlaku)) }}</span> </li>
                <li class="list-group-item">Berakhir Tanggal :<span>
                        {{ date('d-M-Y', strtotime($detail[0]->tgl_berakhir)) }}</span> </li>
                <li class="list-group-item">Status :<span>
                        @if ($detail[0]->status == 0)
                            Aktif
                        @else
                            Tidak Aktif
                        @endif
                    </span> </li>
            </ul>
            <br>
            <div class="buttons">
                <a href="/storage/{{ $detail[0]->path_surat }}" class="btn btn-outline-success" make>Lihat <i
                        class="fas fa-eye"></i></a>
                <a href="/storage/{{ $detail[0]->path_surat }}" class="btn btn-outline-primary" download>Unduh <i
                        class="fas fa-download"></i></a>

            </div>
        </div>
    </div>
    <div class="card">
        <div class="card-header">
            <h4>History</h4>
        </div>
        <div class="card-body">
            @if ($status != null)
                <h6>Diubah Dengan</h6>
                <ul>
                    <li>{{ $status[0]->nomor_surat }}</li>
                </ul>
                <h6>Mencabut</h6>
                <ul>
                    @foreach ($status as $s)
                        <li>{{ $s->nomor_surat_event }}</li>
                    @endforeach
                </ul>
            @endif
        </div>
    </div>


@endsection
