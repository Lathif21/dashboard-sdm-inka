@extends('layout.main_content')
{{-- @extends('welcome') --}}
@section('title_page', 'Detail')

@section('pageContent')
    <div class="card">
        <div class="card-header">
            <h4>Detail Pegawai</h4>
        </div>
        <div class="card-body">
            <ul class="list-group">
                <li class="list-group-item">Nama :<span> {{ $pegawai[0]->nama_pegawai }}</span></li>
                <li class="list-group-item">NIP :<span> {{ $pegawai[0]->NIP }} </span></li>
                <li class="list-group-item">Jabatan :<span> {{ $pegawai[0]->jabatan_pegawai }}</span></li>
            </ul>
            <br>
        </div>
    </div>
    <div class="card">
        <div class="card-header">
            <h4>Aktivitas</h4>
        </div>
        <div class="card-body">
            @if ($semuaAktivitas != null)
                <h6>Aktivitas</h6>
                <table class="table table-bordered table-striped" id="tableDivisi">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama Aktivitas</th>
                            <th>Keterangan</th>
                            <th>Jabatan Dalam Tim</th>
                            <th>Dasar</th>
                            <th>Tanggal terbit</th>
                            <th>Tanggal berlaku</th>
                            <th>Tanggal berakhir</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php $i=1 @endphp
                        @foreach ($semuaAktivitas as $d)
                            <tr>
                                <td>{{ $i++ }}</td>
                                <td>{{ $d->nama_aktivitas }}</td>
                                <td>{{ $d->keterangan }}</td>
                                <td>{{ $d->jabatan_tim }}</td>
                                <td>{{ $d->dasar }}</td>
                                <td>{{ $d->tgl_terbit }}</td>
                                <td>{{ $d->tgl_berlaku }}</td>
                                <td>{{ $d->tgl_berakhir }}</td>
                                <td>
                                    <button class="btn btn-info btn-sm text-white" id="detailDivisi" data-toggle="modal"
                                        data-target="#modalDivisi" data-id="{{ $d->id_detail }}"
                                        title="Lihat detail divisi">
                                        <i class="fas fa-cog"></i>
                                    </button>
                                    <a href="#" class="btn btn-danger btn-sm" onclick="return confirmation()"
                                        title="Hapus divisi">
                                        <i class="fa fa-trash"></i>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            @else
                <h6>Pegawai tidak terafiliasi di dalam aktivitas apapun</h6>
            @endif
        </div>
    </div>


@endsection
