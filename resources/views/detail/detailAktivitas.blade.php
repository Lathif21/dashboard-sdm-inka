@extends('layout.main_content')
@section('title_page', 'Detail')

@section('pageContent')

    @if (session()->has('message'))
        <div class="alert alert-{{ session('alert') }} alert-has-icon  alert-dismissible fade show mx-1 justify-content-center"
            role="alert">
            <div class="alert-icon"><i class="far fa-lightbulb"></i></div>
            <div class="alert-body">
                <button class="close" data-dismiss="alert" aria-label="close">
                    <span aria-hidden="true">&times;</span>
                    <span class="alert-title">{{ session('title') }}</span>
                </button>
                <span class="alert-text">{{ session('message') }}</span>
            </div>
        </div>
    @endif
    <div class="card">
        <div class="card-header">
            <h4>Detail Aktivitas</h4>
        </div>
        <div class="card-body">
            <ul class="list-group">
                <li class="list-group-item">Landasan :<span> {{ $detail[0]->nomor_surat }}</span></li>
                <li class="list-group-item">Nama Aktivitas :<span> {{ $detail[0]->nama_aktivitas }} </span></li>
                <li class="list-group-item">Keterangan :<span> {{ $detail[0]->keterangan }}</span></li>
                <li class="list-group-item">Ditetapkan Tanggal :<span>
                    {{ date('d-M-Y', strtotime($detail[0]->tgl_terbit)) }}</span> </li>
                <li class="list-group-item">Berlaku Tanggal :<span>
                    {{ date('d-M-Y', strtotime($detail[0]->tgl_berlaku)) }}</span> </li>
                <li class="list-group-item">Berakhir Tanggal :<span>
                    {{ date('d-M-Y', strtotime($detail[0]->tgl_berakhir)) }}</span> </li>
            </ul>
            <br>
            <div class="buttons">
                <a href="/tambahDetail/{{ $detail[0]->id_aktivitas }}" type="button" class="btn btn-outline-primary"
                    title="Tambah Peserta">Tambah Peserta <i class="fas fa-plus"></i></a>
            </div>
        </div>
    </div>
    @if ($semuaPegawai == null)
        <h6>Belum Ada Anggota Aktivitas</h6>
    @else
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h4>Peserta</h4>
                    </div>
                    <div class="card-body">
                        <table class="table table-bordered table-striped" id="tableDivisi">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>NIP</th>
                                    <th>Nama</th>
                                    <th>Jabatan</th>
                                    <th>Jabatan Dalam Tim</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php $i=1 @endphp
                                @foreach ($semuaPegawai as $d)
                                    <tr>
                                        <td>{{ $i++ }}</td>
                                        <td>{{ $d->NIP }}</td>
                                        <td>{{ $d->nama_pegawai }}</td>
                                        <td>{{ $d->jabatan_pegawai }}</td>
                                        <td>{{ $d->jabatan_tim }}</td>
                                        <td>
                                            <button class="btn btn-info btn-sm text-white" id="detailDivisi"
                                                data-toggle="modal" data-target="#modalDivisi"
                                                data-id="{{ $d->id_detail }}" title="Lihat detail divisi">
                                                <i class="fas fa-cog"></i>
                                            </button>
                                            <a href="{{ route('deleteDetail', ['id' => $d->id_detail]) }}"
                                                class="btn btn-danger btn-sm" onclick="return confirmation()"
                                                title="Hapus divisi">
                                                <i class="fa fa-trash"></i>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        {{-- Edit --}}
        <div class="modal fade" id="modalDivisi" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Edit Peserta</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form action="{{ route('updateDetail') }}" method="POST" enctype="multipart/form-data">
                            @csrf
                            <input type="hidden" name="id_detail" value="" id="id_detail">
                            <div class="form-group">
                                <label for="id_pegawai">NIP</label>
                                <select name="id_pegawai" id="id_pegawai" class="form-control">
                                    @foreach ($pegawai as $d)
                                        <option value="{{ $d->id_pegawai }}">{{ $d->NIP }} -
                                            {{ $d->nama_pegawai }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                            <input type="hidden" class="form-control" name="id_aktivitas" id="id_aktivitas" value="">
                            <div class="form-group">
                                <label for="jabatan_tim">Jabatan Dalam Tim</label>
                                <input type="text" name="jabatan_tim" id="jabatan_tim" class="form-control">
                            </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save changes</button>
                    </div>
                    </form>
                </div>
            </div>
        </div>

    @endif
@endsection
@section('script')
    <script>
        $(document).ready(function() {
            $('#tableDivisi').DataTable({
                'paging': true,
                // 'info': false,
                // "scrollX": true,
                search: {
                    return: true
                },
                'language': {
                    'search': '<i class="fas fa-search"></i>',
                    searchPlaceholder: 'Tekan Enter untuk mencari',
                    "paginate": {
                        "previous": '<i class="ni ni-bold-left"></i>',
                        'next': '<i class="ni ni-bold-right"></i>',
                    },
                }
            });
        })
    </script>
    <script>
        $('[data-toggle=modal]').on('click', function() {
            $('#exampleModalLabel').html('Form Edit Peserta')

            var id = $(this).data('id')
            var url = `{{ url('/detail/getDetail/${id}') }}`

            if (!id) {
                return;
            }
            $.ajax({
                method: 'GET',
                url: url,
                dataType: 'JSON',
                success: function(data) {
                    $('#id_detail').val(data.id_detail)
                    $('#id_pegawai').val(data.id_pegawai)
                    $('#id_aktivitas').val(data.id_aktivitas)
                    $('#jabatan_tim').val(data.jabatan_tim)
                }
            })
        });
    </script>
    <script>
        $(document).ready(function() {
            $('.select2').select2();
        });
    </script>
@endsection
