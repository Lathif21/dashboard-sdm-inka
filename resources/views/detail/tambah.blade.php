@extends('layout.main_content')
@section('title_page', 'Tambah Data')
@section('pageContent')

    <div class="row">
        <div class="col-12">
            <form action="{{route ('addDetail')}}" method="post">
                @csrf
            <table>
                <thead>
                    <tr>
                        <th>NIP</th>
                        <th>Jabatan Dalam Tim</th>
                        <th><a href="javascript:void(0)" class="btn btn-outline-info addRow">+</a></th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>
                            <select name="id_pegawai[]" id="id_pegawai"
                                class="form-control select2">
                                @foreach ($pegawai as $d)
                                    <option value="{{ $d->id_pegawai }}">{{ $d->NIP }} - {{$d->nama_pegawai}}</option>
                                @endforeach
                            </select>
                        </td>
                        <td>
                            <input type="text" class="form-control" name="jabatan_tim[]" id="jabatan_tim" placeholder="Jabatan Dalam Tim">
                        </td>
                            <input type="hidden" class="form-control" name="id_aktivitas" id="id_aktivitas" value="{{$id_aktivitas}}">
                            {{-- <input type="hidden" class="form-control" name="id_surat" id="id_surat" value="{{$id_surat}}"> --}}
                        <th>
                            <a href="javascript:void(0)" class="btn btn-outline-danger deleteRow">-</a>
                        </th>
                    </tr>
                </tbody>
            </table>
            <button type="submit" class="btn btn-primary">Save changes</button>
        </form>
        </div>
    </div>
    {{-- <div class="modal-footer">
        <button type="reset" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary" id="submit-btn">Save changes</button>
    </div> --}}
@endsection
@section('script')
    <script>
        $('thead').on('click', '.addRow', function() {
            var tr = "<tr>" +
                        "<td>"+
                            "<select name='id_pegawai[]' id='id_pegawai' class='form-control select2'>"+
                                "@foreach ($pegawai as $d)"+
                                    "<option value='{{ $d->id_pegawai }}'>{{ $d->NIP }} - {{$d->nama_pegawai}}</option>"+
                                "@endforeach"+
                            "</select>"+
                        "</td>"+
                        "<td>"+
                            "<input type='text' class='form-control' name='jabatan_tim[]' id='jabatan_tim' placeholder='Jabatan Dalam Tim'>"+
                        "</td>"+
                        "<th><a href='javascript:void(0)' class='btn btn-outline-danger deleteRow'>-</a></th>"+
                    "</tr>"
            $('tbody').append(tr);                
            $('tbody').on('click', '.deleteRow', function() {
                $(this).parent().parent().remove();
            });
            $(document).ready(function(){
            $('.select2').select2();
            });
        });   
    </script>
    <script>
         $(document).ready(function(){
            $('.select2').select2();
            });
    </script>
@endsection
