<div class="navbar-bg"></div>
<nav class="navbar navbar-expand-lg main-navbar">
    <form class="form-inline mr-auto">
        <ul class="navbar-nav mr-3">
            <li><a href="#" data-toggle="sidebar" class="nav-link nav-link-lg"><i class="fas fa-bars"></i></a></li>
            <li><a href="#" data-toggle="search" class="nav-link nav-link-lg d-sm-none"><i class="fas fa-search"></i></a>
            </li>
        </ul>
        <div class="search-element">
        </div>
    </form>
    <ul class="navbar-nav navbar-right">
        @if (auth()->user() !== null)
            @include('dashboard.notifikasi')
        @endif
        <li class="dropdown"><a href="#" data-toggle="dropdown"
                class="nav-link dropdown-toggle nav-link-lg nav-link-user">
                <img alt="image" src="{{ asset('assets/img/avatar/avatar-1.png') }}" class="rounded-circle mr-1">
                @if (auth()->user() == null)
                    <div class="d-sm-none d-lg-inline-block">Hi, Selamat Datang</div>
                @else
                    <div class="d-sm-none d-lg-inline-block">Hi, {{ auth()->user()->nama_user }}</div>
                @endif
            </a>
            @if (auth()->user() == null)
                <div class="dropdown-menu dropdown-menu-right">

                    <form action="{{ route('login') }}" method="get">
                        @csrf
                        <input type="submit" class="dropdown-item has-icon text-primary" value="Log In">
                    </form>
                </div>
            @else
                <div class="dropdown-menu dropdown-menu-right">

                    <form action="{{ route('logout') }}" method="post">
                        @csrf
                        <input type="submit" class="dropdown-item has-icon text-danger" value="Log Out">
                    </form>
                </div>
            @endif
        </li>
    </ul>
</nav>
