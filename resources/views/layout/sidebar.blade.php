<div class="main-sidebar">
    <aside id="sidebar-wrapper">
        <div class="sidebar-brand">
            <a href="/">Dashboard SDM</a>
        </div>
        <div class="sidebar-brand sidebar-brand-sm">
            <a href="/">SDM</a>
        </div>
        <ul class="sidebar-menu">
            <li><a class="nav-link" href="/"><i class="fas fa-fire"></i>
                    <span>Home</span>
                </a>
            </li>
            @if (auth()->user() !== null)
                <li class="nav-item dropdown">
                    <a href="#" class="nav-link has-dropdown"><i class="fas fa-pencil-ruler"></i>
                        <span>Konfigurasi Surat</span></a>
                    <ul class="dropdown-menu">
                        <li><a class="nav-link" href="/jenis">Kategori Surat</a></li>
                        <li><a class="nav-link" href="/subJenis">Sub Kategori Surat</a></li>
                        <li><a class="nav-link" href="/event">Status Surat</a></li>
                    </ul>
                </li>
                <li class="nav-item dropdown">
                    <a href="#" class="nav-link has-dropdown"><i class="far fa-file-alt"></i>
                        <span>Konfigurasi Aktivitas</span></a>
                    <ul class="dropdown-menu">
                        <li><a class="nav-link" href="/pegawai">Pegawai</a></li>
                        <li><a class="nav-link" href="/aktivitas">Aktivitas</a></li>
                    </ul>
                </li>
                <li>
                    <a href="/user" class="nav-link "><i class="far fa-user"></i>
                        <span>Pengguna</span></a>
                </li>
            @endif

            <li><a class="nav-link" href="/surat"><i class="fas fa-tasks"></i>
                    <span>Peraturan ke SDM-an</span>
                </a>
            </li>

        </ul>
    </aside>
</div>
