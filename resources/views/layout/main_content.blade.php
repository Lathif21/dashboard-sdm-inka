<!DOCTYPE html>
<html lang="en">
{{-- Header --}}
@include('layout.header')

<body>
    <div id="app">
        <div class="main-wrapper">
            {{-- Navigation Bar --}}
            @include('layout.navbar')
            {{-- Side bar --}}
            @include('layout.sidebar')

            <!-- Main Content -->
            <div class="main-content">
                <section class="section">
                    <div class="section-header">
                        <h1>@yield('title_page')</h1>
                    </div>
                    <div class="section-body">
                        @yield('pageContent')
                    </div>
                </section>
            </div>
            {{-- Footer --}}
            @include('layout.footer')
        </div>
    </div>
    {{-- Script JavaScript --}}
    @include('layout.scriptJS')
</body>

</html>
