@extends('layout.main_content')
{{-- @extends('welcome') --}}
@section('title_page', 'Status Surat')

@section('pageContent')
    @if (session()->has('message'))
        <div class="alert alert-{{ session('alert') }} alert-has-icon  alert-dismissible fade show mx-1 justify-content-center"
            role="alert">
            <div class="alert-icon"><i class="far fa-lightbulb"></i></div>
            <div class="alert-body">
                <button class="close" data-dismiss="alert" aria-label="close">
                    <span aria-hidden="true">&times;</span>
                    <span class="alert-title">{{ session('title') }}</span>
                </button>
                <span class="alert-text">{{ session('message') }}</span>
            </div>
        </div>
    @endif
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h4>Tabel Status Surat</h4>
                </div>
                <div class="card-body">
                    {{-- <div class="float-right">
                        <form>
                            <div class="input-group">
                                <input type="text" class="form-control" placeholder="Search">
                                <div class="input-group-append">
                                    <button class="btn btn-primary"><i class="fas fa-search"></i></button>
                                </div>
                            </div>
                        </form>
                    </div> --}}

                    <div class="float-left">
                        <button class="btn btn-primary" id="detailJenis" data-toggle="modal" data-target="#Jenis">Tambah
                            Status
                        </button>
                    </div>

                    <div class="clearfix mb-3"></div>
                    <div class="table-responsive">
                        <table class="table table-striped" id="jenisTable">
                            <thead>
                                <tr>
                                    <th scope="col" class="sort">No</th>
                                    {{-- <th scope="col" class="sort">ID Jenis</th> --}}
                                    <th scope="col" class="sort">Surat Baru</th>
                                    <th scope="col" class="sort">Keterangan</th>
                                    <th scope="col" class="sort">Surat Tergantikan</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php $i=1 @endphp
                                @foreach ($event as $j)
                                    <tr>
                                        <th scope="row">
                                            {{ $i++ }}
                                        </th>
                                        {{-- <td>
                                            {{ $j->id_jenis }}
                                        </td> --}}
                                        <td>
                                            {{ $j->nomor_surat }}
                                            <div class="table-links">
                                                <div class="bullet"></div>
                                                <a href="#" id="editEvent" data-toggle="modal" data-target="#modalJenis"
                                                    data-id="{{ $j->id_event }}" title="Ubah Status Surat">Edit</a>
                                                <div class="bullet"></div>
                                                <a href="{{ route('removeEvent', ['id' => $j->id_event]) }}"
                                                    class="text-danger"
                                                    onclick="return confirm('Data akan terhapus permanen. Apakah akan dilanjutkan?')">
                                                    Hapus
                                                </a>
                                            </div>
                                        </td>
                                        <td>
                                            {{ $j->event }}
                                        </td>
                                        <td>
                                            {{ $j->nomor_surat_event }}
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{-- Modal Jenis --}}
    <div id="Jenis" aria-labelledby="labelModal">
        <form action="{{ route('addEvent') }}" method="post">
            @csrf
            <input type="hidden" name="id_event" value="" id="id_event">
            <div class="form-group">
                <label for="id_surat">Surat Baru</label>
                <select name="id_surat" id="id_surat" class="form-control">
                    <option value="" selected disabled>Pilih</option>
                    @foreach ($surat as $s)
                        <option value="{{ $s->id_surat }}">{{ $s->nomor_surat }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label for="event">Keterangan</label>
                <input type="text" name="event" id="event" class="form-control">
            </div>
            <div class="form-group">
                <label for="surat_object">Surat Tergantikan</label>
                <select name="surat_object" id="surat_object" class="form-control">
                    <option value="" selected disabled>Pilih</option>
                    @foreach ($surat as $s)
                        <option value="{{ $s->id_surat }}">{{ $s->nomor_surat }}</option>
                    @endforeach
                </select>
            </div>
        </form>
    </div>
    <div class="modal fade" id="modalJenis" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Edit Status Surat</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="{{ route('editEvent') }}" method="post">
                        @csrf
                        <input type="hidden" name="id_event" value="" id="id_event">
                        <div class="form-group">
                            <label for="id_surat">Surat Baru</label>
                            <select name="id_surat" id="id_surat" class="form-control">
                                @foreach ($surat as $s)
                                    <option value="{{ $s->id_surat }}">{{ $s->nomor_surat }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="event">Keterangan</label>
                            <input type="text" name="event" id="event" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="surat_object">Surat Tergantikan</label>
                            <select name="surat_object" id="surat_object" class="form-control">
                                @foreach ($surat as $s)
                                    <option value="{{ $s->id_surat }}">{{ $s->nomor_surat }}</option>
                                @endforeach
                            </select>
                        </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
                </form>
            </div>
        </div>
    @endsection
    @section('script')
        <script>
            $(document).ready(function() {
                $('#jenisTable').DataTable({
                    'paging': true,
                    search: {
                        return: true
                    },
                    'language': {
                        'search': '<i class="fas fa-search"></i>',
                        searchPlaceholder: 'Tekan Enter untuk mencari',
                        "paginate": {
                            "previous": '<i class="ni ni-bold-left"></i>',
                            'next': '<i class="ni ni-bold-right"></i>',
                        },
                    }
                });
            })
        </script>
        <script>
            $("#detailJenis").fireModal({
                title: 'Tambah Status Surat',
                footerClass: 'bg-whitesmoke',
                body: $("#Jenis"),
                center: true,
                buttons: [{
                        text: 'Close',
                        class: 'btn btn-secondary',
                        handler: function(current_modal) {
                            $.destroyModal(current_modal);
                        }
                    },
                    {
                        text: 'Save',
                        submit: true,
                        class: 'btn btn-primary',
                        handler: function() {

                        }
                    }
                ]
            });
        </script>
        <script>
            $('[data-toggle=modal]').on('click', function() {
                // $('#Jenis form').attr('action', "{{ route('editEvent') }}")

                var id = $(this).data('id')

                var url = `{{ url('/event/getStatus/${id}') }}`

                if (!id) {
                    return;
                }

                $.ajax({
                    method: 'GET',
                    url: url,
                    dataType: 'JSON',
                    success: function(data) {
                        $('#id_event').val(data.id_event)
                        $('#id_surat').val(data.id_surat)
                        $('#event').val(data.event)
                        $('#surat_object').val(data.surat_object)
                    }
                })
            })
        </script>
    @endsection
