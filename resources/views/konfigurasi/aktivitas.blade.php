@extends('layout.main_content')
{{-- @extends('welcome') --}}
@section('title_page', 'Aktivitas')

@section('pageContent')
    @if (session()->has('message'))
        <div class="alert alert-{{ session('alert') }} alert-has-icon  alert-dismissible fade show mx-1 justify-content-center"
            role="alert">
            <div class="alert-icon"><i class="far fa-lightbulb"></i></div>
            <div class="alert-body">
                <button class="close" data-dismiss="alert" aria-label="close">
                    <span aria-hidden="true">&times;</span>
                    <span class="alert-title">{{ session('title') }}</span>
                </button>
                <span class="alert-text">{{ session('message') }}</span>
            </div>
        </div>
    @endif
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h4>Tabel Aktivitas</h4>
                </div>
                <div class="card-body">

                    <div class="float-left">
                        <div class="row">
                            <div class="col-12">
                            <button class="btn btn-primary mr-3" id="detailST" data-toggle="modal"
                                data-target="#modalST">Tambah Data
                            </button>
                        </div>
                        </div>
                    </div>

                    <div class="clearfix mb-3"></div>

                    <div class="table-responsive">
                        <table class="table table-striped" id="st-table">
                            <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>Landasan</th>
                                    <th>Nama Aktivitas</th>
                                    <th>Keterangan</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php $i=1 @endphp
                                @foreach ($aktivitas as $s)
                                    <tr>
                                        <td>
                                            {{ $i++ }}
                                        </td>
                                        <td>
                                            {{ $s->nomor_surat }}
                                            <div class="table-links">
                                                <a href="{{ route('detailAktivitas', ['id_aktivitas' => $s->id_aktivitas]) }}"
                                                    id="detailS">Detail</a>
                                                <div class="bullet"></div>
                                                <a href="#" id="updateSurat" data-toggle="modal" data-target="#modalSurat"
                                                    data-id="{{ $s->id_aktivitas }}" title="Ubah Kategori Surat">Edit</a>
                                                <div class="bullet"></div>
                                                <a href="{{ route('removeAktivitas', ['id' => $s->id_aktivitas]) }}"
                                                    class="text-danger"
                                                    onclick="return confirm('Data akan terhapus permanen. Apakah akan dilanjutkan?')">
                                                    Hapus
                                                </a>
                                            </div>
                                        </td>
                                        <td>
                                            {{ $s->nama_aktivitas }}
                                        </td>
                                        <td>
                                            {{ $s->keterangan }}
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{-- Modal --}}
    <div id="detailST-part">
        <form action="{{ route('addAktivitas') }}" method="post" enctype="multipart/form-data">
            @csrf
            <input type="hidden" name="id_aktivitas" value="" id="id_aktivitas">
            <div class="form-group">
                <label for="id_surat">Landasan</label>
                <select name="id_surat" id="id_surat" class="form-control">
                    <option value=""selected disabled>Pilih</option>
                    @foreach ($surat as $s)
                        <option value="{{ $s->id_surat }}">{{ $s->nomor_surat }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label for="nama_aktivitas">Nama Aktivitas</label>
                <input type="text" name="nama_aktivitas" id="nama_aktivitas" class="form-control">
            </div>
            <div class="form-group">
                <label for="keterangan">Keterangan</label>
                <input type="text" name="keterangan" id="keterangan" class="form-control">
            </div>
            <input type="hidden" name="updated_by" id="" value="#">
        </form>
    </div>
    {{--  --}}
    <div class="modal fade" id="modalSurat" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Edit Data Aktivitas</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="{{ route('updateAktivitas') }}" method="post" enctype="multipart/form-data">
                        @csrf
                        <input type="hidden" name="id_aktivitas" value="" id="id_aktivitas">
                        <div class="form-group">
                            <label for="id_surat">Landasan</label>
                            <select name="id_surat" id="id_surat" class="form-control">
                                @foreach ($surat as $s)
                                    <option value="{{ $s->id_surat }}">{{ $s->nomor_surat }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="nama_aktivitas">Nama Aktivitas</label>
                            <input type="text" name="nama_aktivitas" id="nama_aktivitas" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="keterangan">Keterangan</label>
                            <input type="text" name="keterangan" id="keterangan" class="form-control">
                        </div>
                        <input type="hidden" name="updated_by" id="" value="#">
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Save changes</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    @endsection
    @section('script')
        <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.js"></script>
        <script>
            $(document).ready(function() {
                $('#st-table').DataTable({
                    'paging': true,
                    search: {
                        return: true
                    },
                    'language': {
                        'search': '<i class="fas fa-search"></i>',
                        searchPlaceholder: 'Tekan Enter untuk mencari',
                        "paginate": {
                            "previous": '<i class="ni ni-bold-left"></i>',
                            'next': '<i class="ni ni-bold-right"></i>',
                        },
                    }
                });
            })
        </script>
        <script>
            $("#detailST").fireModal({
                title: 'Tambah Data Aktivitas',
                footerClass: 'bg-whitesmoke',
                body: $("#detailST-part"),
                center: true,
                buttons: [{
                        text: 'Close',
                        class: 'btn btn-secondary',
                        handler: function(current_modal) {
                            $.destroyModal(current_modal);
                        }
                    },
                    {
                        text: 'Save',
                        submit: true,
                        class: 'btn btn-primary',
                        handler: function() {}
                    }
                ]
            });
        </script>
        <script>
            $('[data-toggle=modal]').on('click', function() {

                var id = $(this).data('id')
                var url = `{{ url('/aktivitas/getAktivitas/${id}') }}`

                if (!id) {
                    return;
                }

                $.ajax({
                    method: 'GET',
                    url: url,
                    dataType: 'JSON',
                    success: function(data) {
                        // console.log(data);
                        $('#id_aktivitas').val(data.id_aktivitas)
                        $('#id_surat').val(data.id_surat)
                        $('#nama_aktivitas').val(data.nama_aktivitas)
                        $('#keterangan').val(data.keterangan)
                    }
                })
            })
        </script>
    @endsection
