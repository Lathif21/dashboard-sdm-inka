@extends('layout.main_content')
{{-- @extends('welcome') --}}
@section('title_page', 'Sub Kategori Surat')

@section('pageContent')
    @if (session()->has('message'))
        <div class="alert alert-{{ session('alert') }} alert-has-icon  alert-dismissible fade show mx-1 justify-content-center"
            role="alert">
            <div class="alert-icon"><i class="far fa-lightbulb"></i></div>
            <div class="alert-body">
                <button class="close" data-dismiss="alert" aria-label="close">
                    <span aria-hidden="true">&times;</span>
                    <span class="alert-title">{{ session('title') }}</span>
                </button>
                <span class="alert-text">{{ session('message') }}</span>
            </div>
        </div>
    @endif
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h4>Tabel Sub Kategori Surat</h4>
                </div>
                <div class="card-body">
                    <div class="float-left">
                        <button class="btn btn-primary" id="detailSub" data-toggle="modal" data-target="#Sub">Tambah
                            Sub Kategori
                        </button>
                    </div>

                    <div class="clearfix mb-3"></div>
                    <div class="table-responsive">
                        <table class="table table-striped" id="subTable">
                            <thead>
                                <tr>
                                    <th scope="col" class="sort">No</th>
                                    {{-- <th scope="col" class="sort">ID Jenis</th> --}}
                                    <th scope="col" class="sort">Sub Kategori Surat</th>
                                    <th scope="col" class="sort">Kategori Surat</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php $i=1 @endphp
                                @foreach ($sub_jenis_surat as $sj)
                                    <tr>
                                        <th scope="row">
                                            {{ $i++ }}
                                        </th>
                                        <td>
                                            {{ $sj->sub_jenis }}
                                            <div class="table-links">
                                                <div class="bullet"></div>
                                                <a href="#" id="editSub" data-toggle="modal" data-target="#modalSub"
                                                    data-id="{{ $sj->id_sub_jenis }}" title="Ubah Kategori Surat">Edit</a>
                                                <div class="bullet"></div>
                                                <a href="{{ route('removeSub', ['id' => $sj->id_sub_jenis]) }}"
                                                    class="text-danger"
                                                    onclick="return confirm('Data akan terhapus permanen. Apakah akan dilanjutkan?')">
                                                    Hapus
                                                </a>
                                            </div>
                                        </td>
                                        @foreach ($jenis_surat as $j)
                                            @if ($j->id_jenis == $sj->id_jenis)
                                                <td>
                                                    {{ $j->jenis_surat }}
                                                </td>
                                            @endif
                                        @endforeach
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{-- Modal Sub Jenis --}}
    <div id="Sub" aria-labelledby="labelModal">
        <form action="{{ route('addSub') }}" method="post">
            @csrf
            <input type="hidden" name="id_sub_jenis" value="" id="id_sub_jenis">
            <div class="form-group">
                <label for="sub_jenis">Sub Kategori Surat</label>
                <input type="text" name="sub_jenis" id="sub_jenis" class="form-control">
            </div>
            <div class="form-group">
                <label for="id_jenis">Kategori Surat</label>
                <select name="id_jenis" id="id_jenis" class="form-control">
                    @foreach ($jenis_surat as $s)
                        <option value="{{ $s->id_jenis }}">{{ $s->jenis_surat }}</option>
                    @endforeach
                </select>
            </div>
            <input type="hidden" name="updated_by" id="" value="#">
        </form>
    </div>
    {{--  --}}
    <div class="modal fade" id="modalSub" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Edit Sub Kategori Surat</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="{{ route('editSub') }}" method="post">
                        @csrf
                        <input type="hidden" name="id_sub_jenis" value="" id="id_sub_jenis">
                        <div class="form-group">
                            <label for="sub_jenis">Sub Kategori Surat</label>
                            <input type="text" name="sub_jenis" id="sub_jenis" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="id_jenis">Kategori Surat</label>
                            <select name="id_jenis" id="id_jenis" class="form-control">
                                @foreach ($jenis_surat as $s)
                                    <option value="{{ $s->id_jenis }}">{{ $s->jenis_surat }}</option>
                                @endforeach
                            </select>
                        </div>
                        <input type="hidden" name="updated_by" id="" value="#">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script>
        $(document).ready(function() {
            $('#subTable').DataTable({
                'paging': true,
                // 'info': false,
                // "scrollX": true,
                search: {
                    return: true
                },
                'language': {
                    'search': '<i class="fas fa-search"></i>',
                    searchPlaceholder: 'Tekan Enter untuk mencari',
                    "paginate": {
                        "previous": '<i class="ni ni-bold-left"></i>',
                        'next': '<i class="ni ni-bold-right"></i>',
                    },
                }
            });
        })
    </script>
    <script>
        $("#detailSub").fireModal({
            title: 'Tambah Sub Kategori',
            footerClass: 'bg-whitesmoke',
            body: $("#Sub"),
            center: true,
            buttons: [{
                    text: 'Close',
                    class: 'btn btn-secondary',
                    handler: function(current_modal) {
                        $.destroyModal(current_modal);
                    }
                },
                {
                    text: 'Save',
                    submit: true,
                    class: 'btn btn-primary',
                    handler: function() {

                    }
                }
            ]
        });
    </script>
    <script>
        $('[data-toggle=modal]').on('click', function() {
            $('#Sub form').attr('action', "{{ route('editSub') }}")

            var id = $(this).data('id')

            var url = `{{ url('/subJenis/getSub/${id}') }}`

            if (!id) {
                return;
            }

            $.ajax({
                method: 'GET',
                url: url,
                dataType: 'JSON',
                success: function(data) {
                    // console.log(data);
                    $('#id_sub_jenis').val(data.id_sub_jenis)
                    $('#id_jenis').val(data.id_jenis)
                    $('#sub_jenis').val(data.sub_jenis)
                }
            })
        })
    </script>
@endsection
