@extends('layout.main_content')

@section('pageTitle', 'Pengguna')

@section('pageContent')
    @if (session()->has('message'))
        <div class="alert alert-{{ session('alert') }} alert-has-icon  alert-dismissible fade show mx-1 justify-content-center"
            role="alert">
            <div class="alert-icon"><i class="far fa-lightbulb"></i></div>
            <div class="alert-body">
                <button class="close" data-dismiss="alert" aria-label="close">
                    <span aria-hidden="true">&times;</span>
                    <span class="alert-title">{{ session('title') }}</span>
                </button>
                <span class="alert-text">{{ session('message') }}</span>
            </div>
        </div>
    @endif
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h4>Tabel Pengguna</h4>
                </div>
                <div class="card-body">

                    <div class="float-left">
                        <div class="row">
                            <button class="btn btn-primary mr-3" id="detailST" data-toggle="modal"
                                data-target="#modalST">Tambah Akun
                            </button>
                        </div>
                    </div>

                    <div class="clearfix mb-3"></div>

                    <div class="table-responsive">
                        <table class="table table-striped" id="st-table">
                            <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>Nama</th>
                                    <th>Username</th>
                                    <th>Role</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php $i=1 @endphp
                                @foreach ($user as $s)
                                    <tr>
                                        <td>
                                            {{ $i++ }}
                                        </td>
                                        <td>
                                            {{ $s->nama_user }}
                                        </td>
                                        <td>
                                            {{ $s->username }}
                                        </td>
                                        <td>
                                            {{ $s->role }}
                                        </td>
                                        <td>
                                            <button class="btn btn-info btn-sm text-white" id="updateSurat"
                                                data-toggle="modal" data-target="#modalSurat" data-id="{{ $s->id_user }}"
                                                title="Ubah Detail Pengguna">Edit
                                                <i class="fas fa-cog"></i>
                                            </button>
                                            <button href="{{ route('removeUser', ['id' => $s->id_user]) }}"
                                                class="btn btn-danger btn-sm text-white"
                                                onclick="return confirm('Data akan terhapus permanen. Apakah akan dilanjutkan?')">
                                                Hapus
                                                <i class="fa fa-trash"></i>
                                            </button>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{-- Modal --}}
    <div id="detailST-part">
        <form action="{{ route('addUser') }}" method="post" enctype="multipart/form-data">
            @csrf
            <input type="hidden" name="id_user" value="" id="id_user">
            <div class=" form-group">
                <label for="username">Username</label>
                <input type="text" name="username" id="username" class="form-control">
            </div>
            <div class="form-group">
                <label for="nama_user">Nama Pengguna</label>
                <input type="text" name="nama_user" id="nama_user" class="form-control">
            </div>
            <div class="form-group">
                <label for="role">Role</label>
                <select class="form-control" id="role" name="role">
                    <option value="Admin">Admin</option>
                </select>
            </div>
            <div id="sandi">
                <div class="form-group">
                    <label for="password">Password</label>
                    <div class="input-group">
                        <input type="password" name="password" id="password"
                            class="form-control @error('password') is-invalid @enderror" placeholder="Sandi saat ini">
                        <div class="input-group-append" style="cursor: pointer;">
                            <span class="input-group-text" id="eye"><i class="far fa-eye"></i></span>
                        </div>
                    </div>
                    @error('password')
                        <small style="color: red;">{{ $message }}</small>
                    @enderror
                </div>
            </div>
        </form>
    </div>
    {{--  --}}
    <div class="modal fade" id="modalSurat" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Update Pengguna</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="{{ route('updateUser') }}" method="post" enctype="multipart/form-data">
                        @csrf
                        <input type="hidden" name="id_user" value="" id="id_user">
                        <div class=" form-group">
                            <label for="username">Username</label>
                            <input type="text" name="username" id="username" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="nama_user">Nama Pengguna</label>
                            <input type="text" name="nama_user" id="nama_user" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="role">Role</label>
                            <select class="form-control" id="role" name="role">
                                <option value="Admin">Admin</option>
                            </select>
                        </div>
                        <div id="sandi">
                            <div class="form-group">
                                <label for="password">Password</label>
                                <div class="input-group">
                                    <input type="password" name="password" id="password"
                                        class="form-control @error('password') is-invalid @enderror"
                                        placeholder="Sandi Baru">
                                    <div class="input-group-append" style="cursor: pointer;">
                                        <span class="input-group-text" id="eye"><i class="far fa-eye"></i></span>
                                    </div>
                                </div>
                                @error('password')
                                    <small style="color: red;">{{ $message }}</small>
                                @enderror
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Save changes</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    @endsection

    @section('script')
        <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.js"></script>
        <script>
            $(document).ready(function() {
                $('#st-table').DataTable({
                    'paging': true,
                    search: {
                        return: true
                    },
                    'language': {
                        'search': '<i class="fas fa-search"></i>',
                        searchPlaceholder: 'Tekan Enter untuk mencari',
                        "paginate": {
                            "previous": '<i class="ni ni-bold-left"></i>',
                            'next': '<i class="ni ni-bold-right"></i>',
                        },
                    }
                });
            })
        </script>
        <script>
            $("#detailST").fireModal({
                title: 'Tambah Penggunaa Baru',
                footerClass: 'bg-whitesmoke',
                body: $("#detailST-part"),
                center: true,
                buttons: [{
                        text: 'Close',
                        class: 'btn btn-secondary',
                        handler: function(current_modal) {
                            $.destroyModal(current_modal);
                        }
                    },
                    {
                        text: 'Save',
                        submit: true,
                        class: 'btn btn-primary',
                        handler: function() {}
                    }
                ]
            });
        </script>
        <script>
            $('#eye').on('click', function(event) {
                event.preventDefault();
                if ($('#password').attr('type') == 'password') {
                    $('#password').attr('type', 'text')
                    $(this).html('<i class="far fa-eye-slash"></i>')
                } else if ($('#password').attr('type') == 'text') {
                    $('#password').attr('type', 'password')
                    $('#eye').html('<i class="far fa-eye"></i>')
                }
            })
        </script>
        <script>
            $('[data-toggle=modal]').on('click', function() {
                var id = $(this).data('id')
                var url = `{{ url('/user/getPengguna/${id}') }}`

                if (!id) {
                    return;
                }

                $.ajax({
                    method: 'GET',
                    url: url,
                    dataType: 'JSON',
                    success: function(data) {
                        // console.log(data);
                        $('#id_user').val(data.id_user)
                        $('#nama_user').val(data.nama_user)
                        $('#username').val(data.username)
                        $('#role').val(data.role)
                    }
                })
            })
        </script>
    @endsection
