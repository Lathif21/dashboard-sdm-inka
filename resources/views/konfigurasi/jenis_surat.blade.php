@extends('layout.main_content')
{{-- @extends('welcome') --}}
@section('title_page', 'Kategori Surat')

@section('pageContent')
    @if (session()->has('message'))
        <div class="alert alert-{{ session('alert') }} alert-has-icon  alert-dismissible fade show mx-1 justify-content-center"
            role="alert">
            <div class="alert-icon"><i class="far fa-lightbulb"></i></div>
            <div class="alert-body">
                <button class="close" data-dismiss="alert" aria-label="close">
                    <span aria-hidden="true">&times;</span>
                    <span class="alert-title">{{ session('title') }}</span>
                </button>
                <span class="alert-text">{{ session('message') }}</span>
            </div>
        </div>
    @endif
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h4>Tabel Kategori Surat</h4>
                </div>
                <div class="card-body">
                    {{-- <div class="float-right">
                        <form>
                            <div class="input-group">
                                <input type="text" class="form-control" placeholder="Search">
                                <div class="input-group-append">
                                    <button class="btn btn-primary"><i class="fas fa-search"></i></button>
                                </div>
                            </div>
                        </form>
                    </div> --}}

                    <div class="float-left">
                        <button class="btn btn-primary" id="detailJenis" data-toggle="modal" data-target="#Jenis">Tambah
                            Kategori
                        </button>
                    </div>

                    <div class="clearfix mb-3"></div>
                    <div class="table-responsive">
                        <table class="table table-striped" id="jenisTable">
                            <thead>
                                <tr>
                                    <th scope="col" class="sort">No</th>
                                    {{-- <th scope="col" class="sort">ID Jenis</th> --}}
                                    <th scope="col" class="sort">Kategori Surat</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php $i=1 @endphp
                                @foreach ($jenis_surat as $j)
                                    <tr>
                                        <th scope="row">
                                            {{ $i++ }}
                                        </th>
                                        {{-- <td>
                                            {{ $j->id_jenis }}
                                        </td> --}}
                                        <td>
                                            {{ $j->jenis_surat }}
                                            <div class="table-links">
                                                <div class="bullet"></div>
                                                <a href="#" id="editJenis" data-toggle="modal" data-target="#modalJenis"
                                                    data-id="{{ $j->id_jenis }}" title="Ubah Kategori Surat">Edit</a>
                                                <div class="bullet"></div>
                                                <a href="{{ route('removeJenis', ['id' => $j->id_jenis]) }}"
                                                    class="text-danger"
                                                    onclick="return confirm('Data akan terhapus permanen. Apakah akan dilanjutkan?')">
                                                    Hapus
                                                </a>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    {{-- <div class="float-right">
                        <nav>
                            <ul class="pagination">
                                <li class="page-item disabled">
                                    <a class="page-link" href="#" aria-label="Previous">
                                        <span aria-hidden="true">«</span>
                                        <span class="sr-only">Previous</span>
                                    </a>
                                </li>
                                <li class="page-item active">
                                    <a class="page-link" href="#">1</a>
                                </li>
                                <li class="page-item">
                                    <a class="page-link" href="#">2</a>
                                </li>
                                <li class="page-item">
                                    <a class="page-link" href="#">3</a>
                                </li>
                                <li class="page-item">
                                    <a class="page-link" href="#" aria-label="Next">
                                        <span aria-hidden="true">»</span>
                                        <span class="sr-only">Next</span>
                                    </a>
                                </li>
                            </ul>
                        </nav>
                    </div> --}}
                </div>
            </div>
        </div>
    </div>
    {{-- Modal Jenis --}}
    <div id="Jenis" aria-labelledby="labelModal">
        <form action="{{ route('addJenis') }}" method="post">
            @csrf
            <input type="hidden" name="id_jenis" value="" id="id_jenis">
            <div class="form-group">
                <label for="jenis_surat">Kategori Surat</label>
                <input type="text" name="jenis_surat" id="jenis_surat" class="form-control">
            </div>
            <input type="hidden" name="updated_by" id="" value="#">
        </form>
    </div>
    <div class="modal fade" id="modalJenis" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Edit Kategori Surat</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="{{ route('editJenis') }}" method="post">
                        @csrf
                        <input type="hidden" name="id_jenis" value="" id="id_jenis">
                        <div class="form-group">
                            <label for="jenis_surat">Kategori Surat</label>
                            <input type="text" name="jenis_surat" id="jenis_surat" class="form-control">
                        </div>
                        <input type="hidden" name="updated_by" id="" value="#">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script>
        $(document).ready(function() {
            $('#jenisTable').DataTable({
                'paging': true,
                // 'info': false,
                // "scrollX": true,
                search: {
                    return: true
                },
                'language': {
                    'search': '<i class="fas fa-search"></i>',
                    searchPlaceholder: 'Tekan Enter untuk mencari',
                    "paginate": {
                        "previous": '<i class="ni ni-bold-left"></i>',
                        'next': '<i class="ni ni-bold-right"></i>',
                    },
                }
            });
        })
    </script>
    <script>
        $("#detailJenis").fireModal({
            title: 'Tambah Jenis Surat',
            footerClass: 'bg-whitesmoke',
            body: $("#Jenis"),
            center: true,
            buttons: [{
                    text: 'Close',
                    class: 'btn btn-secondary',
                    handler: function(current_modal) {
                        $.destroyModal(current_modal);
                    }
                },
                {
                    text: 'Save',
                    submit: true,
                    class: 'btn btn-primary',
                    handler: function() {

                    }
                }
            ]
        });
    </script>
    <script>
        $('[data-toggle=modal]').on('click', function() {
            $('#Jenis form').attr('action', "{{ route('editJenis') }}")

            var id = $(this).data('id')

            var url = `{{ url('/jenis/getJenis/${id}') }}`

            if (!id) {
                return;
            }

            $.ajax({
                method: 'GET',
                url: url,
                dataType: 'JSON',
                success: function(data) {
                    console.log(data);
                    $('#id_jenis').val(data.id_jenis)
                    $('#jenis_surat').val(data.jenis_surat)
                }
            })
        })
    </script>
@endsection
