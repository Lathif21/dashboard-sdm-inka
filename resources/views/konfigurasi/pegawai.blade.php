@extends('layout.main_content')
{{-- @extends('welcome') --}}
@section('title_page', 'Pegawai Yang Bertugas')

@section('pageContent')
    @if (session()->has('message'))
        <div class="alert alert-{{ session('alert') }} alert-has-icon  alert-dismissible fade show mx-1 justify-content-center"
            role="alert">
            <div class="alert-icon"><i class="far fa-lightbulb"></i></div>
            <div class="alert-body">
                <button class="close" data-dismiss="alert" aria-label="close">
                    <span aria-hidden="true">&times;</span>
                    <span class="alert-title">{{ session('title') }}</span>
                </button>
                <span class="alert-text">{{ session('message') }}</span>
            </div>
        </div>
    @endif
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h4>Tabel Pegawai</h4>
                </div>
                <div class="card-body">
                    {{-- <div class="float-right">
                        <form>
                            <div class="input-group">
                                <input type="text" class="form-control" placeholder="Search">
                                <div class="input-group-append">
                                    <button class="btn btn-primary"><i class="fas fa-search"></i></button>
                                </div>
                            </div>
                        </form>
                    </div> --}}
                    <div class="float-left">
                        <div class="row">
                            <div class="col">
                                <button class="btn btn-primary" id="detailJenis" data-toggle="modal"
                                    data-target="#Jenis">Tambah
                                    Data
                                </button>
                                <button class="btn btn-outline-success" title="Impor Data Pegawai" id="imporPegawai"
                                    data-toggle="modal" data-target="#Import">
                                    <span class="d-none d-md-block"><i class="fas fa-upload mr-3"></i>Import</span>
                                </button>
                            </div>
                        </div>
                    </div>

                    <div class="clearfix mb-3"></div>
                    <div class="table-responsive">
                        <table class="table table-striped" id="jenisTable">
                            <thead>
                                <tr>
                                    <th scope="col" class="sort">No</th>
                                    <th scope="col" class="sort">Nama Pegawai</th>
                                    <th scope="col" class="sort">NIP</th>
                                    <th scope="col" class="sort">Jabatan</th>
                                    <th scope="col" class="sort">Divisi</th>
                                    <th scope="col" class="sort">Departemen</th>
                                    <th scope="col" class="sort">Status Pegawai</th>
                                    {{-- <th scope="col" class="sort">Jumlah Aktivitas</th> --}}
                                </tr>
                            </thead>
                            <tbody>
                                @php $i=1 @endphp
                                @foreach ($pegawai as $j)
                                    <tr>
                                        <th scope="row">
                                            {{ $i++ }}
                                        </th>
                                        <td>
                                            {{ $j->nama_pegawai }}
                                            <div class="table-links">
                                                <a href="{{ route('detailPegawai', ['id_pegawai' => $j->id_pegawai]) }}"
                                                    id="detailS">Detail</a>
                                                <div class="bullet"></div>
                                                {{-- <a href="#" id="editJenis" data-toggle="modal" data-target="#modalJenis"
                                                    data-id="{{ $j->id_pegawai }}" title="Ubah Kategori Surat">Edit</a>
                                                <div class="bullet"></div>
                                                <a href="{{ route('removePegawai', ['id' => $j->id_pegawai]) }}"
                                                    class="text-danger"
                                                    onclick="return confirm('Data akan terhapus permanen. Apakah akan dilanjutkan?')">
                                                    Hapus
                                                </a> --}}
                                            </div>
                                        </td>
                                        <td>
                                            {{ $j->NIP }}
                                        </td>
                                        <td>
                                            {{ $j->jabatan_pegawai }}
                                        </td>
                                        {{-- <td>
                                            {{ $j->total_aktivitas }}
                                        </td> --}}
                                        <td>
                                            {{ $j->Divisi }}
                                        </td>
                                        <td>
                                            {{ $j->Departemen }}
                                        </td>
                                        <td>
                                            {{ $j->Status }}
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{-- Modal Jenis --}}
    <div id="Jenis" aria-labelledby="labelModal">
        <form action="{{ route('addPegawai') }}" method="post">
            @csrf
            <input type="hidden" name="id_pegawai" value="" id="id_pegawai">
            <div class="form-group">
                <label for="nama_pegawai">Nama Pegawai</label>
                <input type="text" name="nama_pegawai" id="nama_pegawai" class="form-control">
            </div>
            <div class="form-group">
                <label for="NIP">NIP</label>
                <input type="number" name="NIP" id="NIP" class="form-control">
            </div>
            <div class="form-group">
                <label for="jabatan_pegawai">Jabatan Pegawai</label>
                <input type="text" name="jabatan_pegawai" id="jabatan_pegawai" class="form-control">
            </div>
            <input type="hidden" name="updated_by" id="" value="#">
        </form>
    </div>
    {{-- Import --}}
    <div id="Import" aria-labelledby="importModal">
        <form action="{{ route('file-import') }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label for="file">Unggah Berkas</label>
                <input type="file" name="file" id="file" class="form-control-file">
            </div>
        </form>
    </div>
    {{--  --}}
    <div class="modal fade" id="modalJenis" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-body">
                    <form action="{{ route('editPegawai') }}" method="POST">
                        @csrf
                        <section id="edit-form">
                            <input type="hidden" name="id_pegawai" value="" id="id_pegawai">
                            <div class="form-group">
                                <label for="nama_pegawai">Nama Pegawai</label>
                                <input type="text" name="nama_pegawai" id="nama_pegawai" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="NIP">NIP</label>
                                <input type="number" name="NIP" id="NIP" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="jabatan_pegawai">Jabatan</label>
                                <input type="text" name="jabatan_pegawai" id="jabatan_pegawai" class="form-control">
                            </div>
                            <input type="hidden" name="updated_by" id="" value="#">
                        </section>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script>
        $(document).ready(function() {
            $('#jenisTable').DataTable({
                'paging': true,
                // 'info': false,
                // "scrollX": true,
                search: {
                    return: true
                },
                'language': {
                    'search': '<i class="fas fa-search"></i>',
                    searchPlaceholder: 'Tekan Enter untuk mencari',
                    "paginate": {
                        "previous": '<i class="ni ni-bold-left"></i>',
                        'next': '<i class="ni ni-bold-right"></i>',
                    },
                }
            });
        })
    </script>
    <script>
        $("#detailJenis").fireModal({
            title: 'Tambah Data Pegawai',
            footerClass: 'bg-whitesmoke',
            body: $("#Jenis"),
            center: true,
            buttons: [{
                    text: 'Close',
                    class: 'btn btn-secondary',
                    handler: function(current_modal) {
                        $.destroyModal(current_modal);
                    }
                },
                {
                    text: 'Save',
                    submit: true,
                    class: 'btn btn-primary',
                    handler: function() {

                    }
                }
            ]
        });
    </script>
    <script>
        $("#imporPegawai").fireModal({
            title: 'Import Data Pegawai',
            footerClass: 'bg-whitesmoke',
            body: $("#Import"),
            center: true,
            buttons: [{
                    text: 'Close',
                    class: 'btn btn-secondary',
                    handler: function(current_modal) {
                        $.destroyModal(current_modal);
                    }
                },
                {
                    text: 'Save',
                    submit: true,
                    class: 'btn btn-primary',
                    handler: function() {

                    }
                }
            ]
        });
    </script>
    <script>
        $('[data-toggle=modal]').on('click', function() {
            $('#exampleModalLabel').html('Form Edit Data')
            $('#import-section').hide()
            $('#edit-form').show()
            $('#modalJenis form').attr('action', "{{ route('editPegawai') }}")
            var id = $(this).data('id')
            var url = `{{ url('/pegawai/getPegawai/${id}') }}`

            if (!id) {
                return;
            }

            $.ajax({
                method: 'GET',
                url: url,
                dataType: 'JSON',
                success: function(data) {
                    $('#id_pegawai').val(data.id_pegawai)
                    $('#nama_pegawai').val(data.nama_pegawai)
                    $('#NIP').val(data.NIP)
                    $('#jabatan_pegawai').val(data.jabatan_pegawai)
                }
            })
        });
    </script>
@endsection
