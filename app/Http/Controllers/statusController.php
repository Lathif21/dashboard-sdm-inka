<?php

namespace App\Http\Controllers;

use App\Models\EventSurat;
use App\Models\Surat;
use Illuminate\Http\Request;
use SuratEvent;

class statusController extends Controller
{
    public function index()
    {
        $event = EventSurat::select(
            'id_event',
            'm_surat.id_surat',
            'm_surat.nomor_surat',
            'event',
            'surat_object',
            'em.id_surat as id_surat_event',
            'em.nomor_surat as nomor_surat_event'

        )
            ->join('m_surat', 'surat_event.id_surat', 'm_surat.id_surat')
            ->join('m_surat as em', 'surat_event.surat_object', 'em.id_surat')
            ->get();
        $surat = Surat::select('id_surat', 'nomor_surat', 'is_active')
            ->get();

        return view('konfigurasi.event', [
            'event' => $event,
            'surat' => $surat
        ]);
    }
    public function store(Request $request)
    {
        $request->validate([
            'id_surat' => 'required',
            'event' => 'required',
            'surat_object' => 'required'
        ]);

        $suratLama = Surat::findOrFail($request->surat_object);
        $suratLama->is_active = 1;
        $suratLama->save();

        if (EventSurat::create($request->all())) {
            return back()->with([
                'message' => 'Data Berhasil Ditambahkan',
                'alert' => 'success'
            ]);
        }
        return back()->with([
            'message' => 'Data Gagal Ditambahkan',
            'alert' => 'danger'
        ]);
    }
    public function getStatus($id)
    {
        $event = EventSurat::findOrFail($id);
        echo json_encode($event);
    }
    public function update(Request $request)
    {
        $request->validate([
            // 'id_event' => 'required',
            'id_surat' => 'required',
            'event' => 'required',
            'surat_object' => 'required'
        ]);
        $update = EventSurat::findOrFail($request->id_event);
        if ($update->update($request->all())) {
            return back()->with([
                'message' => 'Data Berhasil Di Update',
                'alert' => 'success'
            ]);
        }
        return back()->with([
            'message' => 'Data Gagal Di Update',
            'alert' => 'danger'
        ]);
    }
    public function delete($id)
    {
        $delete = EventSurat::findOrFail($id);
        if ($delete->delete()) {
            return back()->with([
                'message' => 'Data Berhasil Dihapus',
                'alert' => 'success'
            ]);
        }
        return back()->with([
            'message' => 'Data Gagal Dihapus',
            'alert' => 'danger'
        ]);
    }
}
