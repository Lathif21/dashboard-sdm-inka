<?php

namespace App\Http\Controllers;

use App\Models\JenisSurat;
use App\Models\Surat;
use Illuminate\Http\Request;

class jenisController extends Controller
{
    public function index()
    {
        $jenis_surat = JenisSurat::all();
        $surat = Surat::select('nomor_surat', 'is_active')
            ->get();
        return view('konfigurasi.jenis_surat', [
            'jenis_surat' => $jenis_surat,
            'surat' => $surat
        ]);
    }
    public function getJenis($id)
    {
        $jenis_surat = JenisSurat::findOrFail($id);
        echo json_encode($jenis_surat);
    }
    public function store(Request $request)
    {
        $request->validate([
            'jenis_surat' => 'required'
        ]);
        if (JenisSurat::create($request->all())) {
            return back()->with([
                'message' => 'Data berhasil ditambahkan',
                'alert' => 'success'
            ]);
        }
        return back()->with([
            'message' => 'Data gagal ditambahkan',
            'alert' => 'danger'
        ]);
    }
    public function update(Request $request)
    {
        $request->validate([
            'jenis_surat' => 'required'
        ]);
        $update = JenisSurat::findOrFail($request->id_jenis);
        $update->jenis_surat = $request->jenis_surat;
        if ($update->save()) {
            return back()->with([
                'message' => 'Data berhasil diubah',
                'alert' => 'success'
            ]);
        }
        return back()->with([
            'message' => 'Data gagal diubah',
            'alert' => 'danger'
        ]);
    }
    public function delete($id)
    {
        $delete = JenisSurat::findOrFail($id);
        if ($delete->delete()) {
            return back()->with([
                'message' => 'Data Berhasil Dihapus',
                'alert' => 'success'
            ]);
        }
        return back()->with([
            'message' => 'Data Gagal Dihapus',
            'alert' => 'danger'
        ]);
    }
}
