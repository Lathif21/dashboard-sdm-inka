<?php

namespace App\Http\Controllers;

use App\Models\Aktivitas;
use App\Models\Surat;
use Illuminate\Http\Request;

class aktivitasController extends Controller
{
    public function index()
    {
        $aktivitas = Aktivitas::select(
            'id_aktivitas',
            'm_surat.id_surat',
            'm_surat.nomor_surat',
            'nama_aktivitas',
            'keterangan',
        )
            ->join('m_surat', 'aktivitas.id_surat', 'm_surat.id_surat')
            ->get();
        $surat = Surat::select('is_active', 'nomor_surat', 'id_surat')
            ->get();
        return view('konfigurasi.aktivitas', compact('aktivitas', 'surat'));
    }

    public function store(Request $request)
    {
        if (Aktivitas::create($request->all())) {
            return back()->with([
                'message' => 'Data berhasil ditambahkan',
                'alert' => 'success'
            ]);
        }
        return back()->with([
            'message' => 'Data gagal ditambahkan',
            'alert' => 'danger'
        ]);
    }

    public function update(Request $request)
    {
        $update = Aktivitas::findOrFail($request->id_aktivitas);
        $update->id_surat = $request->id_surat;
        $update->nama_aktivitas = $request->nama_aktivitas;
        $update->keterangan = $request->keterangan;
        if ($update->save()) {
            return back()->with([
                'message' => 'Data berhasil diubah',
                'alert' => 'success'
            ]);
        }
        return back()->with([
            'message' => 'Data gagal diubah',
            'alert' => 'danger'
        ]);
    }

    public function getAktivitas($id)
    {
        $aktivitas = Aktivitas::findOrFail($id);
        return response()->json($aktivitas);
    }

    public function delete($id)
    {
        $delete = Aktivitas::findOrFail($id);
        if ($delete->delete()) {
            return back()->with([
                'message' => 'Data Berhasil Dihapus',
                'alert' => 'success'
            ]);
        }
        return back()->with([
            'message' => 'Data Gagal Dihapus',
            'alert' => 'danger'
        ]);
    }
}
