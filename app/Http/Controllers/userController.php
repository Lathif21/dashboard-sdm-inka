<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Surat;
use Illuminate\Support\Facades\Hash;

class userController extends Controller
{
    public function index()
    {
        $user = User::all();
        $surat = Surat::select('nomor_surat', 'is_active')->get();
        return view('konfigurasi.user', ['user' => $user, 'surat' => $surat]);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * 
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama_user' => 'required|max:255',
            'username' => 'required|unique:user,username',
            'password' => 'required|min:5',
            'role' => 'required'
        ]);

        $newUser = new User();
        $newUser->nama_user = $request->nama_user;
        $newUser->username = $request->username;
        $newUser->password = Hash::make($request->password);
        $newUser->role = $request->role;

        if ($newUser->save()) {
            return back()->with(
                [
                    'message' => 'Data Berhasil Ditambahkan',
                    'alert' => 'success'
                ]
            );
        }

        return back()->with(
            [
                'message' => 'Data Gagal Ditambahkan.',
                'alert' => 'danger'
            ]
        );
    }

    /**
     * @param \Illuminate\Http\Request $request
     * 
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request)
    {
        $updatedUser = User::findOrFail($request->id_user);

        if ($request->passwordOld && !Hash::check($request->passwordOld, $updatedUser->password)) {
            return back()->with([
                'message' => 'Sandi Anda Tidak Sesuai!',
                'alert' => 'warning'
            ]);
        }

        $updatedUser->nama_user = $request->nama_user;
        $updatedUser->username = $request->username;
        $updatedUser->role = $request->role;
        if ($request->passwordNew) {
            $updatedUser->password = Hash::make($request->passwordNew);
        }

        if ($updatedUser->save()) {
            return back()->with(
                [
                    'message' => 'Data Berhasil Diubah.',
                    'alert' => 'success'
                ]
            );
        }

        return back()->with(
            [
                'message' => 'Data Gagal Diubah.',
                'alert' => 'danger'
            ]
        );
    }

    /**
     * @param mixed $id
     * 
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete($id)
    {
        $deletedUser = User::findOrFail($id);

        if ($deletedUser->delete()) {
            return back()->with(
                [
                    'message' => 'Data berhasil dihapus.',
                    'alert' => 'success'
                ]
            );
        }

        return back()->with(
            [
                'message' => 'Data gagal dihapus.',
                'alert' => 'danger'
            ]
        );
    }

    /**
     * @param mixed $id
     * 
     * @return void
     */
    public function getPenggunaById($id)
    {
        $user = User::findOrFail($id);
        echo json_encode($user);
    }
}
