<?php

namespace App\Http\Controllers;

use App\Models\JenisSurat;
use App\Models\SubJenisSurat;
use App\Models\Surat;
use Illuminate\Http\Request;

class subJenisController extends Controller
{
    public function index()
    {
        $sub_jenis_surat = SubJenisSurat::select('id_sub_jenis', 'id_jenis', 'sub_jenis')
            ->get();
        $jenis_surat = JenisSurat::all();
        $surat = Surat::select('nomor_surat', 'is_active')
            ->get();
        return view('konfigurasi.subJenis_surat', [
            'sub_jenis_surat' => $sub_jenis_surat,
            'jenis_surat' => $jenis_surat,
            'surat' => $surat
        ]);
    }
    public function getSub($id)
    {
        $sub_jenis_surat = SubJenisSurat::findOrFail($id);
        echo json_encode($sub_jenis_surat);
    }
    public function store(Request $request)
    {
        $request->validate([
            'id_jenis' => 'required',
            'sub_jenis' => 'required'
        ]);
        if (SubJenisSurat::create($request->all())) {
            return back()->with([
                'message' => 'Data Berhasil Ditambahkan',
                'alert' => 'success'
            ]);
        }
        return back() - with([
            'message' => 'Data Gagal Ditambahkan',
            'alert' => 'danger'
        ]);
    }
    public function update(Request $request)
    {
        $request->validate([
            'id_jenis' => 'required',
            'sub_jenis' => 'required'
        ]);
        $update = SubJenisSurat::findOrFail($request->id_sub_jenis);
        $update->id_jenis = $request->id_jenis;
        $update->sub_jenis = $request->sub_jenis;
        if ($update->save()) {
            return back()->with([
                'message' => 'Data Berhasil Diupdate',
                'alert' => 'success'
            ]);
        }
        return back()->with([
            'message' => 'Data Gagal Diupdate',
            'alert' => 'danger'
        ]);
    }
    public function delete($id)
    {
        $delete = SubJenisSurat::findOrFail($id);
        if ($delete->delete()) {
            return back()->with([
                'message' => 'Data Berhasil Dihapus',
                'alert' => 'success'
            ]);
        }
        return back()->with([
            'message' => 'Data Gagal Dihapus',
            'alert' => 'danger'
        ]);
    }
}
