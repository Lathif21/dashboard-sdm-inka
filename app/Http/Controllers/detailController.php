<?php

namespace App\Http\Controllers;

use App\Models\Aktivitas;
use App\Models\EventSurat;
use App\Models\JenisSurat;
use App\Models\Surat;
use App\Models\Pegawai;
use App\Models\DetailAktivitas;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class detailController extends Controller
{
    public function index($id_surat)
    {
        $detail = Surat::select(
            'nomor_surat',
            'judul_surat',
            'jenis_surat',
            'id_sub_jenis',
            'tgl_terbit',
            'tgl_berlaku',
            'tgl_berakhir',
            'is_active',
            'path_surat'
        )
            ->join('m_jenis_surat', 'm_surat.id_jenis', 'm_jenis_surat.id_jenis')
            ->where('id_surat', $id_surat)
            ->get();

        $status = null;
        $surat = Surat::select('nomor_surat', 'is_active')->get();

        if (EventSurat::where('surat_event.id_surat', $id_surat)->count() > 0) {
            $status = EventSurat::select(
                'm_surat.id_surat',
                'm_surat.nomor_surat',
                'event',
                'surat_object',
                'em.id_surat as id_surat_event',
                'em.nomor_surat as nomor_surat_event'
            )
                ->join('m_surat', 'surat_event.id_surat', 'm_surat.id_surat')
                ->join('m_surat as em', 'surat_event.surat_object', 'em.id_surat')
                ->where('surat_event.id_surat', $id_surat)
                ->get();
        } elseif (EventSurat::where('surat_event.surat_object', $id_surat)->count() > 0) {
            $status = EventSurat::select(
                'm_surat.id_surat',
                'm_surat.nomor_surat',
                'event',
                'surat_object',
                'em.id_surat as id_surat_event',
                'em.nomor_surat as nomor_surat_event'
            )
                ->join('m_surat', 'surat_event.id_surat', 'm_surat.id_surat')
                ->join('m_surat as em', 'surat_event.surat_object', 'em.id_surat')
                ->where('surat_event.surat_object', $id_surat)
                ->get();
        }
        return view('detail.detailST', [
            'detail' => $detail,
            'status' => $status,
            'surat' => $surat
        ]);
    }
    public function homeIndex()
    {
        $totalSurat = Surat::count();

        $stCount = Surat::where('id_jenis', 2)
            ->count();
        $seCount = Surat::where('id_jenis', 3)
            ->count();
        $pkwtCount = Surat::where('id_jenis', 6)
            ->count();
        $pemberitahuanCount = Surat::where('id_jenis', 4)
            ->count();
        $surat = Surat::select('nomor_surat', 'is_active')->get();
        return view('home', compact('stCount', 'seCount', 'pkwtCount', 'pemberitahuanCount', 'surat'));
    }

    public function pegawaiIndex($id_pegawai)
    {
        $pegawai = Pegawai::select(
            'nama_pegawai',
            'NIP',
            'jabatan_pegawai',
        )
            ->where('id_pegawai', $id_pegawai)
            ->get();

        $detailAktivitasPegawai = null;

        $surat = Surat::select('nomor_surat', 'is_active')->get();

        if (DetailAktivitas::where('detail_aktivitas.id_pegawai', $id_pegawai)->count() > 0) {
            /*$semuaAktivitas = DetailAktivitas::select(
                'detail_aktivitas.id_aktivitas',
                'aktivitas.nama_aktivitas',
                'aktivitas.id_aktivitas',
                'aktivitas.nomor_surat',
                'id_pegawai',
                'jabatan_tim',
                'keterangan',
            )
                ->join('m_surat', 'aktivitas.id_surat', 'm_surat.id_surat')
                ->leftJoin('aktivitas', 'aktivitas.id_aktivitas', 'detail_aktivitas.id_aktivitas')
                ->where('id_pegawai', $id_pegawai)
                ->get();*/
            $detailAktivitasPegawai = DetailAktivitas::select(
                'detail_aktivitas.id_aktivitas',
                'aktivitas.nama_aktivitas',
                'aktivitas.keterangan',
                'jabatan_tim',
                'm_surat.nomor_surat as dasar',
                'm_surat.tgl_terbit',
                'm_surat.tgl_berlaku',
                'm_surat.tgl_berakhir',
            )
                ->join('aktivitas', 'detail_aktivitas.id_aktivitas', 'aktivitas.id_aktivitas')
                ->join('m_surat', 'aktivitas.id_surat', 'm_surat.id_surat')
                ->where('detail_aktivitas.id_pegawai', $id_pegawai)
                ->get();
        }
        // dd($pegawai, $detailAktivitasPegawai, $surat);
        return view('detail.detailPegawai', [
            'pegawai' => $pegawai,
            'surat' => $surat,
            'semuaAktivitas' => $detailAktivitasPegawai,
        ]);
    }
    public function detailAktivitas($id_aktivitas)
    {
        $detail = Aktivitas::select(
            'id_aktivitas',
            'm_surat.id_surat',
            'm_surat.nomor_surat',
            'nama_aktivitas',
            'keterangan',
            'm_surat.tgl_terbit',
            'm_surat.tgl_berlaku',
            'm_surat.tgl_berakhir',
        )
            ->join('m_surat', 'aktivitas.id_surat', 'm_surat.id_surat')
            ->where('id_aktivitas', $id_aktivitas)
            ->get();
        $surat = Surat::select('nomor_surat', 'is_active')->get();
        $semuaPegawai = null;
        $pegawai = Pegawai::select(
            'id_pegawai',
            'nama_pegawai',
            'NIP',
            'jabatan_pegawai',
        )
            ->get();
        if (DetailAktivitas::where('detail_aktivitas.id_aktivitas', $id_aktivitas)->count() > 0) {
            $semuaPegawai = DetailAktivitas::select(
                'detail_aktivitas.id_detail',
                'detail_aktivitas.id_aktivitas',
                'detail_aktivitas.id_pegawai',
                'detail_aktivitas.jabatan_tim',
                'pegawai.nama_pegawai',
                'pegawai.NIP',
                'pegawai.jabatan_pegawai',
                'm_surat.nomor_surat',
            )
                ->join('pegawai', 'pegawai.id_pegawai', 'detail_aktivitas.id_pegawai')
                ->join('aktivitas', 'aktivitas.id_aktivitas', 'detail_aktivitas.id_aktivitas')
                ->join('m_surat', 'm_surat.id_surat', 'aktivitas.id_surat')
                ->where('detail_aktivitas.id_aktivitas', $id_aktivitas)
                ->get();
        }

        return view('detail.detailAktivitas', [
            'detail' => $detail,
            'surat' => $surat,
            'semuaPegawai' => $semuaPegawai,
            'pegawai' => $pegawai,

        ]);
    }
    public function store(Request $request)
    {
        // dd($request->all());
        $id_pegawai = $request->id_pegawai;
        $jabatan_tim = $request->jabatan_tim;
        for ($i = 0; $i < count($id_pegawai); $i++) {
            $data = [
                'id_pegawai' => $id_pegawai[$i],
                'jabatan_tim' => $jabatan_tim[$i],
                'id_aktivitas' => $request->id_aktivitas
            ];
            DB::table('detail_aktivitas')->insert($data);
        }
        return redirect()->route('detailAktivitas', $request->id_aktivitas);
    }
    public function tambah($id_aktivitas)
    {
        $pegawai = Pegawai::select(
            'id_pegawai',
            'nama_pegawai',
            'NIP',
            'jabatan_pegawai',
        )
            ->get();
        $surat = Surat::select('nomor_surat', 'is_active')->get();

        return view('detail.tambah', [
            'pegawai' => $pegawai,
            'surat' => $surat,
            'id_aktivitas' => $id_aktivitas
        ]);
    }
    // {
    //     $pegawai = Pegawai::select('id_pegawai', 'nama_pegawai', 'jabatan_pegawai', 'NIP')->get();
    //     $surat = Surat::select('nomor_surat', 'is_active')->get();
    //     return view('detail.tambah', compact('pegawai', 'surat', 'id_aktivitas', 'id_surat'));
    // }
    public function update(Request $request)
    {
        $update = DetailAktivitas::findOrFail($request->id_detail);
        $update->id_pegawai = $request->id_pegawai;
        $update->jabatan_tim = $request->jabatan_tim;
        $update->id_aktivitas = $request->id_aktivitas;
        if ($update->save()) {
            return back()->with([
                'message' => 'Data berhasil diubah',
                'alert' => 'success'
            ]);
        }
        return back()->with([
            'message' => 'Data gagal diubah',
            'alert' => 'danger'
        ]);
    }
    public function delete($id)
    {
        $delete = DetailAktivitas::findOrFail($id);
        if ($delete->delete()) {
            return back()->with([
                'message' => 'Data berhasil dihapus',
                'alert' => 'success'
            ]);
        }
        return back()->with([
            'message' => 'Data gagal dihapus',
            'alert' => 'danger'
        ]);
    }
    public function getDetail($id_detail)
    {
        $detail = DetailAktivitas::findOrFail($id_detail);
        echo json_encode($detail);
    }
}
