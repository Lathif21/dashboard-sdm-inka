<?php

namespace App\Http\Controllers;

use App\Models\Surat;
use App\Models\JenisSurat;
use App\Models\SubJenisSurat;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;

class m_suratController extends Controller
{
    public function index()
    {
        $surat = Surat::select(
            'id_surat',
            'nomor_surat',
            'judul_surat',
            'tgl_terbit',
            'tgl_berlaku',
            'tgl_berakhir',
            'jenis_surat',
            'sub_jenis',
            'is_active',
            'is_public',
            'path_surat'
        )
            ->join('m_jenis_surat', 'm_surat.id_jenis', 'm_jenis_surat.id_jenis')
            ->leftJoin('m_sub_jenis_surat', 'm_surat.id_sub_jenis', 'm_sub_jenis_surat.id_sub_jenis')
            ->get();

        if (Auth::user() === null) {
            $surat = $surat->where('is_public', 0);
        }

        $jenis_surat = JenisSurat::all();
        $sub_jenis = SubJenisSurat::all();
        return view(
            'dashboard.surat_tugas',
            [
                'surat' => $surat,
                'jenis_surat' => $jenis_surat,
                'sub_jenis' => $sub_jenis
            ]
        );
    }
    public function getSurat($id)
    {
        $surat = Surat::findOrFail($id);
        echo json_encode($surat);
    }

    public function getSubJenisById($jenis_surat)
    {
        $data = SubJenisSurat::select('id_sub_jenis', 'id_jenis', 'sub_jenis')
            ->where('m_sub_jenis_surat.id_jenis', $jenis_surat)
            ->get();
        echo json_encode($data);
        // return dd($data);
    }
    public function store(Request $request)
    {
        if ($request->hasFile('path_surat')) {
            $file = $request->file('path_surat');
            $fileName = $file->getClientOriginalName();
            $path = Carbon::now()->format('Y') . '/';

            $data = [
                'id_surat',
                'nomor_surat' => $request->nomor_surat,
                'judul_surat' => $request->judul_surat,
                'tgl_terbit' => $request->tgl_terbit,
                'tgl_berlaku' => $request->tgl_berlaku,
                'tgl_berakhir' => $request->tgl_berakhir,
                'id_jenis' => $request->id_jenis,
                'id_sub_jenis' => $request->id_sub_jenis,
                'is_active' => $request->is_active,
                'is_public' => $request->is_public,
                'path_surat' => $path . $fileName
            ];

            Storage::putFileAs(
                'public/' . $path,
                $file,
                $fileName
            );

            Surat::create($data);
            return back()->with([
                'message' => 'Data Berhasil Ditambahkan',
                'alert' => 'success'
            ]);
        }
        return back() - with([
            'message' => 'Data Gagal Ditambahkan',
            'alert' => 'danger'
        ]);
    }
    public function delete($id)
    {
        $delete = Surat::findOrFail($id);
        if ($delete->delete()) {
            return back()->with([
                'message' => 'Data Berhasil Dihapus',
                'alert' => 'success'
            ]);
        }
        return back()->with([
            'message' => 'Data Gagal Dihapus',
            'alert' => 'danger'
        ]);
    }
    public function update(Request $request)
    {
        $update = Surat::findOrFail($request->id_surat);

        if ($request->hasFile('path_surat_edit')) {
            $file = $request->file('path_surat_edit');
            $fileName = $file->getClientOriginalName();
            $path = Carbon::now()->format('Y') . '/';
            Storage::delete('public/' . $update->path_surat);

            Storage::putFileAs(
                'public/' . $path,
                $file,
                $fileName
            );

            // update path_surat
            $update->path_surat = $path . $fileName;
        }

        $update->nomor_surat = $request->nomor_surat;
        $update->judul_surat = $request->judul_surat;
        $update->id_jenis = $request->id_jenis;
        $update->id_sub_jenis = $request->id_sub_jenis;
        $update->tgl_terbit = $request->tgl_terbit;
        $update->tgl_berlaku = $request->tgl_berlaku;
        $update->tgl_berakhir = $request->tgl_berakhir;
        $update->is_active = $request->is_active;
        $update->is_public = $request->is_public;
        // dd($update);

        if ($update->save()) {
            return back()->with([
                'message' => 'Data Berhasil Di Update',
                'alert' => 'success'
            ]);
        } else {
            return back()->with([
                'message' => 'Data Gagal Di Update',
                'alert' => 'danger'
            ]);
        }
    }

    public function filter(Request $req)
    {
        try {

            $surat = Surat::select(
                'nomor_surat',
                'judul_surat',
                'tgl_terbit',
                'tgl_berlaku',
                'tgl_berakhir',
                'jenis_surat',
                'sub_jenis',
                'is_active',
                'is_public',
                'path_surat'
            )
                ->join('m_jenis_surat', 'm_surat.id_jenis', 'm_jenis_surat.id_jenis')
                ->leftJoin('m_sub_jenis_surat', 'm_surat.id_sub_jenis', 'm_sub_jenis_surat.id_sub_jenis')
                ->whereBetween('tgl_berakhir', [$req->mulai_tgl, $req->sampai_tgl])
                ->get();
            $jenis_surat = JenisSurat::all();
            $sub_jenis = SubJenisSurat::all();
            return view('dashboard.surat_tugas', ['surat' => $surat, 'jenis_surat' => $jenis_surat, 'sub_jenis' => $sub_jenis]);
        } catch (\Throwable $th) {
            throw $th;
        }
    }
}
