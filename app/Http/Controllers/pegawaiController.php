<?php

namespace App\Http\Controllers;

use App\Imports\pegawaiImport;
use Illuminate\Http\Request;
use App\Models\Pegawai;
use App\Models\Surat;
use App\Models\DetailAktivitas;
use Maatwebsite\Excel\Facades\Excel;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;

class pegawaiController extends Controller
{

    public function index()
    {

        $pegawai = Pegawai::select('*')
            ->get();
        // dd($pegawai);
        // DB::raw('count(detail_aktivitas.id_pegawai) AS total_aktivitas')
        // ->leftJoin('detail_aktivitas', 'pegawai.id_pegawai', 'detail_aktivitas.id_pegawai')
        // ->groupBy('id_pegawai', 'nama_pegawai', 'nip', 'jabatan_pegawai')
        // ->get();

        $surat = Surat::select('is_active', 'nomor_surat')
            ->get();
        return view('konfigurasi.pegawai', compact(
            'pegawai',
            'surat',
            // 'semuaAktivitas'
        ));
    }
    public function fileImport(Request $request)
    {
        $this->validate($request, [
            'file' => 'required|mimes:csv,xls,xlsx'
        ]);
        $file = $request->file('file');
        $fileName = $file->getClientOriginalName();
        Storage::putFileAs('public', $file, $fileName);
        if (($test = new pegawaiImport)->import($file)) {
            // dd($test);
            return back()->with([
                'message' => 'Data berhasil diImport',
                'alert' => 'success'
            ]);
        }
        return back()->with([
            'message' => 'Data gagal diImport',
            'alert' => 'danger'
        ]);
    }
    public function update(Request $request)
    {
        $update = Pegawai::findOrFail($request->id_pegawai);
        $update->nama_pegawai = $request->nama_pegawai;
        $update->NIP = $request->NIP;
        $update->jabatan_pegawai = $request->jabatan_pegawai;
        if ($update->save()) {
            return back()->with([
                'message' => 'Data berhasil diubah',
                'alert' => 'success'
            ]);
        }
        return back()->with([
            'message' => 'Data gagal diubah',
            'alert' => 'danger'
        ]);
    }
    public function delete($id)
    {
        $delete = Pegawai::findOrFail($id);
        if ($delete->delete()) {
            return back()->with([
                'message' => 'Data Berhasil Dihapus',
                'alert' => 'success'
            ]);
        }
        return back()->with([
            'message' => 'Data Gagal Dihapus',
            'alert' => 'danger'
        ]);
    }
    public function store(Request $request)
    {
        if (Pegawai::create($request->all())) {
            return back()->with([
                'message' => 'Data berhasil ditambahkan',
                'alert' => 'success'
            ]);
        }
        return back()->with([
            'message' => 'Data gagal ditambahkan',
            'alert' => 'danger'
        ]);
    }
    public function getPegawai($id)
    {
        $pegawai = Pegawai::findOrFail($id);
        echo json_encode($pegawai);
    }
}
