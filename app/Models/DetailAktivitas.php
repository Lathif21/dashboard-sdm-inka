<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DetailAktivitas extends Model
{
    use HasFactory;

    protected $table = 'detail_aktivitas';

    protected $primaryKey = 'id_detail';

    public $timestamps = false;

    protected $fillable = [
        'id_detail',
        'id_aktivitas',
        'id_pegawai',
        'jabatan_tim',
    ];
}
