<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DateTimeInterface;

class Surat extends Model
{
    use HasFactory;

    protected $table = 'm_surat';

    public $timestamps = false;

    protected $primaryKey = 'id_surat';

    protected $fillable = [
        'id_surat',
        'id_jenis',
        'id_sub_jenis',
        'nomor_surat',
        'judul_surat',
        'tgl_terbit',
        'tgl_berlaku',
        'tgl_berakhir',
        'is_active',
        'is_public',
        'path_surat',
        'deleted_at'
    ];

    // protected function serializeDate(DateTimeInterface $date)
    // {
    //     return $date->format('Y-m-d H:i:s');
    // }
}
