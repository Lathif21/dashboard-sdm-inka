<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SubJenisSurat extends Model
{
    use HasFactory;

    protected $table = 'm_sub_jenis_surat';

    public $timestamps = false;

    protected $primaryKey = 'id_sub_jenis';

    protected $fillable = [
        'id_sub_jenis',
        'id_jenis',
        'sub_jenis'
    ];
}
