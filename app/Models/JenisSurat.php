<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class JenisSurat extends Model
{
    use HasFactory;

    protected $table = 'm_jenis_surat';

    public $timestamps = false;

    protected $primaryKey = 'id_jenis';

    protected $fillable = [
        'id_jenis',
        'jenis_surat'
    ];
}
