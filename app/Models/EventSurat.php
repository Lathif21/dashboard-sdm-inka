<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EventSurat extends Model
{
    use HasFactory;

    protected $table = 'surat_event';

    public $timestamps = false;

    protected $primaryKey = 'id_event';

    protected $fillable = [
        'id_event',
        'id_surat',
        'event',
        'surat_object',
    ];
}
