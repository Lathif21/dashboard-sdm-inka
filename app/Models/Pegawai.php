<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pegawai extends Model
{
    use HasFactory;

    // protected $connection = 'sqlsrv';

    protected $table = 'pegawai';

    public $timestamps = false;

    protected $primaryKey = 'id_pegawai';

    protected $fillable = [
        'nama_pegawai',
        'NIP',
        'jabatan_pegawai',
        'Status',
        'Divisi',
        'Departemen',
    ];
}
