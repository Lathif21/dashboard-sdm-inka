<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Aktivitas extends Model
{
    use HasFactory;

    protected $table = 'aktivitas';
    protected $primaryKey = 'id_aktivitas';
    public $timestamps = false;
    protected $fillable = [
        'id_aktivitas',
        'id_surat',
        'nama_aktivitas',
        'keterangan',
    ];
}
