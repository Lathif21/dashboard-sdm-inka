<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class NotifyUsers extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'daily:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Notifikasi Telah Berjalan';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        DB::table('m_surat')
            ->where('tgl_berakhir', '>', Carbon::today())
            ->where('tgl_berakhir', '<=', Carbon::today()->addDays(14))
            ->update(['is_active' => 2]);
        echo "Notifikasi Telah Berjalan";
    }
}
