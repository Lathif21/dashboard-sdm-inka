<?php

namespace App\Imports;

use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Concerns\ToModel;
use App\Models\Pegawai;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithCalculatedFormulas;
use Maatwebsite\Excel\Concerns\WithBatchInserts;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Maatwebsite\Excel\Concerns\WithUpserts;
use Carbon\Carbon;
use PhpOffice\PhpSpreadsheet\Shared\Date;
use ErrorException;
use Maatwebsite\Excel\Concerns\WithValidation;
use Maatwebsite\Excel\Concerns\SkipsEmptyRows;
use Maatwebsite\Excel\Row;

class pegawaiImport implements
    ToModel,
    WithValidation,
    WithCalculatedFormulas,
    WithHeadingRow,
    WithUpserts,
    WithBatchInserts,
    WithChunkReading,
    SkipsEmptyRows

{
    use Importable;
    /**
     * @param array $row
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function model(array $row)
    {

        return new Pegawai([
            'nama_pegawai' => $row['nama'] ?? null,
            'NIP' => $row['nip'] ?? null,
            'jabatan_pegawai' => $row['jabatan'] ?? null,
            'Status' => $row['status'] ?? null,
            'Divisi' => $row['divisi'] ?? null,
            'Departemen' => $row['departemen'] ?? null,
        ]);
    }
    public function transformDate($value, $format = 'd-m-Y')
    {
        try {
            return Carbon::instance(Date::excelToDateTimeObject($value));
        } catch (ErrorException $e) {
            return Carbon::createFromFormat($format, $value);
        }
    }

    public function uniqueBy()
    {
        return 'id_pegawai';
    }

    public function batchSize(): int
    {
        return 75;
    }

    public function chunkSize(): int
    {
        return 75;
    }
    public function rules(): array
    {
        return [];
    }
}
