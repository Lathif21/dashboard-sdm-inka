-- phpMyAdmin SQL Dump
-- version 5.0.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 20, 2022 at 10:31 AM
-- Server version: 10.4.14-MariaDB
-- PHP Version: 7.4.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sdm`
--

-- --------------------------------------------------------

--
-- Table structure for table `aktivitas`
--

CREATE TABLE `aktivitas` (
  `id_aktivitas` int(11) UNSIGNED NOT NULL,
  `id_surat` bigint(20) UNSIGNED NOT NULL,
  `nama_aktivitas` varchar(200) NOT NULL,
  `keterangan` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `aktivitas`
--

INSERT INTO `aktivitas` (`id_aktivitas`, `id_surat`, `nama_aktivitas`, `keterangan`) VALUES
(1, 4, 'Gembala Paus', 'Tim Tam'),
(2, 5, 'Beternak Unicorn', 'Memberi makan unicorn');

-- --------------------------------------------------------

--
-- Table structure for table `detail_aktivitas`
--

CREATE TABLE `detail_aktivitas` (
  `id_detail` int(11) NOT NULL,
  `id_aktivitas` int(11) UNSIGNED NOT NULL,
  `id_pegawai` int(11) NOT NULL,
  `jabatan_tim` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2019_12_14_000001_create_personal_access_tokens_table', 1),
(5, '2022_01_10_065543_m_jenis_surat', 1),
(6, '2022_01_10_065705_m_sub_jenis_surat', 1),
(10, '2022_01_10_065725_user', 2);

-- --------------------------------------------------------

--
-- Table structure for table `m_jenis_surat`
--

CREATE TABLE `m_jenis_surat` (
  `id_jenis` bigint(20) UNSIGNED NOT NULL,
  `jenis_surat` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `m_jenis_surat`
--

INSERT INTO `m_jenis_surat` (`id_jenis`, `jenis_surat`) VALUES
(1, 'Surat Keputusan'),
(2, 'Surat Tugas'),
(3, 'Surat Edaran'),
(4, 'Pemberitahuan'),
(5, 'Surat Dinas'),
(6, 'Kontrak PKWT'),
(7, 'Storage History');

--
-- Triggers `m_jenis_surat`
--
DELIMITER $$
CREATE TRIGGER `id` BEFORE INSERT ON `m_jenis_surat` FOR EACH ROW BEGIN
           DECLARE id bigint(20) DEFAULT 0;
           SET id = (SELECT COUNT(id_jenis) FROM m_jenis_surat) + 1;
           SET new.id_jenis = id;
       END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `m_sub_jenis_surat`
--

CREATE TABLE `m_sub_jenis_surat` (
  `id_sub_jenis` bigint(20) UNSIGNED NOT NULL,
  `id_jenis` bigint(20) UNSIGNED NOT NULL,
  `sub_jenis` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `m_sub_jenis_surat`
--

INSERT INTO `m_sub_jenis_surat` (`id_sub_jenis`, `id_jenis`, `sub_jenis`) VALUES
(1, 2, 'PLT'),
(2, 2, 'PJS'),
(3, 2, 'Tim'),
(4, 1, 'Rotasi'),
(5, 1, 'Promosi'),
(6, 1, 'Penugasan Tim'),
(7, 1, 'Kenaikan Golongan');

--
-- Triggers `m_sub_jenis_surat`
--
DELIMITER $$
CREATE TRIGGER `id_sub` BEFORE INSERT ON `m_sub_jenis_surat` FOR EACH ROW BEGIN
           DECLARE id bigint(20) DEFAULT 0;
           SET id = (SELECT COUNT(id_sub_jenis) FROM m_sub_jenis_surat) + 1;
           SET new.id_sub_jenis = id;
       END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `m_surat`
--

CREATE TABLE `m_surat` (
  `id_surat` bigint(20) UNSIGNED NOT NULL,
  `id_jenis` bigint(20) UNSIGNED NOT NULL,
  `id_sub_jenis` bigint(20) UNSIGNED DEFAULT NULL,
  `nomor_surat` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `judul_surat` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tgl_terbit` date NOT NULL,
  `tgl_berlaku` date NOT NULL,
  `tgl_berakhir` date NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `is_public` tinyint(1) NOT NULL,
  `path_surat` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `m_surat`
--

INSERT INTO `m_surat` (`id_surat`, `id_jenis`, `id_sub_jenis`, `nomor_surat`, `judul_surat`, `tgl_terbit`, `tgl_berlaku`, `tgl_berakhir`, `is_active`, `is_public`, `path_surat`, `deleted_at`) VALUES
(1, 2, 1, 'ST NO.12 2022', 'Pengelolaan Sistem Elektronik, Informasi Elektronik, dan Dokumen Elektronik di Lingkungan Badan Pemeriksa Keuangan', '2021-12-16', '2021-12-18', '2022-02-28', 1, 1, '', NULL),
(2, 4, NULL, 'ST NO.122 2022', 'Pengelolaan  Dokumen di Lingkungan Badan Pemeriksa Keuangan', '2022-01-15', '2022-01-16', '2022-01-29', 0, 0, NULL, NULL),
(3, 3, NULL, 'ST NO.18 2020', 'Tata Letak Parkir', '2020-01-15', '2020-01-16', '2022-01-29', 1, 0, '2022/Install.pdf', NULL),
(4, 6, NULL, 'ST NO.128 2019', 'Magang Lathif Sihab Dewantoro', '2022-01-28', '2022-01-29', '2022-01-31', 1, 0, '2022/Pengajuan Dana - Abid Shofa.pdf', NULL),
(5, 1, 6, 'SK NO.32 2022', 'Peratutan Magang PT.INKA', '2022-01-31', '2022-02-01', '2022-02-23', 2, 0, '2022/SURAT KETERANGAN RT RW.pdf', NULL);

--
-- Triggers `m_surat`
--
DELIMITER $$
CREATE TRIGGER `id_surat` BEFORE INSERT ON `m_surat` FOR EACH ROW BEGIN
           DECLARE id bigint(20) DEFAULT 0;
           SET id = (SELECT COUNT(id_surat) FROM m_surat) + 1;
           SET new.id_surat = id;
       END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pegawai`
--

CREATE TABLE `pegawai` (
  `id_pegawai` int(11) NOT NULL,
  `nama_pegawai` varchar(200) DEFAULT NULL,
  `NIP` varchar(25) DEFAULT NULL,
  `jabatan_pegawai` varchar(200) DEFAULT NULL,
  `Status` varchar(25) NOT NULL,
  `Divisi` varchar(100) NOT NULL,
  `Departemen` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `pegawai`
--

INSERT INTO `pegawai` (`id_pegawai`, `nama_pegawai`, `NIP`, `jabatan_pegawai`, `Status`, `Divisi`, `Departemen`) VALUES
(100, 'LITA MURTI HASTANTI                               ', '999600004 ', 'General Manager Keuangan dan Akuntansi                                                              ', 'AKTIF', 'KEUANGAN DAN AKUNTANSI                            ', 'DIVISI KEUANGAN DAN AKUNTANSI'),
(101, 'YULIUS ARTHA FIYANTHARA                           ', '991100020 ', 'Spesialis Muda - Div. Keuangan dan Akuntansi                                                        ', 'AKTIF', 'KEUANGAN DAN AKUNTANSI                            ', 'DIVISI KEUANGAN DAN AKUNTANSI'),
(102, 'RARA FEBRINA SUSANTO                              ', '662100004 ', 'Staf PKWT (Gol I) Div. Keuangan dan Akuntansi                                                       ', 'AKTIF', 'KEUANGAN DAN AKUNTANSI                            ', 'DIVISI KEUANGAN DAN AKUNTANSI'),
(103, 'DINI MARTIA PRASTITI                              ', '990900006 ', 'Senior Manager Akuntansi dan Perpajakan                                                             ', 'AKTIF', 'KEUANGAN DAN AKUNTANSI                            ', 'AKUNTANSI DAN PERPAJAKAN'),
(104, 'AGUNG ARIPIN                                      ', '661900029 ', 'Staf PKWT (Gol I) Dep. Akuntansi dan Perpajakan                                                     ', 'AKTIF', 'KEUANGAN DAN AKUNTANSI                            ', 'AKUNTANSI DAN PERPAJAKAN'),
(105, 'ANISA DWI OKTAVIANI                               ', '661900032 ', 'Staf PKWT (Gol I) Dep. Akuntansi dan Perpajakan                                                     ', 'AKTIF', 'KEUANGAN DAN AKUNTANSI                            ', 'AKUNTANSI DAN PERPAJAKAN'),
(106, 'GITA DAHLIANI                                     ', '661900207 ', 'Staf PKWT (Gol I) Dep. Akuntansi dan Perpajakan                                                     ', 'AKTIF', 'KEUANGAN DAN AKUNTANSI                            ', 'AKUNTANSI DAN PERPAJAKAN'),
(107, 'PRIMA AMANDA PERMATASARI                          ', '991800021 ', 'Manager Pelaporan dan Analisa Akuntansi/Keuangan                                                    ', 'AKTIF', 'KEUANGAN DAN AKUNTANSI                            ', 'AKUNTANSI DAN PERPAJAKAN'),
(108, 'DESTIA KUSUMA                                     ', '991400004 ', 'Staf (Gol II) Bag. Pelaporan dan Analisa Akuntansi/Keuangan                                         ', 'AKTIF', 'KEUANGAN DAN AKUNTANSI                            ', 'AKUNTANSI DAN PERPAJAKAN'),
(109, 'GRAHANDY NUR HIDAYAT                              ', '991700062 ', 'Staf (Gol II) Bag. Pelaporan dan Analisa Akuntansi/Keuangan                                         ', 'AKTIF', 'KEUANGAN DAN AKUNTANSI                            ', 'AKUNTANSI DAN PERPAJAKAN'),
(110, 'HIMA RESFIANA MARCHYLOVA                          ', '991500009 ', 'Staf (Gol II) Bag. Pajak dan Fasilitas Kepabeanan                                                   ', 'AKTIF', 'KEUANGAN DAN AKUNTANSI                            ', 'AKUNTANSI DAN PERPAJAKAN'),
(111, 'YANUAR HADI IRNANTO                               ', '991400007 ', 'Staf (Gol I) Bag. Pajak dan Fasilitas Kepabeanan                                                    ', 'AKTIF', 'KEUANGAN DAN AKUNTANSI                            ', 'AKUNTANSI DAN PERPAJAKAN'),
(112, 'EDWYN DWI CAHYO                                   ', '991100040 ', 'Senior Manager Pendanaan dan Perbendaharaan                                                         ', 'AKTIF', 'KEUANGAN DAN AKUNTANSI                            ', 'PENDANAAN DAN PERBENDAHARAAN'),
(113, 'KHORI PRATIWI                                     ', '661900109 ', 'Staf PKWT (Gol I) Dep. Pendanaan dan Perbendaharaan                                                 ', 'AKTIF', 'KEUANGAN DAN AKUNTANSI                            ', 'PENDANAAN DAN PERBENDAHARAAN'),
(114, 'AISA YULIANA                                      ', '662100014 ', 'Staf PKWT (Gol I) Dep. Pendanaan dan Perbendaharaan                                                 ', 'AKTIF', 'KEUANGAN DAN AKUNTANSI                            ', 'PENDANAAN DAN PERBENDAHARAAN'),
(115, 'ADHITYA NOVIYUDHA YUSTIAWAN                       ', '991700076 ', 'Manager Pendanaan                                                                                   ', 'AKTIF', 'KEUANGAN DAN AKUNTANSI                            ', 'PENDANAAN DAN PERBENDAHARAAN'),
(116, 'ERVYNA JASTYANI                                   ', '991100006 ', 'Staf (Gol I) Bag. Pendanaan                                                                         ', 'AKTIF', 'KEUANGAN DAN AKUNTANSI                            ', 'PENDANAAN DAN PERBENDAHARAAN'),
(117, 'ETIN KURNIATIN                                    ', '991100007 ', 'Manager Perbendaharaan dan Asuransi                                                                 ', 'AKTIF', 'KEUANGAN DAN AKUNTANSI                            ', 'PENDANAAN DAN PERBENDAHARAAN'),
(118, 'ROCHMAT SUMANTRI                                  ', '991400005 ', 'Staf (Gol II) Bag. Perbendaharaan dan Asuransi                                                      ', 'AKTIF', 'KEUANGAN DAN AKUNTANSI                            ', 'PENDANAAN DAN PERBENDAHARAAN'),
(119, 'INSI KAMILAH INDALLAH                             ', '991500010 ', 'Staf (Gol II) Bag. Perbendaharaan dan Asuransi                                                      ', 'AKTIF', 'KEUANGAN DAN AKUNTANSI                            ', 'PENDANAAN DAN PERBENDAHARAAN'),
(120, 'DJOKO PRIJATNO                                    ', '999900109 ', 'Staf (Gol I) Bag. Perbendaharaan dan Asuransi                                                       ', 'AKTIF', 'KEUANGAN DAN AKUNTANSI                            ', 'PENDANAAN DAN PERBENDAHARAAN'),
(121, 'DEVITASARI INABELLA                               ', '662100005 ', 'Staf PKWT (Gol I) Dep. Perencanaan dan Pengendalian Keuangan                                        ', 'AKTIF', 'KEUANGAN DAN AKUNTANSI                            ', 'PERENCANAAN DAN PENGENDALIAN KEUANGAN'),
(122, 'VINA                                              ', '991400006 ', 'Manager Perencanaan Keuangan                                                                        ', 'AKTIF', 'KEUANGAN DAN AKUNTANSI                            ', 'PERENCANAAN DAN PENGENDALIAN KEUANGAN'),
(123, 'DIMAS CATUR PRASETYO                              ', '991700058 ', 'Staf (Gol II) Bag. Perencanaan Keuangan                                                             ', 'AKTIF', 'KEUANGAN DAN AKUNTANSI                            ', 'PERENCANAAN DAN PENGENDALIAN KEUANGAN'),
(124, 'FANDITAMA AKBAR NUGRAHA                           ', '992100012 ', 'Staf (Gol I) Bag. Perencanaan Keuangan                                                              ', 'AKTIF', 'KEUANGAN DAN AKUNTANSI                            ', 'PERENCANAAN DAN PENGENDALIAN KEUANGAN'),
(125, 'RATNA YULI ASTUTIK                                ', '991500034 ', 'Manager Verifikasi                                                                                  ', 'AKTIF', 'KEUANGAN DAN AKUNTANSI                            ', 'PERENCANAAN DAN PENGENDALIAN KEUANGAN'),
(126, 'RETNO PUJI RAHAYU                                 ', '991100015 ', 'Staf (Gol I) Bag. Verifikasi                                                                        ', 'AKTIF', 'KEUANGAN DAN AKUNTANSI                            ', 'PERENCANAAN DAN PENGENDALIAN KEUANGAN'),
(127, 'GEDE PASEK SUARDIKA                               ', '990000014 ', 'Komisaris Utama                                                                                     ', 'AKTIF', 'DEWAN KOMISARIS                                   ', 'DEWAN KOMISARIS'),
(128, 'MUHAMMAD KHAYAM                                   ', '990000015 ', 'Komisaris                                                                                           ', 'AKTIF', 'DEWAN KOMISARIS                                   ', 'DEWAN KOMISARIS'),
(129, 'SITI MALKHAMAH                                    ', '990000016 ', 'Komisaris Independen                                                                                ', 'AKTIF', 'DEWAN KOMISARIS                                   ', 'DEWAN KOMISARIS'),
(130, 'JOKO UNTORO SLAMET                                ', '990000008 ', 'Sekretaris Dewan Komisaris                                                                          ', 'AKTIF', 'DEWAN KOMISARIS                                   ', 'DEWAN KOMISARIS'),
(131, 'FRANKY SETIAWAN                                   ', '990000009 ', 'Anggota Komite Audit                                                                                ', 'AKTIF', 'DEWAN KOMISARIS                                   ', 'DEWAN KOMISARIS'),
(132, 'ILHAM                                             ', '990000010 ', 'Anggota Komite Pemantau Manajemen Risiko                                                            ', 'AKTIF', 'DEWAN KOMISARIS                                   ', 'DEWAN KOMISARIS'),
(133, 'BUDI NOVIANTORO                                   ', '990000001 ', 'Direktur Utama                                                                                      ', 'AKTIF', 'DEWAN KOMISARIS                                   ', 'DEWAN KOMISARIS'),
(134, 'ANDY BUDIMAN                                      ', '990000012 ', 'Direktur Keuangan, SDM dan Manajemen Risiko                                                         ', 'AKTIF', 'DEWAN KOMISARIS                                   ', 'DEWAN KOMISARIS'),
(135, 'AGUNG SEDAJU                                      ', '990000003 ', 'Direktur Pengembangan                                                                               ', 'AKTIF', 'DEWAN KOMISARIS                                   ', 'DEWAN KOMISARIS'),
(136, 'I GEDE AGUS PRAYATNA                              ', '990000013 ', 'Direktur Operasi                                                                                    ', 'AKTIF', 'DEWAN KOMISARIS                                   ', 'DEWAN KOMISARIS'),
(137, 'ANDRAWINA SUKMA HARSANJANI                        ', '662200009 ', 'Staf PKWT (Gol I) Bag. Manajemen Risiko                                                             ', 'AKTIF', 'MANAJEMEN RISIKO DAN HUKUM                        ', 'MANAJEMEN RISIKO DAN KEPATUHAN'),
(138, 'DYAH RETNO WATI                                   ', '999200048 ', 'Manager Kepatuhan, GCG dan KPKU                                                                     ', 'AKTIF', 'MANAJEMEN RISIKO DAN HUKUM                        ', 'MANAJEMEN RISIKO DAN KEPATUHAN'),
(139, 'NOVIDA PRATIWI                                    ', '991900026 ', 'Staf (Gol I) Bag. Kepatuhan, GCG dan KPKU                                                           ', 'AKTIF', 'MANAJEMEN RISIKO DAN HUKUM                        ', 'MANAJEMEN RISIKO DAN KEPATUHAN'),
(140, 'BEDY KHARISMA                                     ', '991700077 ', 'Spesialis Muda - Bag. Kepatuhan, GCG dan KPKU                                                       ', 'AKTIF', 'MANAJEMEN RISIKO DAN HUKUM                        ', 'MANAJEMEN RISIKO DAN KEPATUHAN'),
(141, 'EMIRA IFFAT                                       ', '661710332 ', 'Staf PKWT (Gol I) Dep. Manajemen Risiko dan Kepatuhan                                               ', 'AKTIF', 'MANAJEMEN RISIKO DAN HUKUM                        ', 'MANAJEMEN RISIKO DAN KEPATUHAN'),
(142, 'MUTIA AYU APRILIA PUTRI                           ', '661900263 ', 'Staf PKWT (Gol I) Dep. Manajemen Risiko dan Kepatuhan                                               ', 'AKTIF', 'MANAJEMEN RISIKO DAN HUKUM                        ', 'MANAJEMEN RISIKO DAN KEPATUHAN'),
(143, 'AGNISA BHAKTI PERSADA                             ', '991200003 ', 'Manager Manajemen Risiko                                                                            ', 'AKTIF', 'MANAJEMEN RISIKO DAN HUKUM                        ', 'MANAJEMEN RISIKO DAN KEPATUHAN'),
(144, 'ELISA WINDI DAMAYANTI                             ', '662100009 ', 'Staf PKWT (Gol I) Div. Manajemen Risiko dan Hukum                                                   ', 'AKTIF', 'MANAJEMEN RISIKO DAN HUKUM                        ', 'DIVISI MANAJEMEN RISIKO DAN HUKUM'),
(145, 'ROCHMAD AGUNG WIDODO                              ', '991800026 ', 'Senior Manager Hukum                                                                                ', 'AKTIF', 'MANAJEMEN RISIKO DAN HUKUM                        ', 'HUKUM'),
(146, 'HENNY LYNA NILANDARI                              ', '991200020 ', 'Manager Hukum                                                                                       ', 'AKTIF', 'MANAJEMEN RISIKO DAN HUKUM                        ', 'HUKUM'),
(147, 'HARRY INDRAYANTO                                  ', '991400009 ', 'Staf (Gol II) Bag. Hukum                                                                            ', 'AKTIF', 'MANAJEMEN RISIKO DAN HUKUM                        ', 'HUKUM'),
(148, 'WAHYU PURNOMO                                     ', '992100023 ', 'Staf (Gol I) Bag. Hukum                                                                             ', 'AKTIF', 'MANAJEMEN RISIKO DAN HUKUM                        ', 'HUKUM'),
(149, 'EDI WINARNO                                       ', '999400101 ', 'Spesialis Utama - Div. Perencanaan Operasi dan Penyediaan Jasa                                      ', 'AKTIF', 'PERENCANAAN OPERASI DAN PENYEDIAAN JASA           ', 'DIVISI PERENCANAAN OPERASI DAN PENYEDIAAN JASA'),
(150, 'HERY PRASETYA                                     ', '999400017 ', 'Spesialis Utama - Div. Perencanaan Operasi dan Penyediaan Jasa                                      ', 'AKTIF', 'PERENCANAAN OPERASI DAN PENYEDIAAN JASA           ', 'DIVISI PERENCANAAN OPERASI DAN PENYEDIAAN JASA'),
(151, 'BAMBANG JATMIKA                                   ', '999500014 ', 'Spesialis Madya - Div. Perencanaan Operasi dan Penyediaan Jasa                                      ', 'AKTIF', 'PERENCANAAN OPERASI DAN PENYEDIAAN JASA           ', 'DIVISI PERENCANAAN OPERASI DAN PENYEDIAAN JASA'),
(152, 'WINARTO                                           ', '999300008 ', 'Pejabat Setingkat SM Div. Perencanaan Operasi dan Penyediaan Jasa                                   ', 'AKTIF', 'PERENCANAAN OPERASI DAN PENYEDIAAN JASA           ', 'DIVISI PERENCANAAN OPERASI DAN PENYEDIAAN JASA'),
(153, 'SUTORO                                            ', '999900118 ', 'Senior Manager Perencanaan dan Pengendalian Produksi                                                ', 'AKTIF', 'PERENCANAAN OPERASI DAN PENYEDIAAN JASA           ', 'PERENCANAAN DAN PENGENDALIAN PRODUKSI'),
(154, 'MOHAMMAD JA\'FAR SODIQ                             ', '991200024 ', 'Pejabat Setingkat M Dep. Perencanaan dan Pengendalian Produksi                                      ', 'AKTIF', 'PERENCANAAN OPERASI DAN PENYEDIAAN JASA           ', 'PERENCANAAN DAN PENGENDALIAN PRODUKSI'),
(155, 'RIFDAH PERMATA GEMILANG                           ', '662100013 ', 'Staf PKWT (Gol I) Dep. Perencanaan dan Pengendalian Produksi                                        ', 'AKTIF', 'PERENCANAAN OPERASI DAN PENYEDIAAN JASA           ', 'PERENCANAAN DAN PENGENDALIAN PRODUKSI'),
(156, 'ANA                                               ', '991400022 ', 'Manager Perencanaan Produksi                                                                        ', 'AKTIF', 'PERENCANAAN OPERASI DAN PENYEDIAAN JASA           ', 'PERENCANAAN DAN PENGENDALIAN PRODUKSI'),
(157, 'HADI WURIYANTO                                    ', '991200017 ', 'Staf (Gol II) Bag. Perencanaan Produksi                                                             ', 'AKTIF', 'PERENCANAAN OPERASI DAN PENYEDIAAN JASA           ', 'PERENCANAAN DAN PENGENDALIAN PRODUKSI'),
(158, 'DEKANITA ESTRIE PAKSI                             ', '991700057 ', 'Staf (Gol II) Bag. Perencanaan Produksi                                                             ', 'AKTIF', 'PERENCANAAN OPERASI DAN PENYEDIAAN JASA           ', 'PERENCANAAN DAN PENGENDALIAN PRODUKSI'),
(159, 'R. PRAWIRO KUSUMO RAHARJO                         ', '992100017 ', 'Staf (Gol I) Bag. Pengendalian Produksi                                                             ', 'AKTIF', 'PERENCANAAN OPERASI DAN PENYEDIAAN JASA           ', 'PERENCANAAN DAN PENGENDALIAN PRODUKSI'),
(160, 'JANGKUNG ROBIET KRISTIYANTO                       ', '661800074 ', 'Staf PKWT (Gol I) Bag. Pengendalian Produksi                                                        ', 'AKTIF', 'PERENCANAAN OPERASI DAN PENYEDIAAN JASA           ', 'PERENCANAAN DAN PENGENDALIAN PRODUKSI'),
(161, 'SIGIT SETIAWAN                                    ', '661900031 ', 'Staf PKWT (Gol I) Bag. Pengendalian Produksi                                                        ', 'AKTIF', 'PERENCANAAN OPERASI DAN PENYEDIAAN JASA           ', 'PERENCANAAN DAN PENGENDALIAN PRODUKSI'),
(162, 'MUHAMMAD ARIF WAKHID                              ', '991800019 ', 'SPV Unit Perencanaan Produksi                                                                       ', 'AKTIF', 'PERENCANAAN OPERASI DAN PENYEDIAAN JASA           ', 'PERENCANAAN DAN PENGENDALIAN PRODUKSI'),
(163, 'FARID AHMAD LAIL                                  ', '991200014 ', 'Manager Pengendalian Produksi                                                                       ', 'AKTIF', 'PERENCANAAN OPERASI DAN PENYEDIAAN JASA           ', 'PERENCANAAN DAN PENGENDALIAN PRODUKSI'),
(164, 'AFTAHLANA RIZKY HIDAYAT H                         ', '991700050 ', 'Staf (Gol II) Bag. Pengendalian Produksi                                                            ', 'AKTIF', 'PERENCANAAN OPERASI DAN PENYEDIAAN JASA           ', 'PERENCANAAN DAN PENGENDALIAN PRODUKSI'),
(165, 'ADHISMA FIRDAUS                                   ', '991200001 ', 'Staf (Gol I) Bag. Pengendalian Produksi                                                             ', 'AKTIF', 'PERENCANAAN OPERASI DAN PENYEDIAAN JASA           ', 'PERENCANAAN DAN PENGENDALIAN PRODUKSI'),
(166, 'RICORDHA DATU SUKOCO                              ', '991200028 ', 'Staf (Gol I) Bag. Pengendalian Produksi                                                             ', 'AKTIF', 'PERENCANAAN OPERASI DAN PENYEDIAAN JASA           ', 'PERENCANAAN DAN PENGENDALIAN PRODUKSI'),
(167, 'DARSO                                             ', '991200044 ', 'Staf (Gol I) Bag. Pengendalian Produksi                                                             ', 'AKTIF', 'PERENCANAAN OPERASI DAN PENYEDIAAN JASA           ', 'PERENCANAAN DAN PENGENDALIAN PRODUKSI'),
(168, 'AGUS WIDODO                                       ', '991000003 ', 'Senior Manager Penyediaan Jasa                                                                      ', 'AKTIF', 'PERENCANAAN OPERASI DAN PENYEDIAAN JASA           ', 'PENYEDIAAN JASA '),
(169, 'EDI PUGUH TRIJONO                                 ', '999600042 ', 'Staf (Gol II) Dep. Penyediaan Jasa                                                                  ', 'AKTIF', 'PERENCANAAN OPERASI DAN PENYEDIAAN JASA           ', 'PENYEDIAAN JASA '),
(170, 'RIZKI TRI KURNIAWAN                               ', '991100065 ', 'Manager Perencanaan Penyediaan Jasa                                                                 ', 'AKTIF', 'PERENCANAAN OPERASI DAN PENYEDIAAN JASA           ', 'PENYEDIAAN JASA '),
(171, 'HEPI KURNIAWAN                                    ', '992100027 ', 'Staf (Gol I) Bag. Perencanaan Penyediaan Jasa                                                       ', 'AKTIF', 'PERENCANAAN OPERASI DAN PENYEDIAAN JASA           ', 'PENYEDIAAN JASA '),
(172, 'HAQQUL AMINNOVA EL FARIS                          ', '991100009 ', 'SPV Unit Perencanaan Penyediaan Jasa 1                                                              ', 'AKTIF', 'PERENCANAAN OPERASI DAN PENYEDIAAN JASA           ', 'PENYEDIAAN JASA '),
(173, 'REDY AULIYA ROIS                                  ', '991500030 ', 'SPV Unit Perencanaan Penyediaan Jasa 2                                                              ', 'AKTIF', 'PERENCANAAN OPERASI DAN PENYEDIAAN JASA           ', 'PENYEDIAAN JASA '),
(174, 'PUTU RIO ADITYA DHARMA                            ', '991100060 ', 'Manager Pengendalian Penyediaan Jasa                                                                ', 'AKTIF', 'PERENCANAAN OPERASI DAN PENYEDIAAN JASA           ', 'PENYEDIAAN JASA '),
(175, 'ANGGORO DWI PRAKOSO                               ', '991200006 ', 'Staf (Gol II) Bag. Pengendalian Penyediaan Jasa                                                     ', 'AKTIF', 'PERENCANAAN OPERASI DAN PENYEDIAAN JASA           ', 'PENYEDIAAN JASA '),
(176, 'DANANG ARYA YUDHISTIRA                            ', '991900002 ', 'Staf (Gol I) Bag. Pengendalian Penyediaan Jasa                                                      ', 'AKTIF', 'PERENCANAAN OPERASI DAN PENYEDIAAN JASA           ', 'PENYEDIAAN JASA '),
(177, 'TRIONO                                            ', '999800109 ', 'SPV Unit Pengendalian Penyediaan Jasa                                                               ', 'AKTIF', 'PERENCANAAN OPERASI DAN PENYEDIAAN JASA           ', 'PENYEDIAAN JASA '),
(178, 'WIWID AFRIYANTO                                   ', '990900016 ', 'Senior Manager Pengelolaan Proyek                                                                   ', 'AKTIF', 'PERENCANAAN OPERASI DAN PENYEDIAAN JASA           ', 'PENGELOLAAN PROYEK'),
(179, 'ELOK DWI NURWANTO                                 ', '991100005 ', 'Staf (Gol II) Dep. Pengelolaan Proyek                                                               ', 'AKTIF', 'PERENCANAAN OPERASI DAN PENYEDIAAN JASA           ', 'PENGELOLAAN PROYEK'),
(180, 'HERMAN SYAHRONI                                   ', '991100046 ', 'Staf (Gol II) Dep. Pengelolaan Proyek                                                               ', 'AKTIF', 'PERENCANAAN OPERASI DAN PENYEDIAAN JASA           ', 'PENGELOLAAN PROYEK'),
(181, 'ANISA PERMATASARI                                 ', '991500006 ', 'Staf (Gol II) Dep. Pengelolaan Proyek                                                               ', 'AKTIF', 'PERENCANAAN OPERASI DAN PENYEDIAAN JASA           ', 'PENGELOLAAN PROYEK'),
(182, 'LAULIA ARIYANI RANGKUTI                           ', '991500033 ', 'Staf (Gol II) Dep. Pengelolaan Proyek                                                               ', 'AKTIF', 'PERENCANAAN OPERASI DAN PENYEDIAAN JASA           ', 'PENGELOLAAN PROYEK'),
(183, 'ARDHANU USDHIANTOKO                               ', '992100006 ', 'Staf (Gol I) Dep. Pengelolaan Proyek                                                                ', 'AKTIF', 'PERENCANAAN OPERASI DAN PENYEDIAAN JASA           ', 'PENGELOLAAN PROYEK'),
(184, 'DADIK PRANATA TIRTA                               ', '991800025 ', 'Pejabat Setingkat SM Yang Ditugaskan Sebagai PM Proyek Philipina                                    ', 'AKTIF', 'PERENCANAAN OPERASI DAN PENYEDIAAN JASA           ', 'PENGELOLAAN PROYEK'),
(185, 'VIRGIANTO ATLANTIKA                               ', '999800005 ', 'Spesialis Muda - Project Manager Group                                                              ', 'AKTIF', 'PERENCANAAN OPERASI DAN PENYEDIAAN JASA           ', 'PENGELOLAAN PROYEK'),
(186, 'NUR AZIZAH RAHMADANI                              ', '661710344 ', 'Staf PKWT (Gol I) Dep. Pengelolaan Proyek                                                           ', 'AKTIF', 'PERENCANAAN OPERASI DAN PENYEDIAAN JASA           ', 'PENGELOLAAN PROYEK'),
(187, 'ANDI HARTANTO                                     ', '661900279 ', 'Staf PKWT (Gol I) Dep. Pengelolaan Proyek                                                           ', 'AKTIF', 'PERENCANAAN OPERASI DAN PENYEDIAAN JASA           ', 'PENGELOLAAN PROYEK'),
(188, 'INDAH PUSPITA NINGRUM                             ', '661900299 ', 'Staf PKWT (Gol I) Dep. Pengelolaan Proyek                                                           ', 'AKTIF', 'PERENCANAAN OPERASI DAN PENYEDIAAN JASA           ', 'PENGELOLAAN PROYEK'),
(189, 'PRISTA NUR KUSUMA WARDHANI                        ', '662100003 ', 'Staf PKWT (Gol I) Dep. Pengembangan Perusahaan                                                      ', 'AKTIF', 'PENGEMBANGAN PERUSAHAAN                           ', 'PENGEMBANGAN PERUSAHAAN'),
(190, 'DONA ELSAFIRA ANASTASYA                           ', '662100012 ', 'Staf PKWT (Gol I) Dep. Pengembangan Perusahaan                                                      ', 'AKTIF', 'PENGEMBANGAN PERUSAHAAN                           ', 'PENGEMBANGAN PERUSAHAAN'),
(191, 'ROY PRADHANA                                      ', '991400015 ', 'Manager Perencanaan dan Penilaian Kinerja                                                           ', 'AKTIF', 'PENGEMBANGAN PERUSAHAAN                           ', 'PENGEMBANGAN PERUSAHAAN'),
(192, 'BUDI SANTOSO                                      ', '991200043 ', 'Staf (Gol I) Bag. Perencanaan dan Penilaian Kinerja                                                 ', 'AKTIF', 'PENGEMBANGAN PERUSAHAAN                           ', 'PENGEMBANGAN PERUSAHAAN'),
(193, 'FARINDA CESARIZKA RAHMITA                         ', '991900004 ', 'Staf (Gol I) Bag. Perencanaan dan Penilaian Kinerja                                                 ', 'AKTIF', 'PENGEMBANGAN PERUSAHAAN                           ', 'PENGEMBANGAN PERUSAHAAN'),
(194, 'ERLINDA PERMATA SARI                              ', '991700042 ', 'Manager Pengembangan Organisasi                                                                     ', 'AKTIF', 'PENGEMBANGAN PERUSAHAAN                           ', 'PENGEMBANGAN PERUSAHAAN'),
(195, 'NUR HAYATI                                        ', '991100058 ', 'Staf (Gol II) Bag. Pengembangan Organisasi                                                          ', 'AKTIF', 'PENGEMBANGAN PERUSAHAAN                           ', 'PENGEMBANGAN PERUSAHAAN'),
(196, 'SITI NUR RODIYAH                                  ', '991500019 ', 'Staf (Gol II) Bag. Pengembangan Organisasi                                                          ', 'AKTIF', 'PENGEMBANGAN PERUSAHAAN                           ', 'PENGEMBANGAN PERUSAHAAN'),
(197, 'AHMAD AFIF MAULANA                                ', '991800003 ', 'Staf (Gol II) Bag. Pengembangan Organisasi                                                          ', 'AKTIF', 'PENGEMBANGAN PERUSAHAAN                           ', 'PENGEMBANGAN PERUSAHAAN'),
(198, 'SAFIRA RATNASARI                                  ', '992100020 ', 'Staf (Gol I) Bag. Pengembangan Organisasi                                                           ', 'AKTIF', 'PENGEMBANGAN PERUSAHAAN                           ', 'PENGEMBANGAN PERUSAHAAN'),
(199, 'DYKA RAHAYU MEYLA SARI                            ', '991600005 ', 'Manager Hubungan Induk-Anak dan Perencanaan Investasi                                               ', 'AKTIF', 'PENGEMBANGAN PERUSAHAAN                           ', 'PENGEMBANGAN PERUSAHAAN'),
(200, 'GANDI WIDHI ARTHA                                 ', '991900024 ', 'Staf (Gol II) Bag. Hubungan Induk-Anak dan Perencanaan Investasi                                    ', 'AKTIF', 'PENGEMBANGAN PERUSAHAAN                           ', 'PENGEMBANGAN PERUSAHAAN'),
(201, 'RADEN DENNY HERWINDO                              ', '990900013 ', 'Pejabat Setingkat M Yang Diperbantukan Di Anak Perusahaan/Afiliasi PT Rekaindo Global Jasa          ', 'AKTIF', 'REKAINDO GLOBAL JASA                              ', 'REKAINDO GLOBAL JASA'),
(202, 'MU\'AMMAR ITQON                                    ', '990900011 ', 'Pejabat Setingkat M Dep. Teknologi Informasi                                                        ', 'AKTIF', 'RISET DAN PENGEMBANGAN                            ', 'TEKNOLOGI INFORMASI'),
(203, 'AHMAD FAJAR ALKHARIS                              ', '991100022 ', 'Pejabat Setingkat M Dep. Teknologi Informasi                                                        ', 'AKTIF', 'RISET DAN PENGEMBANGAN                            ', 'TEKNOLOGI INFORMASI'),
(204, 'AGRI KRIDANTO                                     ', '991700051 ', 'Staf (Gol II) Dep. Teknologi Informasi                                                              ', 'AKTIF', 'RISET DAN PENGEMBANGAN                            ', 'TEKNOLOGI INFORMASI'),
(205, 'MOCHAMAD YASIN                                    ', '991700068 ', 'Staf (Gol II) Dep. Teknologi Informasi                                                              ', 'AKTIF', 'RISET DAN PENGEMBANGAN                            ', 'TEKNOLOGI INFORMASI'),
(206, 'BASKWORO YOGA INDRA EXSHADI                       ', '991800004 ', 'Staf (Gol II) Dep. Teknologi Informasi                                                              ', 'AKTIF', 'RISET DAN PENGEMBANGAN                            ', 'TEKNOLOGI INFORMASI'),
(207, 'GUNTARTO BUDI ROHMADI                             ', '991800013 ', 'Staf (Gol II) Dep. Teknologi Informasi                                                              ', 'AKTIF', 'RISET DAN PENGEMBANGAN                            ', 'TEKNOLOGI INFORMASI'),
(208, 'ANANG FAJRIAL PRATHAMA                            ', '662000001 ', 'Staf PKWT (Gol I) Dep. Teknologi Informasi                                                          ', 'AKTIF', 'RISET DAN PENGEMBANGAN                            ', 'TEKNOLOGI INFORMASI'),
(209, 'MOCH. DAVID FACHRUDI                              ', '662000002 ', 'Staf PKWT (Gol I) Dep. Teknologi Informasi                                                          ', 'AKTIF', 'RISET DAN PENGEMBANGAN                            ', 'TEKNOLOGI INFORMASI'),
(210, 'AMRU HIDAYAT                                      ', '662100006 ', 'Staf PKWT (Gol I) Dep. Teknologi Informasi                                                          ', 'AKTIF', 'RISET DAN PENGEMBANGAN                            ', 'TEKNOLOGI INFORMASI'),
(211, 'RIZKY AGUNG NURCHOLIK                             ', '662100007 ', 'Staf PKWT (Gol I) Dep. Teknologi Informasi                                                          ', 'AKTIF', 'RISET DAN PENGEMBANGAN                            ', 'TEKNOLOGI INFORMASI'),
(212, 'ILMI NIHAYATUL KAMALIAH                           ', '662100016 ', 'Staf PKWT (Gol I) Dep. Teknologi Informasi                                                          ', 'AKTIF', 'RISET DAN PENGEMBANGAN                            ', 'TEKNOLOGI INFORMASI'),
(213, 'MAULIDYA ANGGRAINI                                ', '662100017 ', 'Staf PKWT (Gol I) Dep. Teknologi Informasi                                                          ', 'AKTIF', 'RISET DAN PENGEMBANGAN                            ', 'TEKNOLOGI INFORMASI'),
(214, 'SETIADY PERWIRA JATI                              ', '991900016 ', 'Staf (Gol II) Dep. Teknologi Informasi                                                              ', 'AKTIF', 'RISET DAN PENGEMBANGAN                            ', 'TEKNOLOGI INFORMASI'),
(215, 'GALIH AMIRUL ROHMAD                               ', '991200016 ', 'Staf (Gol I) Dep. Teknologi Informasi                                                               ', 'AKTIF', 'RISET DAN PENGEMBANGAN                            ', 'TEKNOLOGI INFORMASI'),
(216, 'MUHAMMAD GHAZALI SUWARDI                          ', '992000002 ', 'Staf (Gol I) Dep. Teknologi Informasi                                                               ', 'AKTIF', 'RISET DAN PENGEMBANGAN                            ', 'TEKNOLOGI INFORMASI'),
(217, 'SYAHLAN FAJAR NUZULI                              ', '992100022 ', 'Staf (Gol I) Dep. Teknologi Informasi                                                               ', 'AKTIF', 'RISET DAN PENGEMBANGAN                            ', 'TEKNOLOGI INFORMASI'),
(218, 'SUPRIYANTO                                        ', '999600024 ', 'Staf (Gol I) Dep. Teknologi Informasi                                                               ', 'AKTIF', 'RISET DAN PENGEMBANGAN                            ', 'TEKNOLOGI INFORMASI'),
(219, 'DWI HANANTO BAYU AJI                              ', '661900268 ', 'Staf PKWT (Gol I) Dep. Teknologi Informasi                                                          ', 'AKTIF', 'RISET DAN PENGEMBANGAN                            ', 'TEKNOLOGI INFORMASI'),
(220, 'CHOLIK MOCHAMAD ZAMZAM                            ', '999600003 ', 'General Manager Riset dan Pengembangan                                                              ', 'AKTIF', 'RISET DAN PENGEMBANGAN                            ', 'DIVISI RISET DAN PENGEMBANGAN'),
(221, 'MOCH. ISZAR                                       ', '999500018 ', 'Spesialis Utama - Div. Riset dan Pengembangan                                                       ', 'AKTIF', 'RISET DAN PENGEMBANGAN                            ', 'DIVISI RISET DAN PENGEMBANGAN'),
(222, 'MOKHAMAD FATONI                                   ', '999600018 ', 'Spesialis Utama - Div. Riset dan Pengembangan                                                       ', 'AKTIF', 'RISET DAN PENGEMBANGAN                            ', 'DIVISI RISET DAN PENGEMBANGAN'),
(223, 'TAKASHI EMOTO                                     ', '661800092 ', 'Staf PKWT (Gol V) Div. Riset dan Pengembangan                                                       ', 'AKTIF', 'RISET DAN PENGEMBANGAN                            ', 'DIVISI RISET DAN PENGEMBANGAN'),
(224, 'ALAN DARMASAPUTRA                                 ', '661900264 ', 'Staf PKWT (Gol I) Div. Riset dan Pengembangan                                                       ', 'AKTIF', 'RISET DAN PENGEMBANGAN                            ', 'DIVISI RISET DAN PENGEMBANGAN'),
(225, 'AVIEDYA BUNGA VEDANTA                             ', '662100001 ', 'Staf PKWT (Gol I) Div. Riset dan Pengembangan                                                       ', 'AKTIF', 'RISET DAN PENGEMBANGAN                            ', 'DIVISI RISET DAN PENGEMBANGAN'),
(226, 'APOLEUS KARO KARO                                 ', '999600020 ', 'Senior Manager Pengembangan Bisnis                                                                  ', 'AKTIF', 'RISET DAN PENGEMBANGAN                            ', 'PENGEMBANGAN BISNIS'),
(227, 'FAJRIYANSA PERDANA                                ', '661900014 ', 'Staf PKWT (Gol I) Dep. Pengembangan Bisnis                                                          ', 'AKTIF', 'RISET DAN PENGEMBANGAN                            ', 'PENGEMBANGAN BISNIS'),
(228, 'MOHAMMAD ISNAN AFFANDI                            ', '992100016 ', 'Staf (Gol I) Bag. Skema Bisnis dan Aliansi                                                          ', 'AKTIF', 'RISET DAN PENGEMBANGAN                            ', 'PENGEMBANGAN BISNIS'),
(229, 'NUUR AISYAH MURTI W                               ', '991000014 ', 'Pejabat Setingkat M Dep. Pengembangan Bisnis                                                        ', 'AKTIF', 'RISET DAN PENGEMBANGAN                            ', 'PENGEMBANGAN BISNIS'),
(230, 'MOCHAMMAD ATHUR AKBAR                             ', '991100011 ', 'Manager Riset Bisnis                                                                                ', 'AKTIF', 'RISET DAN PENGEMBANGAN                            ', 'PENGEMBANGAN BISNIS'),
(231, 'HENDRI DWI SAPUTRA                                ', '991700065 ', 'Staf (Gol II) Bag. Riset Bisnis                                                                     ', 'AKTIF', 'RISET DAN PENGEMBANGAN                            ', 'PENGEMBANGAN BISNIS'),
(232, 'RENI KURNIAWATI                                   ', '991100062 ', 'Manager Skema Bisnis dan Aliansi                                                                    ', 'AKTIF', 'RISET DAN PENGEMBANGAN                            ', 'PENGEMBANGAN BISNIS'),
(233, 'ANIS USTANTI                                      ', '991700054 ', 'Staf (Gol II) Bag. Skema Bisnis dan Aliansi                                                         ', 'AKTIF', 'RISET DAN PENGEMBANGAN                            ', 'PENGEMBANGAN BISNIS'),
(234, 'HEIDY ANGGRAINI PUTRI                             ', '991700064 ', 'Staf (Gol II) Bag. Skema Bisnis dan Aliansi                                                         ', 'AKTIF', 'RISET DAN PENGEMBANGAN                            ', 'PENGEMBANGAN BISNIS'),
(235, 'ADITIA RIANTO                                     ', '991800001 ', 'Staf (Gol II) Bag. Skema Bisnis dan Aliansi                                                         ', 'AKTIF', 'RISET DAN PENGEMBANGAN                            ', 'PENGEMBANGAN BISNIS'),
(236, 'TRIAJI WIDI ATMOJO                                ', '991100017 ', 'Senior Manager Pengembangan Produk dan Teknologi                                                    ', 'AKTIF', 'RISET DAN PENGEMBANGAN                            ', 'PENGEMBANGAN PRODUK DAN TEKNOLOGI'),
(237, 'PRELLY MAHARUMI                                   ', '991100059 ', 'Staf (Gol II) Dep. Pengembangan Produk dan Teknologi                                                ', 'AKTIF', 'RISET DAN PENGEMBANGAN                            ', 'PENGEMBANGAN PRODUK DAN TEKNOLOGI'),
(238, 'MUHAMAD HASAN HAFIFI                              ', '991600014 ', 'Staf (Gol II) Dep. Pengembangan Produk dan Teknologi                                                ', 'AKTIF', 'RISET DAN PENGEMBANGAN                            ', 'PENGEMBANGAN PRODUK DAN TEKNOLOGI'),
(239, 'RAISQI KUN HARTADI SAMIONO                        ', '661900242 ', 'Staf PKWT (Gol I) Dep. Pengembangan Produk dan Teknologi                                            ', 'AKTIF', 'RISET DAN PENGEMBANGAN                            ', 'PENGEMBANGAN PRODUK DAN TEKNOLOGI'),
(240, 'MUHAMMAD ADHA RAFLI                               ', '661900288 ', 'Staf PKWT (Gol I) Dep. Pengembangan Produk dan Teknologi                                            ', 'AKTIF', 'RISET DAN PENGEMBANGAN                            ', 'PENGEMBANGAN PRODUK DAN TEKNOLOGI'),
(241, 'ANANG FAKHRUDIN                                   ', '991100027 ', 'Manager Pengembangan Sistem Transportasi                                                            ', 'AKTIF', 'RISET DAN PENGEMBANGAN                            ', 'PENGEMBANGAN PRODUK DAN TEKNOLOGI'),
(242, 'BRENDA YUNIEL VINZA HERMAN                        ', '662200002 ', 'Staf PKWT (Gol I) Bag. Pengembangan Sistem Transportasi                                             ', 'AKTIF', 'RISET DAN PENGEMBANGAN                            ', 'PENGEMBANGAN PRODUK DAN TEKNOLOGI'),
(243, ' AFICENA HIMDANI ILMAM ABHARAN                    ', '992000007 ', 'Staf (Gol I) Bag. Pengembangan Strategi Teknologi dan Manufaktur                                    ', 'AKTIF', 'RISET DAN PENGEMBANGAN                            ', 'PENGEMBANGAN PRODUK DAN TEKNOLOGI'),
(244, 'FIFIN NUGROHO                                     ', '991400019 ', 'Staf (Gol II) Bag. Pengembangan Sistem Transportasi                                                 ', 'AKTIF', 'RISET DAN PENGEMBANGAN                            ', 'PENGEMBANGAN PRODUK DAN TEKNOLOGI'),
(245, 'ERWIN AVIANTO                                     ', '991600007 ', 'Staf (Gol II) Bag. Pengembangan Sistem Transportasi                                                 ', 'AKTIF', 'RISET DAN PENGEMBANGAN                            ', 'PENGEMBANGAN PRODUK DAN TEKNOLOGI'),
(246, 'DEDY KURNIAWAN                                    ', '991700045 ', 'Staf (Gol II) Bag. Pengembangan Sistem Transportasi                                                 ', 'AKTIF', 'RISET DAN PENGEMBANGAN                            ', 'PENGEMBANGAN PRODUK DAN TEKNOLOGI'),
(247, 'YUNIOR LANANG SATRIO                              ', '991800024 ', 'Staf (Gol II) Bag. Pengembangan Sistem Transportasi                                                 ', 'AKTIF', 'RISET DAN PENGEMBANGAN                            ', 'PENGEMBANGAN PRODUK DAN TEKNOLOGI'),
(248, 'ACHMAD SUGANDI                                    ', '992100002 ', 'Staf (Gol I) Bag. Pengembangan Sistem Transportasi                                                  ', 'AKTIF', 'RISET DAN PENGEMBANGAN                            ', 'PENGEMBANGAN PRODUK DAN TEKNOLOGI'),
(249, 'DEWI NALA HUSNA                                   ', '992100010 ', 'Staf (Gol I) Bag. Pengembangan Sistem Transportasi                                                  ', 'AKTIF', 'RISET DAN PENGEMBANGAN                            ', 'PENGEMBANGAN PRODUK DAN TEKNOLOGI'),
(250, 'SUWUN SETYANTO                                    ', '999900124 ', 'General Manager Pemasaran                                                                           ', 'AKTIF', 'PEMASARAN                                         ', 'DIVISI PEMASARAN'),
(251, 'DIAH AYU KRISTIANA                                ', '661800131 ', 'Staf PKWT (Gol I) Div. Pemasaran                                                                    ', 'AKTIF', 'PEMASARAN                                         ', 'DIVISI PEMASARAN'),
(252, 'DURRAH IZZA ZHARFANI                              ', '662200004 ', 'Staf PKWT (Gol I) Div. Pemasaran                                                                    ', 'AKTIF', 'PEMASARAN                                         ', 'DIVISI PEMASARAN'),
(253, 'AMARTIWI SONYA                                    ', '991400008 ', 'Staf (Gol II) Dep. Pemasaran Proyek                                                                 ', 'AKTIF', 'PEMASARAN                                         ', 'PEMASARAN PROYEK'),
(254, 'BAYU PRASETYO                                     ', '991400012 ', 'Staf (Gol II) Dep. Pemasaran Proyek                                                                 ', 'AKTIF', 'PEMASARAN                                         ', 'PEMASARAN PROYEK'),
(255, 'NANANG AGUNG ROHMANDIYAS                          ', '991900011 ', 'Staf (Gol I) Dep. Pemasaran Proyek                                                                  ', 'AKTIF', 'PEMASARAN                                         ', 'PEMASARAN PROYEK'),
(256, 'PUNKY ALHIMNI RUSYDI                              ', '992000011 ', 'Staf (Gol I) Dep. Pemasaran Proyek                                                                  ', 'AKTIF', 'PEMASARAN                                         ', 'PEMASARAN PROYEK'),
(257, 'SUPRIYOKO                                         ', '999900112 ', 'Spesialis Muda - Dep. Pemasaran Proyek                                                              ', 'AKTIF', 'PEMASARAN                                         ', 'PEMASARAN PROYEK'),
(258, 'AGUNG NUR MUHARROM                                ', '991100021 ', 'Manager Perencanaan dan Pengendalian Pemasaran                                                      ', 'AKTIF', 'PEMASARAN                                         ', 'PEMASARAN PROYEK'),
(259, 'WENDHY PRATAMA                                    ', '992100025 ', 'Staf (Gol I) Bag. Perencanaan dan Pengendalian Pemasaran                                            ', 'AKTIF', 'PEMASARAN                                         ', 'PEMASARAN PROYEK'),
(260, 'WISNU MELIANTO                                    ', '991100019 ', 'Staf (Gol II) Dep. Bid and Pricing                                                                  ', 'AKTIF', 'PEMASARAN                                         ', 'BID AND PRICING'),
(261, 'BESTYA RIZKA WIJAYA                               ', '991500021 ', 'Staf (Gol II) Dep. Bid and Pricing                                                                  ', 'AKTIF', 'PEMASARAN                                         ', 'BID AND PRICING'),
(262, 'ELOK FATCHIYATI                                   ', '991100042 ', 'Spesialis Muda - Dep. Bid and Pricing                                                               ', 'AKTIF', 'PEMASARAN                                         ', 'BID AND PRICING'),
(263, 'DIAREKTA ADI PUTRI                                ', '661900267 ', 'Staf PKWT (Gol I) Dep. Bid and Pricing                                                              ', 'AKTIF', 'PEMASARAN                                         ', 'BID AND PRICING'),
(264, 'RESTU MUSTAQIM                                    ', '661900275 ', 'Staf PKWT (Gol I) Dep. Bid and Pricing                                                              ', 'AKTIF', 'PEMASARAN                                         ', 'BID AND PRICING'),
(265, 'APRIDITA DWI SETIAHADI                            ', '991200008 ', 'Manager Bid Preparation and Administration                                                          ', 'AKTIF', 'PEMASARAN                                         ', 'BID AND PRICING'),
(266, 'NANANG DWI PRASETIYO                              ', '991100012 ', 'Staf (Gol II) Bag. Bid Preparation and Administration                                               ', 'AKTIF', 'PEMASARAN                                         ', 'BID AND PRICING'),
(267, 'NUNGKI KUS LUDFIANI                               ', '991400001 ', 'Staf (Gol II) Bag. Bid Preparation and Administration                                               ', 'AKTIF', 'PEMASARAN                                         ', 'BID AND PRICING'),
(268, 'ONNY MARETHA PRASTYA                              ', '991500005 ', 'Staf (Gol II) Bag. Bid Preparation and Administration                                               ', 'AKTIF', 'PEMASARAN                                         ', 'BID AND PRICING'),
(269, 'DEDY AGUS SETIAWAN                                ', '991800006 ', 'Staf (Gol II) Bag. Bid Preparation and Administration                                               ', 'AKTIF', 'PEMASARAN                                         ', 'BID AND PRICING'),
(270, 'BHAYANGKARA KUMANDANG J                           ', '991700055 ', 'Staf (Gol II) Bag. Pricing and Project Order Controlling                                            ', 'AKTIF', 'PEMASARAN                                         ', 'BID AND PRICING'),
(271, 'INGGIT WAHYU PRASETIO                             ', '991000009 ', 'Senior Manager Manajemen Rantai Pasokan                                                             ', 'AKTIF', 'PEMASARAN                                         ', 'MANAJEMEN RANTAI PASOKAN'),
(272, 'RIO FITRI SUSANTI                                 ', '661800152 ', 'Staf PKWT (Gol I) Dep. Manajemen Rantai Pasokan                                                     ', 'AKTIF', 'PEMASARAN                                         ', 'MANAJEMEN RANTAI PASOKAN'),
(273, 'MESTUTI INTAN CHOIRUNNISA                         ', '661900021 ', 'Staf PKWT (Gol I) Dep. Manajemen Rantai Pasokan                                                     ', 'AKTIF', 'PEMASARAN                                         ', 'MANAJEMEN RANTAI PASOKAN'),
(274, 'WINDYA SAPUTRO                                    ', '661900028 ', 'Staf PKWT (Gol I) Dep. Manajemen Rantai Pasokan                                                     ', 'AKTIF', 'PEMASARAN                                         ', 'MANAJEMEN RANTAI PASOKAN'),
(275, 'NADIA RAHMAWATI                                   ', '661900048 ', 'Staf PKWT (Gol I) Dep. Manajemen Rantai Pasokan                                                     ', 'AKTIF', 'PEMASARAN                                         ', 'MANAJEMEN RANTAI PASOKAN'),
(276, 'FENDY SUSENO                                      ', '661900052 ', 'Staf PKWT (Gol I) Dep. Manajemen Rantai Pasokan                                                     ', 'AKTIF', 'PEMASARAN                                         ', 'MANAJEMEN RANTAI PASOKAN'),
(277, 'ALLY EVAYURI                                      ', '990100006 ', 'Staf (Gol I) Bag. Pengendalian dan Evaluasi Rantai Pasokan                                          ', 'AKTIF', 'PEMASARAN                                         ', 'MANAJEMEN RANTAI PASOKAN'),
(278, 'AJI WIRA PRADHANA                                 ', '992000001 ', 'Staf (Gol I) Bag. Pengendalian dan Evaluasi Rantai Pasokan                                          ', 'AKTIF', 'PEMASARAN                                         ', 'MANAJEMEN RANTAI PASOKAN'),
(279, 'WAHYU YOGA UTAMA                                  ', '992100024 ', 'Staf (Gol I) Bag. Pengendalian dan Evaluasi Rantai Pasokan                                          ', 'AKTIF', 'PEMASARAN                                         ', 'MANAJEMEN RANTAI PASOKAN'),
(280, 'DADANG TRIAWAN                                    ', '992100009 ', 'Staf (Gol I) Bag. Perencanaan dan Pengadaan Rantai Pasokan                                          ', 'AKTIF', 'PEMASARAN                                         ', 'MANAJEMEN RANTAI PASOKAN'),
(281, 'SRI UTAMI                                         ', '999200049 ', 'Staf (Gol I) Bag. Perencanaan dan Pengadaan Rantai Pasokan                                          ', 'AKTIF', 'PEMASARAN                                         ', 'MANAJEMEN RANTAI PASOKAN'),
(282, 'IHSAN HERI ACHMADI                                ', '991100010 ', 'Manager Pengendalian dan Evaluasi Rantai Pasokan                                                    ', 'AKTIF', 'PEMASARAN                                         ', 'MANAJEMEN RANTAI PASOKAN'),
(283, 'DARMANTO ZAENAL                                   ', '991200012 ', 'Staf (Gol II) Bag. Pengendalian dan Evaluasi Rantai Pasokan                                         ', 'AKTIF', 'PEMASARAN                                         ', 'MANAJEMEN RANTAI PASOKAN'),
(284, 'MOCH RYAN ARDIANSYAH                              ', '991800017 ', 'Staf (Gol II) Bag. Pengendalian dan Evaluasi Rantai Pasokan                                         ', 'AKTIF', 'PEMASARAN                                         ', 'MANAJEMEN RANTAI PASOKAN'),
(285, 'BAGOES IMAN PRAKOSO                               ', '991900001 ', 'Staf (Gol II) Bag. Pengendalian dan Evaluasi Rantai Pasokan                                         ', 'AKTIF', 'PEMASARAN                                         ', 'MANAJEMEN RANTAI PASOKAN');
INSERT INTO `pegawai` (`id_pegawai`, `nama_pegawai`, `NIP`, `jabatan_pegawai`, `Status`, `Divisi`, `Departemen`) VALUES
(286, 'NUR RIANA FAJARWATI                               ', '991400002 ', 'Manager Perencanaan dan Pengadaan Rantai Pasokan                                                    ', 'AKTIF', 'PEMASARAN                                         ', 'MANAJEMEN RANTAI PASOKAN'),
(287, 'KRISNAWATI                                        ', '991500003 ', 'Staf (Gol II) Bag. Perencanaan dan Pengadaan Rantai Pasokan                                         ', 'AKTIF', 'PEMASARAN                                         ', 'MANAJEMEN RANTAI PASOKAN'),
(288, 'YUKIE OKTAVIANTY                                  ', '991500004 ', 'Staf (Gol II) Bag. Perencanaan dan Pengadaan Rantai Pasokan                                         ', 'AKTIF', 'PEMASARAN                                         ', 'MANAJEMEN RANTAI PASOKAN'),
(289, 'RUSDIANA RIZKY ANGGRAINI                          ', '991500013 ', 'Staf (Gol II) Bag. Perencanaan dan Pengadaan Rantai Pasokan                                         ', 'AKTIF', 'PEMASARAN                                         ', 'MANAJEMEN RANTAI PASOKAN'),
(290, 'AGUS PRIANTO                                      ', '999200002 ', 'Staf (Gol II) Bag. Perencanaan dan Pengadaan Rantai Pasokan                                         ', 'AKTIF', 'PEMASARAN                                         ', 'MANAJEMEN RANTAI PASOKAN'),
(291, 'APRILIANDY FAJAR SYAH PUTRA                       ', '991800027 ', 'Staf (Gol I) Bag. Perencanaan dan Pengadaan Rantai Pasokan                                          ', 'AKTIF', 'PEMASARAN                                         ', 'MANAJEMEN RANTAI PASOKAN'),
(292, 'FEBRY PANDU WIJAYA                                ', '990900009 ', 'Kepala SBU Tier 1 dan 2                                                                             ', 'AKTIF', 'SBU TIER 1 DAN 2                                  ', 'SBU TIER 1 DAN 2 '),
(293, 'DIAN RAHMAT FAUZI                                 ', '991100038 ', 'Pejabat Setingkat M SBU Tier 1 dan 2                                                                ', 'AKTIF', 'SBU TIER 1 DAN 2                                  ', 'SBU TIER 1 DAN 2 '),
(294, 'OLDVIKA NURMA MAS\'UDI                             ', '661900274 ', 'Staf PKWT (Gol I) SBU Tier 1 dan 2                                                                  ', 'AKTIF', 'SBU TIER 1 DAN 2                                  ', 'SBU TIER 1 DAN 2 '),
(295, 'YULIZAR EDO PRATAMA CORDOVA                       ', '662200008 ', 'Staf PKWT (Gol I) SBU Tier 1 dan 2                                                                  ', 'AKTIF', 'SBU TIER 1 DAN 2                                  ', 'SBU TIER 1 DAN 2 '),
(296, 'ARIF MUHAIMIN                                     ', '999600007 ', 'General Manager SDM dan GA                                                                          ', 'AKTIF', 'SDM DAN GA                                        ', 'DIVISI SDM DAN GA'),
(297, 'FANNY ANANDITA MULYAWARDHANI                      ', '661900015 ', 'Staf PKWT (Gol I) Div. SDM dan GA                                                                   ', 'AKTIF', 'SDM DAN GA                                        ', 'DIVISI SDM DAN GA'),
(298, 'SIGIT SUGIARTO                                    ', '999800002 ', 'Senior Manager Perencanaan, Pengelolaan dan Pengembangan SDM                                        ', 'AKTIF', 'SDM DAN GA                                        ', 'PERENCANAAN, PENGELOLAAN DAN PENGEMBANGAN SDM'),
(299, 'DENI WULANDANI                                    ', '999900117 ', 'Spesialis Muda - Dep. Perencanaan, Pengelolaan dan Pengembangan SDM                                 ', 'AKTIF', 'SDM DAN GA                                        ', 'PERENCANAAN, PENGELOLAAN DAN PENGEMBANGAN SDM'),
(300, 'BEFILLA ASTIKA PUTRI                              ', '661710380 ', 'Staf PKWT (Gol I) Dep. Perencanaan, Pengelolaan dan Pengembangan SDM                                ', 'AKTIF', 'SDM DAN GA                                        ', 'PERENCANAAN, PENGELOLAAN DAN PENGEMBANGAN SDM'),
(301, 'REZA AGUNG PRIAMBODO                              ', '661900293 ', 'Staf PKWT (Gol I) Dep. Perencanaan, Pengelolaan dan Pengembangan SDM                                ', 'AKTIF', 'SDM DAN GA                                        ', 'PERENCANAAN, PENGELOLAAN DAN PENGEMBANGAN SDM'),
(302, 'RANGGA SUKMANTARA                                 ', '991200027 ', 'Manager Perencanaan dan Pengelolaan SDM                                                             ', 'AKTIF', 'SDM DAN GA                                        ', 'PERENCANAAN, PENGELOLAAN DAN PENGEMBANGAN SDM'),
(303, 'DYAH KARTIKA ARIANTI                              ', '991100003 ', 'Staf (Gol II) Bag. Perencanaan dan Pengelolaan SDM                                                  ', 'AKTIF', 'SDM DAN GA                                        ', 'PERENCANAAN, PENGELOLAAN DAN PENGEMBANGAN SDM'),
(304, 'NURLAILA ANISAHWATI                               ', '991900012 ', 'Staf (Gol II) Bag. Pengembangan SDM dan Diklat                                                      ', 'AKTIF', 'SDM DAN GA                                        ', 'PERENCANAAN, PENGELOLAAN DAN PENGEMBANGAN SDM'),
(305, 'SASKIA METTASARI                                  ', '991900015 ', 'Staf (Gol II) Bag. Pengembangan SDM dan Diklat                                                      ', 'AKTIF', 'SDM DAN GA                                        ', 'PERENCANAAN, PENGELOLAAN DAN PENGEMBANGAN SDM'),
(306, 'REZA DENNYZA SATRIAWAN                            ', '991500018 ', 'Staf (Gol I) Bag. Pengembangan SDM dan Diklat                                                       ', 'AKTIF', 'SDM DAN GA                                        ', 'PERENCANAAN, PENGELOLAAN DAN PENGEMBANGAN SDM'),
(307, 'CANIA PUTRI ASRI                                  ', '991500016 ', 'Staf (Gol II) Bag. Perencanaan dan Pengelolaan SDM                                                  ', 'AKTIF', 'SDM DAN GA                                        ', 'PERENCANAAN, PENGELOLAAN DAN PENGEMBANGAN SDM'),
(308, 'KUSHARMANTO                                       ', '999900110 ', 'Staf (Gol I) Bag. Perencanaan dan Pengelolaan SDM                                                   ', 'AKTIF', 'SDM DAN GA                                        ', 'PERENCANAAN, PENGELOLAAN DAN PENGEMBANGAN SDM'),
(309, 'DANDIK BUDI PRASETYO                              ', '662000004 ', 'Staf PKWT (Gol I) Bag. Perencanaan dan Pengelolaan SDM                                              ', 'AKTIF', 'SDM DAN GA                                        ', 'PERENCANAAN, PENGELOLAAN DAN PENGEMBANGAN SDM'),
(310, 'SASKIA DWININDA OKTAVIA                           ', '662200007 ', 'Staf PKWT (Gol I) Bag. Perencanaan dan Pengelolaan SDM                                              ', 'AKTIF', 'SDM DAN GA                                        ', 'PERENCANAAN, PENGELOLAAN DAN PENGEMBANGAN SDM'),
(311, 'INSTIKA DANI                                      ', '991200022 ', 'Manager Pengembangan SDM dan Diklat                                                                 ', 'AKTIF', 'SDM DAN GA                                        ', 'PERENCANAAN, PENGELOLAAN DAN PENGEMBANGAN SDM'),
(312, 'EKIN AYU SAPUTRI                                  ', '990900008 ', 'Staf (Gol II) Bag. Pengembangan SDM dan Diklat                                                      ', 'AKTIF', 'SDM DAN GA                                        ', 'PERENCANAAN, PENGELOLAAN DAN PENGEMBANGAN SDM'),
(313, 'EGIE WENDRA APRILIAWAN                            ', '991100041 ', 'Spesialis Muda - Dep. Kesejahteraan SDM dan Hubungan Industrial                                     ', 'AKTIF', 'SDM DAN GA                                        ', 'KESEJAHTERAAN SDM DAN HUBUNGAN INDUSTRIAL'),
(314, 'DWIANTO BAYU SUSANTO                              ', '661710329 ', 'Staf PKWT (Gol I) Dep. Kesejahteraan SDM dan Hubungan Industrial                                    ', 'AKTIF', 'SDM DAN GA                                        ', 'KESEJAHTERAAN SDM DAN HUBUNGAN INDUSTRIAL'),
(315, 'YUDIANTO                                          ', '999800010 ', 'Manager Kesejahteraan SDM                                                                           ', 'AKTIF', 'SDM DAN GA                                        ', 'KESEJAHTERAAN SDM DAN HUBUNGAN INDUSTRIAL'),
(316, 'RATNA JUWITA                                      ', '991500017 ', 'Staf (Gol II) Bag. Kesejahteraan SDM                                                                ', 'AKTIF', 'SDM DAN GA                                        ', 'KESEJAHTERAAN SDM DAN HUBUNGAN INDUSTRIAL'),
(317, 'SUSI AMBARSARI                                    ', '999600057 ', 'Staf (Gol I) Bag. Kesejahteraan SDM                                                                 ', 'AKTIF', 'SDM DAN GA                                        ', 'KESEJAHTERAAN SDM DAN HUBUNGAN INDUSTRIAL'),
(318, 'AGUS SAMSI                                        ', '999200003 ', 'Manager Hubin dan HRIS                                                                              ', 'AKTIF', 'SDM DAN GA                                        ', 'KESEJAHTERAAN SDM DAN HUBUNGAN INDUSTRIAL'),
(319, 'DARMANTO                                          ', '999800144 ', 'Staf (Gol I) Bag. Hubin dan HRIS                                                                    ', 'AKTIF', 'SDM DAN GA                                        ', 'KESEJAHTERAAN SDM DAN HUBUNGAN INDUSTRIAL'),
(320, 'DEDIE HERY SETIAWAN                               ', '999800184 ', 'Staf (Gol I) Bag. Hubin dan HRIS                                                                    ', 'AKTIF', 'SDM DAN GA                                        ', 'KESEJAHTERAAN SDM DAN HUBUNGAN INDUSTRIAL'),
(321, 'SUDIONO                                           ', '999900035 ', 'Staf (Gol I) Bag. Hubin dan HRIS                                                                    ', 'AKTIF', 'SDM DAN GA                                        ', 'KESEJAHTERAAN SDM DAN HUBUNGAN INDUSTRIAL'),
(322, 'SUROTO AGUS CAHYONO                               ', '999900057 ', 'Staf (Gol I) Bag. Hubin dan HRIS                                                                    ', 'AKTIF', 'SDM DAN GA                                        ', 'KESEJAHTERAAN SDM DAN HUBUNGAN INDUSTRIAL'),
(323, 'RETNO PURBOWATI                                   ', '999500001 ', 'Senior Manager General Affairs                                                                      ', 'AKTIF', 'SDM DAN GA                                        ', 'GENERAL AFFAIRS'),
(324, 'ADI CAHYA                                         ', '661900049 ', 'Staf PKWT (Gol I) Dep. General Affairs                                                              ', 'AKTIF', 'SDM DAN GA                                        ', 'GENERAL AFFAIRS'),
(325, 'ALZAIS SAFII                                      ', '661900050 ', 'Staf PKWT (Gol I) Dep. General Affairs                                                              ', 'AKTIF', 'SDM DAN GA                                        ', 'GENERAL AFFAIRS'),
(326, 'ABEL NIZAR SAVERO                                 ', '661900107 ', 'Staf PKWT (Gol I) Dep. General Affairs                                                              ', 'AKTIF', 'SDM DAN GA                                        ', 'GENERAL AFFAIRS'),
(327, 'KHOIRUL ISTIKANA                                  ', '661900110 ', 'Staf PKWT (Gol I) Dep. General Affairs                                                              ', 'AKTIF', 'SDM DAN GA                                        ', 'GENERAL AFFAIRS'),
(328, 'IIS MEI MAHA RANI                                 ', '991100048 ', 'Manager Tata Usaha Aset dan Pengelolaan Aset                                                        ', 'AKTIF', 'SDM DAN GA                                        ', 'GENERAL AFFAIRS'),
(329, 'SUMARYONO                                         ', '999900048 ', 'Staf (Gol I) Bag. Umum dan Keamanan                                                                 ', 'AKTIF', 'SDM DAN GA                                        ', 'GENERAL AFFAIRS'),
(330, 'HENNY KARTIKASARI KUSUMANINGRUM                   ', '999600059 ', 'Staf (Gol II) Bag. Umum dan Keamanan                                                                ', 'AKTIF', 'SDM DAN GA                                        ', 'GENERAL AFFAIRS'),
(331, 'PRASETYO                                          ', '999800106 ', 'Staf (Gol II) Bag. Umum dan Keamanan                                                                ', 'AKTIF', 'SDM DAN GA                                        ', 'GENERAL AFFAIRS'),
(332, 'YAYAN ARIFANDI                                    ', '992100026 ', 'Staf (Gol I) Bag. Umum dan Keamanan                                                                 ', 'AKTIF', 'SDM DAN GA                                        ', 'GENERAL AFFAIRS'),
(333, 'MARIYANTO                                         ', '999200005 ', 'Staf (Gol I) Bag. Umum dan Keamanan                                                                 ', 'AKTIF', 'SDM DAN GA                                        ', 'GENERAL AFFAIRS'),
(334, 'AROFIK                                            ', '999800082 ', 'Staf (Gol I) Bag. Umum dan Keamanan                                                                 ', 'AKTIF', 'SDM DAN GA                                        ', 'GENERAL AFFAIRS'),
(335, 'BUDIJONO                                          ', '999800170 ', 'Staf (Gol I) Bag. Umum dan Keamanan                                                                 ', 'AKTIF', 'SDM DAN GA                                        ', 'GENERAL AFFAIRS'),
(336, 'ROSSY NAIN NOPAN JUWARI                           ', '992000013 ', 'Staf (Gol I) Bag. Tata Usaha Aset dan Pengelolaan Aset                                              ', 'AKTIF', 'SDM DAN GA                                        ', 'GENERAL AFFAIRS'),
(337, 'EVELYN DELLAROSA                                  ', '992100011 ', 'Staf (Gol I) Bag. Tata Usaha Aset dan Pengelolaan Aset                                              ', 'AKTIF', 'SDM DAN GA                                        ', 'GENERAL AFFAIRS'),
(338, 'NURYANTO                                          ', '999400036 ', 'Staf (Gol I) Bag. Tata Usaha Aset dan Pengelolaan Aset                                              ', 'AKTIF', 'SDM DAN GA                                        ', 'GENERAL AFFAIRS'),
(339, 'SUKARDI                                           ', '999600049 ', 'Staf (Gol I) Bag. Tata Usaha Aset dan Pengelolaan Aset                                              ', 'AKTIF', 'SDM DAN GA                                        ', 'GENERAL AFFAIRS'),
(340, 'WINARTO                                           ', '999800129 ', 'Manager Umum dan Keamanan                                                                           ', 'AKTIF', 'SDM DAN GA                                        ', 'GENERAL AFFAIRS'),
(341, 'YOANES SUGIYANTO                                  ', '999200044 ', 'Staf (Gol II) Bag. Umum dan Keamanan                                                                ', 'AKTIF', 'SDM DAN GA                                        ', 'GENERAL AFFAIRS'),
(342, 'PUGUH DWI TJAHJONO                                ', '999900127 ', 'General Manager Sekretaris Perusahaan                                                               ', 'AKTIF', 'SEKRETARIS PERUSAHAAN                             ', 'DIVISI SEKRETARIS PERUSAHAAN'),
(343, 'BAMBANG RAMADHIARTO DAKUNG                        ', '999600015 ', 'Senior Manager TJSL dan Stakeholder Relationship                                                    ', 'AKTIF', 'SEKRETARIS PERUSAHAAN                             ', 'TJSL DAN STAKEHOLDER RELATIONSHIP'),
(344, 'SUGENG BUDI SETIJONO                              ', '999400061 ', 'Spesialis Muda - Dep. TJSL dan Stakeholder Relationship                                             ', 'AKTIF', 'SEKRETARIS PERUSAHAAN                             ', 'TJSL DAN STAKEHOLDER RELATIONSHIP'),
(345, 'DINDHA VITRI PRIMADINI                            ', '661710328 ', 'Staf PKWT (Gol I) Dep. TJSL dan Stakeholder Relationship                                            ', 'AKTIF', 'SEKRETARIS PERUSAHAAN                             ', 'TJSL DAN STAKEHOLDER RELATIONSHIP'),
(346, 'JUVAN SEPTANIGA                                   ', '661800115 ', 'Staf PKWT (Gol I) Dep. TJSL dan Stakeholder Relationship                                            ', 'AKTIF', 'SEKRETARIS PERUSAHAAN                             ', 'TJSL DAN STAKEHOLDER RELATIONSHIP'),
(347, 'BASTIAN NUR RACHMAD MAULADY                       ', '661900283 ', 'Staf PKWT (Gol I) Dep. TJSL dan Stakeholder Relationship                                            ', 'AKTIF', 'SEKRETARIS PERUSAHAAN                             ', 'TJSL DAN STAKEHOLDER RELATIONSHIP'),
(348, 'ANDRIANTO                                         ', '991100028 ', 'Manager Pelaporan Perusahaan                                                                        ', 'AKTIF', 'SEKRETARIS PERUSAHAAN                             ', 'TJSL DAN STAKEHOLDER RELATIONSHIP'),
(349, 'MUHAMMAD IMAMUDDIN WAHID                          ', '662100011 ', 'Staf PKWT (Gol I) Bag. Humas dan Protokoler                                                         ', 'AKTIF', 'SEKRETARIS PERUSAHAAN                             ', 'TJSL DAN STAKEHOLDER RELATIONSHIP'),
(350, 'NOERHIDAYATI                                      ', '999100027 ', 'Manager TJSL                                                                                        ', 'AKTIF', 'SEKRETARIS PERUSAHAAN                             ', 'TJSL DAN STAKEHOLDER RELATIONSHIP'),
(351, 'CANDRA WULAN SARI                                 ', '991900022 ', 'Staf (Gol II) Bag. TJSL                                                                             ', 'AKTIF', 'SEKRETARIS PERUSAHAAN                             ', 'TJSL DAN STAKEHOLDER RELATIONSHIP'),
(352, 'BAMBANG SUPRIYADI                                 ', '999600055 ', 'Staf (Gol II) Bag. TJSL                                                                             ', 'AKTIF', 'SEKRETARIS PERUSAHAAN                             ', 'TJSL DAN STAKEHOLDER RELATIONSHIP'),
(353, 'ARTHARINI                                         ', '992100007 ', 'Staf (Gol I) Bag. TJSL                                                                              ', 'AKTIF', 'SEKRETARIS PERUSAHAAN                             ', 'TJSL DAN STAKEHOLDER RELATIONSHIP'),
(354, 'SETYO PURNOMO                                     ', '999800047 ', 'Staf (Gol I) Bag. TJSL                                                                              ', 'AKTIF', 'SEKRETARIS PERUSAHAAN                             ', 'TJSL DAN STAKEHOLDER RELATIONSHIP'),
(355, 'DIAH LAILI RAHMAWATI                              ', '991500015 ', 'Staf (Gol II) Bag. Pelaporan Perusahaan                                                             ', 'AKTIF', 'SEKRETARIS PERUSAHAAN                             ', 'TJSL DAN STAKEHOLDER RELATIONSHIP'),
(356, 'NIDYA MARCHIKA SUDARSONO                          ', '992000003 ', 'Staf (Gol I) Bag. Pelaporan Perusahaan                                                              ', 'AKTIF', 'SEKRETARIS PERUSAHAAN                             ', 'TJSL DAN STAKEHOLDER RELATIONSHIP'),
(357, 'MUHAMMAD ADVIN HIDAYAT                            ', '991400011 ', 'Manager Humas dan Protokoler                                                                        ', 'AKTIF', 'SEKRETARIS PERUSAHAAN                             ', 'TJSL DAN STAKEHOLDER RELATIONSHIP'),
(358, 'DEA AYU SITORESMY                                 ', '991900023 ', 'Staf (Gol I) Bag. Humas dan Protokoler                                                              ', 'AKTIF', 'SEKRETARIS PERUSAHAAN                             ', 'TJSL DAN STAKEHOLDER RELATIONSHIP'),
(359, 'RIJAL SHOLIH                                      ', '992100018 ', 'Staf (Gol I) Bag. Humas dan Protokoler                                                              ', 'AKTIF', 'SEKRETARIS PERUSAHAAN                             ', 'TJSL DAN STAKEHOLDER RELATIONSHIP'),
(360, 'DIMAS PRATAMA                                     ', '662100010 ', 'Staf PKWT (Gol I) Bag. Humas dan Protokoler                                                         ', 'AKTIF', 'SEKRETARIS PERUSAHAAN                             ', 'TJSL DAN STAKEHOLDER RELATIONSHIP'),
(361, 'AGUNG DWI CAHYONO                                 ', '999300009 ', 'Senior Manager Kantor Perwakilan dan Kesekretariatan                                                ', 'AKTIF', 'SEKRETARIS PERUSAHAAN                             ', 'KANTOR PERWAKILAN DAN KESEKRETARIATAN'),
(362, 'ZULFA ILMA SABILA                                 ', '661710385 ', 'Staf PKWT (Gol I) Dep. Kantor Perwakilan dan Kesekretariatan                                        ', 'AKTIF', 'SEKRETARIS PERUSAHAAN                             ', 'KANTOR PERWAKILAN DAN KESEKRETARIATAN'),
(363, 'SHINTHYA ANISA DEWI                               ', '661800133 ', 'Staf PKWT (Gol I) Dep. Kantor Perwakilan dan Kesekretariatan                                        ', 'AKTIF', 'SEKRETARIS PERUSAHAAN                             ', 'KANTOR PERWAKILAN DAN KESEKRETARIATAN'),
(364, 'RISKA WINIAYU ANGGRAIDA                           ', '661900106 ', 'Staf PKWT (Gol I) Dep. Kantor Perwakilan dan Kesekretariatan                                        ', 'AKTIF', 'SEKRETARIS PERUSAHAAN                             ', 'KANTOR PERWAKILAN DAN KESEKRETARIATAN'),
(365, 'PUTRI AYU JULIAN SETIYANI                         ', '661900262 ', 'Staf PKWT (Gol I) Dep. Kantor Perwakilan dan Kesekretariatan                                        ', 'AKTIF', 'SEKRETARIS PERUSAHAAN                             ', 'KANTOR PERWAKILAN DAN KESEKRETARIATAN'),
(366, 'DIDIK HENDRIATNA                                  ', '999600060 ', 'Manager Kesekretariatan dan Kearsipan                                                               ', 'AKTIF', 'SEKRETARIS PERUSAHAAN                             ', 'KANTOR PERWAKILAN DAN KESEKRETARIATAN'),
(367, 'RINA HARYATI                                      ', '992100019 ', 'Staf (Gol I) Bag. Kantor Perwakilan Banyuwangi                                                      ', 'AKTIF', 'SEKRETARIS PERUSAHAAN                             ', 'KANTOR PERWAKILAN DAN KESEKRETARIATAN'),
(368, 'FARIS MOHAMAD HADI MINARNA                        ', '661900269 ', 'Staf PKWT (Gol I) Bag. Kantor Perwakilan Banyuwangi                                                 ', 'AKTIF', 'SEKRETARIS PERUSAHAAN                             ', 'KANTOR PERWAKILAN DAN KESEKRETARIATAN'),
(369, 'FEBRIAN CITRA ASTUTI                              ', '661800157 ', 'Staf PKWT (Gol I) Bag. Kantor Perwakilan Jakarta dan Bandung                                        ', 'AKTIF', 'SEKRETARIS PERUSAHAAN                             ', 'KANTOR PERWAKILAN DAN KESEKRETARIATAN'),
(370, 'HEFNI ADIF                                        ', '662200010 ', 'Staf PKWT (Gol I) Bag. Kantor Perwakilan Jakarta dan Bandung                                        ', 'AKTIF', 'SEKRETARIS PERUSAHAAN                             ', 'KANTOR PERWAKILAN DAN KESEKRETARIATAN'),
(371, 'ANDY HERMAWAN SETYANTO                            ', '991100029 ', 'Manager Kantor Perwakilan Banyuwangi                                                                ', 'AKTIF', 'SEKRETARIS PERUSAHAAN                             ', 'KANTOR PERWAKILAN DAN KESEKRETARIATAN'),
(372, 'FACHRI AULIA                                      ', '991800011 ', 'Staf (Gol I) Bag. Kantor Perwakilan Banyuwangi                                                      ', 'AKTIF', 'SEKRETARIS PERUSAHAAN                             ', 'KANTOR PERWAKILAN DAN KESEKRETARIATAN'),
(373, 'ICHSANUL ARIEF                                    ', '991900005 ', 'Staf (Gol I) Bag. Kantor Perwakilan Banyuwangi                                                      ', 'AKTIF', 'SEKRETARIS PERUSAHAAN                             ', 'KANTOR PERWAKILAN DAN KESEKRETARIATAN'),
(374, 'PUPUT INDRAYATNO                                  ', '991900014 ', 'Staf (Gol I) Bag. Kantor Perwakilan Banyuwangi                                                      ', 'AKTIF', 'SEKRETARIS PERUSAHAAN                             ', 'KANTOR PERWAKILAN DAN KESEKRETARIATAN'),
(375, 'DELIMA WAHYUNINGTYAS                              ', '991500008 ', 'Staf (Gol II) Bag. Kesekretariatan dan Kearsipan                                                    ', 'AKTIF', 'SEKRETARIS PERUSAHAAN                             ', 'KANTOR PERWAKILAN DAN KESEKRETARIATAN'),
(376, 'ANUGRAH RIZKY NOVAN PRADANA                       ', '991500023 ', 'Staf (Gol II) Bag. Kesekretariatan dan Kearsipan                                                    ', 'AKTIF', 'SEKRETARIS PERUSAHAAN                             ', 'KANTOR PERWAKILAN DAN KESEKRETARIATAN'),
(377, 'FITTA FRIDDITTA ARIEF                             ', '991700061 ', 'Staf (Gol II) Bag. Kesekretariatan dan Kearsipan                                                    ', 'AKTIF', 'SEKRETARIS PERUSAHAAN                             ', 'KANTOR PERWAKILAN DAN KESEKRETARIATAN'),
(378, 'PRAYOGO                                           ', '991700047 ', 'Staf (Gol II) Bag. Kantor Perwakilan Jakarta dan Bandung                                            ', 'AKTIF', 'SEKRETARIS PERUSAHAAN                             ', 'KANTOR PERWAKILAN DAN KESEKRETARIATAN'),
(379, 'LILIK SUBANDHI                                    ', '999300014 ', 'Staf (Gol II) Bag. Kantor Perwakilan Jakarta dan Bandung                                            ', 'AKTIF', 'SEKRETARIS PERUSAHAAN                             ', 'KANTOR PERWAKILAN DAN KESEKRETARIATAN'),
(380, 'OMON SUGANDA                                      ', '999300018 ', 'Staf (Gol II) Bag. Kantor Perwakilan Jakarta dan Bandung                                            ', 'AKTIF', 'SEKRETARIS PERUSAHAAN                             ', 'KANTOR PERWAKILAN DAN KESEKRETARIATAN'),
(381, 'WAHYU MEIDIANTO CAHYONO                           ', '991100069 ', 'Pejabat Setingkat M Yang Diperbantukan Di Anak Perusahaan/Afiliasi PT Stadler INKA Indonesia        ', 'AKTIF', 'STADLER INKA INDONESIA                            ', 'STADLER INKA INDONESIA'),
(382, 'SUKOROTO                                          ', '999500013 ', 'General Manager Satuan Pengawasan Intern (SPI)                                                      ', 'AKTIF', 'SATUAN PENGAWASAN INTERN                          ', 'DIVISI SATUAN PENGAWASAN INTERN'),
(383, 'IIS MARATUS SOLICHAH                              ', '662200005 ', 'Staf PKWT (Gol I) Div. Satuan Pengawasan Intern (SPI)                                               ', 'AKTIF', 'SATUAN PENGAWASAN INTERN                          ', 'DIVISI SATUAN PENGAWASAN INTERN'),
(384, 'MUFID AL HABIB                                    ', '999500010 ', 'Senior Manager Audit Operasional                                                                    ', 'AKTIF', 'SATUAN PENGAWASAN INTERN                          ', 'AUDIT OPERASIONAL'),
(385, 'GODROEPT OKTABRYA SIAPUL HAMI HUTABARAT           ', '999300005 ', 'Staf (Gol III) Dep. Audit Operasional                                                               ', 'AKTIF', 'SATUAN PENGAWASAN INTERN                          ', 'AUDIT OPERASIONAL'),
(386, 'STIAJI TRIADE PRIANTORO                           ', '991900017 ', 'Staf (Gol II) Dep. Audit Operasional                                                                ', 'AKTIF', 'SATUAN PENGAWASAN INTERN                          ', 'AUDIT OPERASIONAL'),
(387, 'TRI YUWONO                                        ', '999600014 ', 'Staf (Gol II) Dep. Audit Operasional                                                                ', 'AKTIF', 'SATUAN PENGAWASAN INTERN                          ', 'AUDIT OPERASIONAL'),
(388, 'JOGI FAHRISAL RAMADHAN HARAHAP                    ', '992100014 ', 'Staf (Gol I) Dep. Audit Operasional                                                                 ', 'AKTIF', 'SATUAN PENGAWASAN INTERN                          ', 'AUDIT OPERASIONAL'),
(389, 'MUHAMAD NUGRAHA                                   ', '991500001 ', 'Spesialis Muda -Dep. Audit Operasional                                                              ', 'AKTIF', 'SATUAN PENGAWASAN INTERN                          ', 'AUDIT OPERASIONAL'),
(390, 'IVING ARISDIYOTO                                  ', '661900300 ', 'Staf PKWT (Gol I) Dep. Audit Operasional                                                            ', 'AKTIF', 'SATUAN PENGAWASAN INTERN                          ', 'AUDIT OPERASIONAL'),
(391, 'SUPARIYANTO                                       ', '999400094 ', 'Senior Manager Tata Kelola Perusahaan                                                               ', 'AKTIF', 'SATUAN PENGAWASAN INTERN                          ', 'TATA KELOLA PERUSAHAAN'),
(392, 'TRI SURYANI                                       ', '999300006 ', 'Spesialis Muda - Dep. Tata Kelola Perusahaan                                                        ', 'AKTIF', 'SATUAN PENGAWASAN INTERN                          ', 'TATA KELOLA PERUSAHAAN'),
(393, 'FITROTUS SHOLIHAH                                 ', '662100008 ', 'Staf PKWT (Gol I) Dep. Tata Kelola Perusahaan                                                       ', 'AKTIF', 'SATUAN PENGAWASAN INTERN                          ', 'TATA KELOLA PERUSAHAAN'),
(394, 'ANNASTYA DEVINA KURNIA PUTRI                      ', '662200006 ', 'Staf PKWT (Gol I) Dep. Tata Kelola Perusahaan                                                       ', 'AKTIF', 'SATUAN PENGAWASAN INTERN                          ', 'TATA KELOLA PERUSAHAAN'),
(395, 'KRISTIYANTO                                       ', '999700004 ', 'Manager Manajemen Mutu                                                                              ', 'AKTIF', 'SATUAN PENGAWASAN INTERN                          ', 'TATA KELOLA PERUSAHAAN'),
(396, 'SURONO                                            ', '999600058 ', 'Staf (Gol II) Bag. Manajemen Mutu                                                                   ', 'AKTIF', 'SATUAN PENGAWASAN INTERN                          ', 'TATA KELOLA PERUSAHAAN'),
(397, 'MUHAMMAD IRFANUDDIN MAJID                         ', '662100015 ', 'Staf PKWT (Gol I) Bag. Keselamatan, Kesehatan Kerja dan Lingkungan                                  ', 'AKTIF', 'SATUAN PENGAWASAN INTERN                          ', 'TATA KELOLA PERUSAHAAN'),
(398, 'DEDI HERMAWAN                                     ', '991200047 ', 'Staf (Gol I) Bag. Keselamatan, Kesehatan Kerja dan Lingkungan                                       ', 'AKTIF', 'SATUAN PENGAWASAN INTERN                          ', 'TATA KELOLA PERUSAHAAN'),
(399, 'PARIJANTO                                         ', '999900043 ', 'Staf (Gol I) Bag. Keselamatan, Kesehatan Kerja dan Lingkungan                                       ', 'AKTIF', 'SATUAN PENGAWASAN INTERN                          ', 'TATA KELOLA PERUSAHAAN'),
(400, 'WIDODO                                            ', '999400027 ', 'Staf (Gol I) Bag. Keselamatan, Kesehatan Kerja dan Lingkungan                                       ', 'AKTIF', 'SATUAN PENGAWASAN INTERN                          ', 'TATA KELOLA PERUSAHAAN'),
(401, 'PURWANTO                                          ', '999800166 ', 'Staf (Gol I) Bag. Keselamatan, Kesehatan Kerja dan Lingkungan                                       ', 'AKTIF', 'SATUAN PENGAWASAN INTERN                          ', 'TATA KELOLA PERUSAHAAN'),
(402, 'TRI NUGROHO BUDI PRAWITO                          ', '999900018 ', 'Staf (Gol I) Bag. Keselamatan, Kesehatan Kerja dan Lingkungan                                       ', 'AKTIF', 'SATUAN PENGAWASAN INTERN                          ', 'TATA KELOLA PERUSAHAAN'),
(403, 'SUYANTO                                           ', '999900040 ', 'Staf (Gol I) Bag. Keselamatan, Kesehatan Kerja dan Lingkungan                                       ', 'AKTIF', 'SATUAN PENGAWASAN INTERN                          ', 'TATA KELOLA PERUSAHAAN'),
(404, 'AGUS NURDIONO                                     ', '999800133 ', 'Staf (Gol I) Bag. Manajemen Mutu                                                                    ', 'AKTIF', 'SATUAN PENGAWASAN INTERN                          ', 'TATA KELOLA PERUSAHAAN'),
(405, 'ERLANGGA BAGUS LAZUARDY                           ', '661710333 ', 'Staf PKWT (Gol I) Bag. Manajemen Mutu                                                               ', 'AKTIF', 'SATUAN PENGAWASAN INTERN                          ', 'TATA KELOLA PERUSAHAAN'),
(406, 'AGUS PURWANTO                                     ', '999600011 ', 'Manager Keselamatan, Kesehatan Kerja dan Lingkungan                                                 ', 'AKTIF', 'SATUAN PENGAWASAN INTERN                          ', 'TATA KELOLA PERUSAHAAN'),
(407, 'DIMAS ALIAKBAR                                    ', '991800008 ', 'Staf (Gol II) Bag. Keselamatan, Kesehatan Kerja dan Lingkungan                                      ', 'AKTIF', 'SATUAN PENGAWASAN INTERN                          ', 'TATA KELOLA PERUSAHAAN'),
(408, 'ANA HABIBAH                                       ', '991100026 ', 'Staf (Gol I) Bag. Keselamatan, Kesehatan Kerja dan Lingkungan                                       ', 'AKTIF', 'SATUAN PENGAWASAN INTERN                          ', 'TATA KELOLA PERUSAHAAN'),
(409, 'HASTI LUCIA RINI                                  ', '991200018 ', 'Staf (Gol I) Bag. Keselamatan, Kesehatan Kerja dan Lingkungan                                       ', 'AKTIF', 'SATUAN PENGAWASAN INTERN                          ', 'TATA KELOLA PERUSAHAAN'),
(410, 'OKSAN CHRISNANTYO                                 ', '991700069 ', 'Staf (Gol II) Dep. Audit Keuangan                                                                   ', 'AKTIF', 'SATUAN PENGAWASAN INTERN                          ', 'AUDIT KEUANGAN'),
(411, 'USY RAMADANI                                      ', '992000014 ', 'Staf (Gol I) Dep. Audit Keuangan                                                                    ', 'AKTIF', 'SATUAN PENGAWASAN INTERN                          ', 'AUDIT KEUANGAN'),
(412, 'TEGOEH HARI ABRIANTO                              ', '999500004 ', 'Spesialis Madya -Dep. Audit Keuangan                                                                ', 'AKTIF', 'SATUAN PENGAWASAN INTERN                          ', 'AUDIT KEUANGAN'),
(413, 'HENDY WIDANARKO                                   ', '992200001 ', 'Spesialis Muda - Dep. Audit Keuangan                                                                ', 'AKTIF', 'SATUAN PENGAWASAN INTERN                          ', 'AUDIT KEUANGAN'),
(414, 'ASTRI NARULITA ANGGRAINI                          ', '661900008 ', 'Staf PKWT (Gol I) Dep. Audit Keuangan                                                               ', 'AKTIF', 'SATUAN PENGAWASAN INTERN                          ', 'AUDIT KEUANGAN'),
(415, 'SHINTHYA NOVITASARI                               ', '661900026 ', 'Staf PKWT (Gol I) Dep. Audit Keuangan                                                               ', 'AKTIF', 'SATUAN PENGAWASAN INTERN                          ', 'AUDIT KEUANGAN'),
(416, 'MOH. HALDY PRATAMA                                ', '662000005 ', 'Staf PKWT (Gol I) Dep. Audit Keuangan                                                               ', 'AKTIF', 'SATUAN PENGAWASAN INTERN                          ', 'AUDIT KEUANGAN'),
(417, 'PANJI SULAKSONO                                   ', '990900012 ', 'Senior Manager Pengendalian Kualitas                                                                ', 'AKTIF', 'TEKNOLOGI                                         ', 'PENGENDALIAN KUALITAS'),
(418, 'DIAN PERDANIA PERMATASARI                         ', '991000006 ', 'Staf (Gol II) Dep. Pengendalian Kualitas                                                            ', 'AKTIF', 'TEKNOLOGI                                         ', 'PENGENDALIAN KUALITAS'),
(419, 'MIRANTI PRANASANTYA                               ', '991100055 ', 'Staf (Gol II) Dep. Pengendalian Kualitas                                                            ', 'AKTIF', 'TEKNOLOGI                                         ', 'PENGENDALIAN KUALITAS'),
(420, 'LUQMAN KHOIRUL HUDA                               ', '991400016 ', 'Staf (Gol II) Dep. Pengendalian Kualitas                                                            ', 'AKTIF', 'TEKNOLOGI                                         ', 'PENGENDALIAN KUALITAS'),
(421, 'DENY NUSANTARI                                    ', '999800183 ', 'Staf (Gol I) Dep. Pengendalian Kualitas                                                             ', 'AKTIF', 'TEKNOLOGI                                         ', 'PENGENDALIAN KUALITAS'),
(422, 'NANSON KURNIAWAN PRANATA PUTRA                    ', '661700052 ', 'Staf PKWT (Gol I) Dep. Pengendalian Kualitas                                                        ', 'AKTIF', 'TEKNOLOGI                                         ', 'PENGENDALIAN KUALITAS'),
(423, 'YOGI SETYA PRAYOGA SAKTI                          ', '992000005 ', 'Staf (Gol I) Bag. Final Inspection and Testing                                                      ', 'AKTIF', 'TEKNOLOGI                                         ', 'PENGENDALIAN KUALITAS'),
(424, 'AGUNG WAHYU HERMAWANTO                            ', '992100003 ', 'Staf (Gol I) Bag. Final Inspection and Testing                                                      ', 'AKTIF', 'TEKNOLOGI                                         ', 'PENGENDALIAN KUALITAS'),
(425, 'AHMAD ABDUL AZIS                                  ', '992100004 ', 'Staf (Gol I) Bag. Final Inspection and Testing                                                      ', 'AKTIF', 'TEKNOLOGI                                         ', 'PENGENDALIAN KUALITAS'),
(426, 'FAJAR ERIANSAH                                    ', '991100044 ', 'SPV Unit Final Inspection dan Testing                                                               ', 'AKTIF', 'TEKNOLOGI                                         ', 'PENGENDALIAN KUALITAS'),
(427, 'SUTRISNO                                          ', '999800111 ', 'Staf (Gol I) Bag. Final Inspection and Testing                                                      ', 'AKTIF', 'TEKNOLOGI                                         ', 'PENGENDALIAN KUALITAS'),
(428, 'RUHAEDI NURYANTO                                  ', '999800122 ', 'Staf (Gol I) Bag. Final Inspection and Testing                                                      ', 'AKTIF', 'TEKNOLOGI                                         ', 'PENGENDALIAN KUALITAS'),
(429, 'EDY IKHWANTONO                                    ', '999800169 ', 'Staf (Gol I) Bag. Final Inspection and Testing                                                      ', 'AKTIF', 'TEKNOLOGI                                         ', 'PENGENDALIAN KUALITAS'),
(430, 'TRI YANDOKO                                       ', '991200080 ', 'Staf (Gol I) Bag. Final Inspection and Testing                                                      ', 'AKTIF', 'TEKNOLOGI                                         ', 'PENGENDALIAN KUALITAS'),
(431, 'WINARNO                                           ', '991200082 ', 'Staf (Gol I) Bag. Final Inspection and Testing                                                      ', 'AKTIF', 'TEKNOLOGI                                         ', 'PENGENDALIAN KUALITAS'),
(432, 'IVAN FIRSTYA NUGRAHA                              ', '991900006 ', 'Staf (Gol I) Bag. Final Inspection and Testing                                                      ', 'AKTIF', 'TEKNOLOGI                                         ', 'PENGENDALIAN KUALITAS'),
(433, 'HERU CAHYONO                                      ', '999400034 ', 'Staf (Gol II) Bag. Final Inspection and Testing                                                     ', 'AKTIF', 'TEKNOLOGI                                         ', 'PENGENDALIAN KUALITAS'),
(434, 'AGUS PRIYO JATMIKO                                ', '991200031 ', 'Staf (Gol I) Bag. Final Inspection and Testing                                                      ', 'AKTIF', 'TEKNOLOGI                                         ', 'PENGENDALIAN KUALITAS'),
(435, 'AGUS SUMANTO                                      ', '991200032 ', 'Staf (Gol I) Bag. Final Inspection and Testing                                                      ', 'AKTIF', 'TEKNOLOGI                                         ', 'PENGENDALIAN KUALITAS'),
(436, 'BAMBANG KURNIANTO                                 ', '991200042 ', 'Staf (Gol I) Bag. Final Inspection and Testing                                                      ', 'AKTIF', 'TEKNOLOGI                                         ', 'PENGENDALIAN KUALITAS'),
(437, 'NUR ZAINAL ARIFIN                                 ', '991200065 ', 'Staf (Gol I) Bag. Final Inspection and Testing                                                      ', 'AKTIF', 'TEKNOLOGI                                         ', 'PENGENDALIAN KUALITAS'),
(438, 'MOHAMAD IRFAN                                     ', '992100015 ', 'Staf (Gol I) Bag. Final Inspection and Testing                                                      ', 'AKTIF', 'TEKNOLOGI                                         ', 'PENGENDALIAN KUALITAS'),
(439, 'MUHAMAD RIFKI EFENDI                              ', '991900009 ', 'Staf (Gol I) Bag. Production Quality Control                                                        ', 'AKTIF', 'TEKNOLOGI                                         ', 'PENGENDALIAN KUALITAS'),
(440, 'PANDHIT ADIGUNA PERDANA                           ', '991900013 ', 'Staf (Gol I) Bag. Production Quality Control                                                        ', 'AKTIF', 'TEKNOLOGI                                         ', 'PENGENDALIAN KUALITAS'),
(441, 'EDY SUDARSANA                                     ', '999400020 ', 'Staf (Gol I) Bag. Production Quality Control                                                        ', 'AKTIF', 'TEKNOLOGI                                         ', 'PENGENDALIAN KUALITAS'),
(442, 'KELANJAR                                          ', '999400059 ', 'Staf (Gol I) Bag. Production Quality Control                                                        ', 'AKTIF', 'TEKNOLOGI                                         ', 'PENGENDALIAN KUALITAS'),
(443, 'SUKARNO                                           ', '999600022 ', 'SPV Unit Production Quality Control                                                                 ', 'AKTIF', 'TEKNOLOGI                                         ', 'PENGENDALIAN KUALITAS'),
(444, 'FERADY CHRISDIYANTORO                             ', '991100008 ', 'Manager Final Inspection and Testing                                                                ', 'AKTIF', 'TEKNOLOGI                                         ', 'PENGENDALIAN KUALITAS'),
(445, 'NUR SANDHA PRIKA                                  ', '661900291 ', 'Staf PKWT (Gol I) Dep. Pengendalian Kualitas                                                        ', 'AKTIF', 'TEKNOLOGI                                         ', 'PENGENDALIAN KUALITAS'),
(446, 'VIVI NOFAN ANGGRAINI                              ', '661900298 ', 'Staf PKWT (Gol I) Dep. Pengendalian Kualitas                                                        ', 'AKTIF', 'TEKNOLOGI                                         ', 'PENGENDALIAN KUALITAS'),
(447, 'DJAHARI IRIANTO                                   ', '999600021 ', 'Manager Production Quality Control                                                                  ', 'AKTIF', 'TEKNOLOGI                                         ', 'PENGENDALIAN KUALITAS'),
(448, 'KUSNO                                             ', '990100001 ', 'Staf (Gol I) Bag. Production Quality Control                                                        ', 'AKTIF', 'TEKNOLOGI                                         ', 'PENGENDALIAN KUALITAS'),
(449, 'WAHYU RIYANTO                                     ', '991700041 ', 'Staf (Gol I) Bag. Production Quality Control                                                        ', 'AKTIF', 'TEKNOLOGI                                         ', 'PENGENDALIAN KUALITAS'),
(450, 'JODI PRASOJO                                      ', '999900080 ', 'Staf (Gol I) Bag. Production Quality Control                                                        ', 'AKTIF', 'TEKNOLOGI                                         ', 'PENGENDALIAN KUALITAS'),
(451, 'MUCHIN SHOLIKHI                                   ', '661700280 ', 'Staf PKWT (Gol I) Dep. Pengendalian Kualitas                                                        ', 'AKTIF', 'TEKNOLOGI                                         ', 'PENGENDALIAN KUALITAS'),
(452, 'RUSLI NASRULLAH SAFAR                             ', '661710355 ', 'Staf PKWT (Gol I) Dep. Pengendalian Kualitas                                                        ', 'AKTIF', 'TEKNOLOGI                                         ', 'PENGENDALIAN KUALITAS'),
(453, 'HUSEIN MUHAMMAD FRAS                              ', '661710364 ', 'Staf PKWT (Gol I) Dep. Pengendalian Kualitas                                                        ', 'AKTIF', 'TEKNOLOGI                                         ', 'PENGENDALIAN KUALITAS'),
(454, 'OKY PRAMUDIANTATO BIMANTORO                       ', '661710365 ', 'Staf PKWT (Gol I) Dep. Pengendalian Kualitas                                                        ', 'AKTIF', 'TEKNOLOGI                                         ', 'PENGENDALIAN KUALITAS'),
(455, 'AGIL DWIJATMOKO RAHMATULLAH                       ', '661710386 ', 'Staf PKWT (Gol I) Dep. Pengendalian Kualitas                                                        ', 'AKTIF', 'TEKNOLOGI                                         ', 'PENGENDALIAN KUALITAS'),
(456, 'MOH LUTFI YAZID                                   ', '661900287 ', 'Staf PKWT (Gol I) Dep. Pengendalian Kualitas                                                        ', 'AKTIF', 'TEKNOLOGI                                         ', 'PENGENDALIAN KUALITAS'),
(457, 'PRADHITA AYU RIYANTO                              ', '662100002 ', 'Staf PKWT (Gol I) Div. Teknologi                                                                    ', 'AKTIF', 'TEKNOLOGI                                         ', 'DIVISI TEKNOLOGI'),
(458, 'DENNA MAULANA ACHMAD                              ', '991500024 ', 'Manager Engineering Information Management                                                          ', 'AKTIF', 'TEKNOLOGI                                         ', 'DIVISI TEKNOLOGI'),
(459, 'DADANG TRI HERIYANTO                              ', '999900034 ', 'Staf (Gol I) Bag. Engineering Information Management                                                ', 'AKTIF', 'TEKNOLOGI                                         ', 'DIVISI TEKNOLOGI'),
(460, 'LILY MERLINAWATI                                  ', '661900044 ', 'Staf PKWT (Gol I) Bag. Engineering Information Management                                           ', 'AKTIF', 'TEKNOLOGI                                         ', 'DIVISI TEKNOLOGI'),
(461, 'MEI NUR CAHYANTI                                  ', '661900111 ', 'Staf PKWT (Gol I) Bag. Engineering Information Management                                           ', 'AKTIF', 'TEKNOLOGI                                         ', 'DIVISI TEKNOLOGI'),
(462, 'BAMBANG SUTRISNO                                  ', '999500017 ', 'General Manager Teknologi                                                                           ', 'AKTIF', 'TEKNOLOGI                                         ', 'DIVISI TEKNOLOGI'),
(463, 'YULIAN LINGGA PERMANA                             ', '991800023 ', 'Staf (Gol II) Div. Teknologi                                                                        ', 'AKTIF', 'TEKNOLOGI                                         ', 'DIVISI TEKNOLOGI'),
(464, 'TOTO ISDARTO                                      ', '990900015 ', 'Spesialis Madya - Div. Teknologi                                                                    ', 'AKTIF', 'TEKNOLOGI                                         ', 'DIVISI TEKNOLOGI'),
(465, 'TARYONO                                           ', '991600022 ', 'Spesialis Muda - Div. Teknologi                                                                     ', 'AKTIF', 'TEKNOLOGI                                         ', 'DIVISI TEKNOLOGI'),
(466, 'SUPARMAN                                          ', '999200037 ', 'Spesialis Muda - Div. Teknologi                                                                     ', 'AKTIF', 'TEKNOLOGI                                         ', 'DIVISI TEKNOLOGI'),
(467, 'HEDI PURNOMO                                      ', '991200019 ', 'Spesialis Pratama - Div. Teknologi                                                                  ', 'AKTIF', 'TEKNOLOGI                                         ', 'DIVISI TEKNOLOGI'),
(468, 'M EVAN WIRYAWAN                                   ', '991000012 ', 'Senior Manager Engineering                                                                          ', 'AKTIF', 'TEKNOLOGI                                         ', 'ENGINEERING'),
(469, 'RAHARDIAN TITUS NURDIANSYAH                       ', '991100061 ', 'Pejabat Setingkat M Dep. Engineering                                                                ', 'AKTIF', 'TEKNOLOGI                                         ', 'ENGINEERING');
INSERT INTO `pegawai` (`id_pegawai`, `nama_pegawai`, `NIP`, `jabatan_pegawai`, `Status`, `Divisi`, `Departemen`) VALUES
(470, 'RUDY INDRAWAN                                     ', '991100068 ', 'Pejabat Setingkat M Dep. Engineering                                                                ', 'AKTIF', 'TEKNOLOGI                                         ', 'ENGINEERING'),
(471, 'R. NOVA ADRIANTO KOESNANDAR                       ', '991200026 ', 'Staf (Gol II) Dep. Engineering                                                                      ', 'AKTIF', 'TEKNOLOGI                                         ', 'ENGINEERING'),
(472, 'DIAN YUDHA PRASETIAWAN                            ', '991500025 ', 'Staf (Gol II) Dep. Engineering                                                                      ', 'AKTIF', 'TEKNOLOGI                                         ', 'ENGINEERING'),
(473, 'FAROUQ YAFFIE                                     ', '991600008 ', 'Staf (Gol II) Dep. Engineering                                                                      ', 'AKTIF', 'TEKNOLOGI                                         ', 'ENGINEERING'),
(474, 'ULUNG HUGE HABIBI                                 ', '991900019 ', 'Staf (Gol I) Bag. Product Engineering                                                               ', 'AKTIF', 'TEKNOLOGI                                         ', 'ENGINEERING'),
(475, 'ANGGARA BAYU PRATAMA                              ', '992100005 ', 'Staf (Gol I) Bag. Product Engineering                                                               ', 'AKTIF', 'TEKNOLOGI                                         ', 'ENGINEERING'),
(476, 'BINTA ALI\' TAMARA                                 ', '992100008 ', 'Staf (Gol I) Bag. Product Engineering                                                               ', 'AKTIF', 'TEKNOLOGI                                         ', 'ENGINEERING'),
(477, 'ATIA FASIHAH NIHAYATI                             ', '991400018 ', 'Staf (Gol II) Bag. Product Engineering                                                              ', 'AKTIF', 'TEKNOLOGI                                         ', 'ENGINEERING'),
(478, 'FIRDAUSA RETNANING RESTU                          ', '991600009 ', 'Staf (Gol II) Bag. Product Engineering                                                              ', 'AKTIF', 'TEKNOLOGI                                         ', 'ENGINEERING'),
(479, 'KHARISMA CAHAYA AQLI                              ', '991600011 ', 'Staf (Gol II) Bag. Product Engineering                                                              ', 'AKTIF', 'TEKNOLOGI                                         ', 'ENGINEERING'),
(480, 'MOH FAZA ROSYADA                                  ', '991600013 ', 'Staf (Gol II) Bag. Product Engineering                                                              ', 'AKTIF', 'TEKNOLOGI                                         ', 'ENGINEERING'),
(481, 'YUNANDARU SAHID PUTRA                             ', '991600019 ', 'Staf (Gol II) Bag. Product Engineering                                                              ', 'AKTIF', 'TEKNOLOGI                                         ', 'ENGINEERING'),
(482, 'MUHAMMAD ZAINAL MAHFUD                            ', '991900025 ', 'Staf (Gol II) Bag. Product Engineering                                                              ', 'AKTIF', 'TEKNOLOGI                                         ', 'ENGINEERING'),
(483, 'ABDOLLAH                                          ', '661900002 ', 'Staf PKWT (Gol I) Dep. Engineering                                                                  ', 'AKTIF', 'TEKNOLOGI                                         ', 'ENGINEERING'),
(484, 'AHMAD MUSLIM                                      ', '661900003 ', 'Staf PKWT (Gol I) Dep. Engineering                                                                  ', 'AKTIF', 'TEKNOLOGI                                         ', 'ENGINEERING'),
(485, 'AHMAD RIDWAN HANAFI                               ', '661900004 ', 'Staf PKWT (Gol I) Dep. Engineering                                                                  ', 'AKTIF', 'TEKNOLOGI                                         ', 'ENGINEERING'),
(486, 'MUHAMMAD IBRAM HIBBANURROHIM                      ', '662200001 ', 'Staf PKWT (Gol I) Dep. Engineering                                                                  ', 'AKTIF', 'TEKNOLOGI                                         ', 'ENGINEERING'),
(487, 'IBNU DWI SUFAJAR                                  ', '662200003 ', 'Staf PKWT (Gol I) Dep. Engineering                                                                  ', 'AKTIF', 'TEKNOLOGI                                         ', 'ENGINEERING'),
(488, 'RIRIN SULISTYOWATI                                ', '991200029 ', 'Manager Product Engineering                                                                         ', 'AKTIF', 'TEKNOLOGI                                         ', 'ENGINEERING'),
(489, 'ALPHA KURNIAWAN                                   ', '991200005 ', 'Spesialis Muda - Dep. Engineering                                                                   ', 'AKTIF', 'TEKNOLOGI                                         ', 'ENGINEERING'),
(490, 'AKHMAT BUSORI                                     ', '991400013 ', 'Spesialis Muda - Dep. Engineering                                                                   ', 'AKTIF', 'TEKNOLOGI                                         ', 'ENGINEERING'),
(491, 'MICHAEL MARTIN                                    ', '992100001 ', 'Spesialis Muda - Dep. Engineering                                                                   ', 'AKTIF', 'TEKNOLOGI                                         ', 'ENGINEERING'),
(492, 'RIDWAN CATUR PRASETYO                             ', '661710349 ', 'Staf PKWT (Gol I) Dep. Engineering                                                                  ', 'AKTIF', 'TEKNOLOGI                                         ', 'ENGINEERING'),
(493, 'YASHERO SAIN MUTTAQIN                             ', '661710363 ', 'Staf PKWT (Gol I) Dep. Engineering                                                                  ', 'AKTIF', 'TEKNOLOGI                                         ', 'ENGINEERING'),
(494, 'MUHAMMAD SYAHRUL FADHIL                           ', '661710378 ', 'Staf PKWT (Gol I) Dep. Engineering                                                                  ', 'AKTIF', 'TEKNOLOGI                                         ', 'ENGINEERING'),
(495, 'RAINDES IKAG MAHENDRA                             ', '991900027 ', 'Staf (Gol I) Dep. Engineering                                                                       ', 'AKTIF', 'TEKNOLOGI                                         ', 'ENGINEERING'),
(496, 'PRASETYA ADI NUGRAHA                              ', '992000004 ', 'Staf (Gol I) Dep. Engineering                                                                       ', 'AKTIF', 'TEKNOLOGI                                         ', 'ENGINEERING'),
(497, 'AHMAD RIFQI                                       ', '992000008 ', 'Staf (Gol I) Dep. Engineering                                                                       ', 'AKTIF', 'TEKNOLOGI                                         ', 'ENGINEERING'),
(498, 'MAWAR SANTIKA                                     ', '992000010 ', 'Staf (Gol I) Dep. Engineering                                                                       ', 'AKTIF', 'TEKNOLOGI                                         ', 'ENGINEERING'),
(499, 'HONO YUDO PRAKOSO                                 ', '992100013 ', 'Staf (Gol I) Dep. Engineering                                                                       ', 'AKTIF', 'TEKNOLOGI                                         ', 'ENGINEERING'),
(500, 'HUDA BARANTO                                      ', '990900010 ', 'Spesialis Muda - Dep. Engineering                                                                   ', 'AKTIF', 'TEKNOLOGI                                         ', 'ENGINEERING'),
(501, 'DONY SAPUTRA                                      ', '991700059 ', 'Staf (Gol II) Dep. Engineering                                                                      ', 'AKTIF', 'TEKNOLOGI                                         ', 'ENGINEERING'),
(502, 'SHERDIAN SUKMA RAHARDAN                           ', '991700073 ', 'Staf (Gol II) Dep. Engineering                                                                      ', 'AKTIF', 'TEKNOLOGI                                         ', 'ENGINEERING'),
(503, 'RENDRA SOFYAR                                     ', '999900114 ', 'Staf (Gol II) Dep. Engineering                                                                      ', 'AKTIF', 'TEKNOLOGI                                         ', 'ENGINEERING'),
(504, 'NURDEWI YANUARINI                                 ', '991100013 ', 'Staf (Gol I) Dep. Engineering                                                                       ', 'AKTIF', 'TEKNOLOGI                                         ', 'ENGINEERING'),
(505, 'LUCKY ANDOYO WIDYASTO                             ', '991900007 ', 'Staf (Gol I) Dep. Engineering                                                                       ', 'AKTIF', 'TEKNOLOGI                                         ', 'ENGINEERING'),
(506, 'MOH. ANSHORI                                      ', '991900008 ', 'Staf (Gol I) Dep. Engineering                                                                       ', 'AKTIF', 'TEKNOLOGI                                         ', 'ENGINEERING'),
(507, 'KRISTANTO                                         ', '991100051 ', 'Senior Manager Desain                                                                               ', 'AKTIF', 'TEKNOLOGI                                         ', 'DESAIN'),
(508, 'BUDI AGUS SETIAWAN                                ', '991100035 ', 'Pejabat Setingkat M Dep. Desain                                                                     ', 'AKTIF', 'TEKNOLOGI                                         ', 'DESAIN'),
(509, 'ROMAL HADI SETYAWAN                               ', '991100066 ', 'Pejabat Setingkat M Dep. Desain                                                                     ', 'AKTIF', 'TEKNOLOGI                                         ', 'DESAIN'),
(510, 'TAUFIQ HIDAYAT                                    ', '991400020 ', 'Staf (Gol II) Dep. Desain                                                                           ', 'AKTIF', 'TEKNOLOGI                                         ', 'DESAIN'),
(511, 'MUHAMMAD MUSHLIH ELHAFID                          ', '991500029 ', 'Staf (Gol II) Dep. Desain                                                                           ', 'AKTIF', 'TEKNOLOGI                                         ', 'DESAIN'),
(512, 'TRI PRASETYA FATHURRODLI                          ', '991500032 ', 'Staf (Gol II) Dep. Desain                                                                           ', 'AKTIF', 'TEKNOLOGI                                         ', 'DESAIN'),
(513, 'RIVANANTO HERNANDA                                ', '991600016 ', 'Staf (Gol II) Bag. Desain Bogie dan Carbody                                                         ', 'AKTIF', 'TEKNOLOGI                                         ', 'DESAIN'),
(514, 'YUSUF TRI WICAKSONO                               ', '991600020 ', 'Staf (Gol II) Bag. Desain Bogie dan Carbody                                                         ', 'AKTIF', 'TEKNOLOGI                                         ', 'DESAIN'),
(515, 'ARIEF HERMAN SOESILO                              ', '991400025 ', 'Staf (Gol I) Bag. Desain Bogie dan Carbody                                                          ', 'AKTIF', 'TEKNOLOGI                                         ', 'DESAIN'),
(516, 'FAHMI KHAFIDHUL HAQ                               ', '992000009 ', 'Staf (Gol I) Bag. Desain Bogie dan Carbody                                                          ', 'AKTIF', 'TEKNOLOGI                                         ', 'DESAIN'),
(517, 'SAPUTRA DIDIK HIDAYAT                             ', '992100021 ', 'Staf (Gol I) Bag. Desain Bogie dan Carbody                                                          ', 'AKTIF', 'TEKNOLOGI                                         ', 'DESAIN'),
(518, 'LINA DAMAYANTI                                    ', '661900286 ', 'Staf PKWT (Gol I) Dep. Desain                                                                       ', 'AKTIF', 'TEKNOLOGI                                         ', 'DESAIN'),
(519, 'RAIS HAQ TEGUH                                    ', '661900292 ', 'Staf PKWT (Gol I) Dep. Desain                                                                       ', 'AKTIF', 'TEKNOLOGI                                         ', 'DESAIN'),
(520, 'HERNAWAN PRASANTO                                 ', '991100047 ', 'Manager Desain Bogie dan Carbody                                                                    ', 'AKTIF', 'TEKNOLOGI                                         ', 'DESAIN'),
(521, 'FATHUR ROKHMAN HIDAYAT                            ', '991400014 ', 'Staf (Gol II) Bag. Desain Bogie dan Carbody                                                         ', 'AKTIF', 'TEKNOLOGI                                         ', 'DESAIN'),
(522, 'KARISMA RIZAL                                     ', '991600010 ', 'Staf (Gol II) Bag. Desain Bogie dan Carbody                                                         ', 'AKTIF', 'TEKNOLOGI                                         ', 'DESAIN'),
(523, 'KRISNA BILAL ARIEF                                ', '991600012 ', 'Staf (Gol II) Bag. Desain Bogie dan Carbody                                                         ', 'AKTIF', 'TEKNOLOGI                                         ', 'DESAIN'),
(524, 'DEFANI FIRDAUSI                                   ', '661900011 ', 'Staf PKWT (Gol I) Dep. Desain                                                                       ', 'AKTIF', 'TEKNOLOGI                                         ', 'DESAIN'),
(525, 'DIAN SAPUTRA                                      ', '661900012 ', 'Staf PKWT (Gol I) Dep. Desain                                                                       ', 'AKTIF', 'TEKNOLOGI                                         ', 'DESAIN'),
(526, 'GUID SUNCOKO                                      ', '661900016 ', 'Staf PKWT (Gol I) Dep. Desain                                                                       ', 'AKTIF', 'TEKNOLOGI                                         ', 'DESAIN'),
(527, 'M. FATHINUL HUMAM                                 ', '661900020 ', 'Staf PKWT (Gol I) Dep. Desain                                                                       ', 'AKTIF', 'TEKNOLOGI                                         ', 'DESAIN'),
(528, 'OKY RISKY DWI SANTOSO                             ', '661900024 ', 'Staf PKWT (Gol I) Dep. Desain                                                                       ', 'AKTIF', 'TEKNOLOGI                                         ', 'DESAIN'),
(529, 'UNGGUL SIGIT WIBOWO                               ', '661900041 ', 'Staf PKWT (Gol I) Dep. Desain                                                                       ', 'AKTIF', 'TEKNOLOGI                                         ', 'DESAIN'),
(530, 'DIVA YULIAN TRISNA                                ', '991800009 ', 'Staf (Gol II) Dep. Desain                                                                           ', 'AKTIF', 'TEKNOLOGI                                         ', 'DESAIN'),
(531, 'ASROFUL ANGSORI                                   ', '991700008 ', 'Staf (Gol I) Dep. Desain                                                                            ', 'AKTIF', 'TEKNOLOGI                                         ', 'DESAIN'),
(532, 'DANNY EKASURYA SASMITA                            ', '991800005 ', 'Staf (Gol I) Dep. Desain                                                                            ', 'AKTIF', 'TEKNOLOGI                                         ', 'DESAIN'),
(533, 'EMIR PERDANA HARAHAP                              ', '991800010 ', 'Staf (Gol I) Dep. Desain                                                                            ', 'AKTIF', 'TEKNOLOGI                                         ', 'DESAIN'),
(534, 'WIDIA KARTIKA                                     ', '661710361 ', 'Staf PKWT (Gol I) Dep. Desain                                                                       ', 'AKTIF', 'TEKNOLOGI                                         ', 'DESAIN'),
(535, 'BHIMANTARA YUDHA PRAWIRA                          ', '661900009 ', 'Staf PKWT (Gol I) Dep. Desain                                                                       ', 'AKTIF', 'TEKNOLOGI                                         ', 'DESAIN'),
(536, 'BAMBANG                                           ', '999800009 ', 'Staf (Gol II) Dep. Desain                                                                           ', 'AKTIF', 'TEKNOLOGI                                         ', 'DESAIN'),
(537, 'AGUNG EKO SETYO BUDI                              ', '991600001 ', 'Staf (Gol II) Dep. Desain                                                                           ', 'AKTIF', 'TEKNOLOGI                                         ', 'DESAIN'),
(538, 'CORNELIUS HERRY RADITYO                           ', '991600004 ', 'Staf (Gol II) Dep. Desain                                                                           ', 'AKTIF', 'TEKNOLOGI                                         ', 'DESAIN'),
(539, 'OKY BAYU MURDIANTO                                ', '991600015 ', 'Staf (Gol II) Dep. Desain                                                                           ', 'AKTIF', 'TEKNOLOGI                                         ', 'DESAIN'),
(540, 'WARIDA FEBRIATI                                   ', '991600017 ', 'Staf (Gol II) Dep. Desain                                                                           ', 'AKTIF', 'TEKNOLOGI                                         ', 'DESAIN'),
(541, 'PRASETYA HUTAMA PUTRA P.                          ', '991700070 ', 'Staf (Gol II) Dep. Desain                                                                           ', 'AKTIF', 'TEKNOLOGI                                         ', 'DESAIN');

-- --------------------------------------------------------

--
-- Table structure for table `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tokenable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `surat_event`
--

CREATE TABLE `surat_event` (
  `id_event` bigint(20) UNSIGNED NOT NULL,
  `id_surat` bigint(20) UNSIGNED NOT NULL,
  `event` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `surat_object` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `surat_event`
--

INSERT INTO `surat_event` (`id_event`, `id_surat`, `event`, `surat_object`) VALUES
(1, 1, 'Mengganti', 3),
(2, 1, 'Menggantikan', 4);

--
-- Triggers `surat_event`
--
DELIMITER $$
CREATE TRIGGER `id_event` BEFORE INSERT ON `surat_event` FOR EACH ROW BEGIN
           DECLARE id bigint(20) DEFAULT 0;
           SET id = (SELECT COUNT(id_event) FROM surat_event) + 1;
           SET new.id_event = id;
       END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id_user` bigint(20) UNSIGNED NOT NULL,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama_user` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id_user`, `username`, `password`, `nama_user`, `role`) VALUES
(1, 'admin', '$2y$10$rVpi6lkpYsytLKITGOi5AukzZZShdCM2p4AhXHeX70woQ2iVQMQE6', 'Dewa', 'Admin'),
(2, 'Dewa', '$2y$10$uV8942zP5xWrkjfgRCgvte9zseXLsx8VFJ7A4NpUh3wCfeeTmrGD6', 'Dewa', 'Admin');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `aktivitas`
--
ALTER TABLE `aktivitas`
  ADD PRIMARY KEY (`id_aktivitas`),
  ADD KEY `fk_surat` (`id_surat`);

--
-- Indexes for table `detail_aktivitas`
--
ALTER TABLE `detail_aktivitas`
  ADD PRIMARY KEY (`id_detail`),
  ADD KEY `fk_aktivitas` (`id_aktivitas`),
  ADD KEY `fk_pegawai` (`id_pegawai`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `m_jenis_surat`
--
ALTER TABLE `m_jenis_surat`
  ADD PRIMARY KEY (`id_jenis`);

--
-- Indexes for table `m_sub_jenis_surat`
--
ALTER TABLE `m_sub_jenis_surat`
  ADD PRIMARY KEY (`id_sub_jenis`),
  ADD KEY `m_sub_jenis_surat_id_jenis_foreign` (`id_jenis`);

--
-- Indexes for table `m_surat`
--
ALTER TABLE `m_surat`
  ADD PRIMARY KEY (`id_surat`),
  ADD KEY `m_surat_id_jenis_foreign` (`id_jenis`),
  ADD KEY `m_surat_id_sub_jenis_foreign` (`id_sub_jenis`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `pegawai`
--
ALTER TABLE `pegawai`
  ADD PRIMARY KEY (`id_pegawai`);

--
-- Indexes for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`);

--
-- Indexes for table `surat_event`
--
ALTER TABLE `surat_event`
  ADD PRIMARY KEY (`id_event`),
  ADD KEY `surat_event_id_surat_foreign` (`id_surat`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `aktivitas`
--
ALTER TABLE `aktivitas`
  MODIFY `id_aktivitas` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `detail_aktivitas`
--
ALTER TABLE `detail_aktivitas`
  MODIFY `id_detail` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `m_jenis_surat`
--
ALTER TABLE `m_jenis_surat`
  MODIFY `id_jenis` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `m_sub_jenis_surat`
--
ALTER TABLE `m_sub_jenis_surat`
  MODIFY `id_sub_jenis` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `m_surat`
--
ALTER TABLE `m_surat`
  MODIFY `id_surat` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `pegawai`
--
ALTER TABLE `pegawai`
  MODIFY `id_pegawai` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=542;

--
-- AUTO_INCREMENT for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `surat_event`
--
ALTER TABLE `surat_event`
  MODIFY `id_event` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id_user` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `aktivitas`
--
ALTER TABLE `aktivitas`
  ADD CONSTRAINT `fk_surat` FOREIGN KEY (`id_surat`) REFERENCES `m_surat` (`id_surat`);

--
-- Constraints for table `detail_aktivitas`
--
ALTER TABLE `detail_aktivitas`
  ADD CONSTRAINT `fk_aktivitas` FOREIGN KEY (`id_aktivitas`) REFERENCES `aktivitas` (`id_aktivitas`),
  ADD CONSTRAINT `fk_pegawai` FOREIGN KEY (`id_pegawai`) REFERENCES `pegawai` (`id_pegawai`);

--
-- Constraints for table `m_sub_jenis_surat`
--
ALTER TABLE `m_sub_jenis_surat`
  ADD CONSTRAINT `m_sub_jenis_surat_id_jenis_foreign` FOREIGN KEY (`id_jenis`) REFERENCES `m_jenis_surat` (`id_jenis`);

--
-- Constraints for table `m_surat`
--
ALTER TABLE `m_surat`
  ADD CONSTRAINT `m_surat_id_jenis_foreign` FOREIGN KEY (`id_jenis`) REFERENCES `m_jenis_surat` (`id_jenis`),
  ADD CONSTRAINT `m_surat_id_sub_jenis_foreign` FOREIGN KEY (`id_sub_jenis`) REFERENCES `m_sub_jenis_surat` (`id_sub_jenis`);

--
-- Constraints for table `surat_event`
--
ALTER TABLE `surat_event`
  ADD CONSTRAINT `surat_event_id_surat_foreign` FOREIGN KEY (`id_surat`) REFERENCES `m_surat` (`id_surat`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
