<?php

use App\Http\Controllers\aktivitasController;
use App\Http\Controllers\SKController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\STController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\detailController;
use App\Http\Controllers\jenisController;
use App\Http\Controllers\m_suratController;
use App\Http\Controllers\pegawaiController;
use App\Http\Controllers\statusController;
use App\Http\Controllers\subJenisController;
use App\Http\Controllers\userController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [detailController::class, 'homeIndex']);

//Login Routes
// Route::get('/login', [AuthController::class, 'login'])->name('login');
Route::get('/login', [AuthController::class, 'index'])->name('login');
Route::post('/auth', [AuthController::class, 'authenicate'])->name('auth');
Route::post('/logout', [AuthController::class, 'logout'])->name('logout');

Route::get('/surat', [m_suratController::class, 'index']);
Route::get('/detail/surat/{id_surat?}', [detailController::class, 'index'])->name('detailSurat');

Route::middleware('auth')->group(function () {
    //User Routes
    Route::get('/user', [userController::class, 'index']);
    Route::get('/user/remove/{id?}', [userController::class, 'delete'])->name('removeUser');
    Route::get('/user/getPengguna/{id}', [userController::class, 'getPenggunaById']);
    Route::post('/user/create', [userController::class, 'store'])->name('addUser');
    Route::post('/user/update', [userController::class, 'update'])->name('updateUser');


    //Basic Routes
    Route::get('/surat/remove/{id?}', [m_suratController::class, 'delete'])->name('removeSurat');
    Route::get('/surat/getSurat/{id}', [m_suratController::class, 'getSurat']);
    Route::get('/surat/getSubJenisById/{id_jenis}', [m_suratController::class, 'getSubJenisById']);
    Route::post('/surat/add', [m_suratController::class, 'store'])->name('addSurat');
    Route::post('/surat/update', [m_suratController::class, 'update'])->name('updateSurat');
    Route::post('/surat/filter', [m_suratController::class, 'filter'])->name('surat.filter');

    //Detail Route

    Route::get('/detail/pegawai/{id_pegawai?}', [detailController::class, 'pegawaiIndex'])->name('detailPegawai');
    Route::get('detail/aktivitas/{id_aktivitas?}', [detailController::class, 'detailAktivitas'])->name('detailAktivitas');
    Route::get('/tambahDetail/{id_aktivitas}', [detailController::class, 'tambah']);
    Route::post('/detail/add', [detailController::class, 'store'])->name('addDetail');
    Route::post('/detail/update', [detailController::class, 'update'])->name('updateDetail');
    Route::get('/detail/delete/{id}', [detailController::class, 'delete'])->name('deleteDetail');
    Route::get('/detail/getDetail/{id_detail}', [detailController::class, 'getDetail']);

    //Kategori
    Route::get('/jenis', [jenisController::class, 'index']);
    Route::get('/jenis/getJenis/{id}', [jenisController::class, 'getJenis']);
    Route::get('/jenis/remove/{id?}', [jenisController::class, 'delete'])->name('removeJenis');
    Route::post('/jenis/add', [jenisController::class, 'store'])->name('addJenis');
    Route::post('/jenis/update', [jenisController::class, 'update'])->name('editJenis');

    //Sub Kategori
    Route::get('/subJenis', [subJenisController::class, 'index']);
    Route::get('/subJenis/getSub/{id}', [subJenisController::class, 'getSub']);
    Route::get('/subJenis/remove/{id?}', [subJenisController::class, 'delete'])->name('removeSub');
    Route::post('/subJenis/add', [subJenisController::class, 'store'])->name('addSub');
    Route::post('/subJenis/update', [subJenisController::class, 'update'])->name('editSub');

    //Event Surat
    Route::get('/event', [statusController::class, 'index']);
    Route::get('/event/getStatus/{id}', [statusController::class, 'getStatus']);
    Route::get('/event/remove/{id?}', [statusController::class, 'delete'])->name('removeEvent');
    Route::post('/event/add', [statusController::class, 'store'])->name('addEvent');
    Route::post('/event/update', [statusController::class, 'update'])->name('editEvent');

    //Pegawai 
    Route::get('/pegawai', [pegawaiController::class, 'index']);
    Route::get('/pegawai/getPegawai/{id}', [pegawaiController::class, 'getPegawai']);
    Route::get('/pegawai/remove/{id?}', [pegawaiController::class, 'delete'])->name('removePegawai');
    Route::post('/pegawai/add', [pegawaiController::class, 'store'])->name('addPegawai');
    Route::post('/pegawai/update', [pegawaiController::class, 'update'])->name('editPegawai');
    Route::post('/file-import', [pegawaiController::class, 'fileImport'])->name('file-import');

    //Aktivitas
    Route::get('/aktivitas', [aktivitasController::class, 'index']);
    Route::get('/aktivitas/getAktivitas/{id}', [aktivitasController::class, 'getAktivitas']);
    Route::get('/aktivitas/remove/{id?}', [aktivitasController::class, 'delete'])->name('removeAktivitas');
    Route::post('/aktivitas/add', [aktivitasController::class, 'store'])->name('addAktivitas');
    Route::post('/aktivitas/update', [aktivitasController::class, 'update'])->name('updateAktivitas');
});
