<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class MSurat extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('m_surat', function (Blueprint $table) {
            $table->id('id_surat');
            $table->unsignedBigInteger('id_jenis');
            $table->unsignedBigInteger('id_sub_jenis');
            $table->string('nomor_surat');
            $table->string('judul_surat');
            $table->date('tgl_terbit');
            $table->date('tgl_berlaku');
            $table->date('tgl_berakhir');
            $table->boolean('is_active');
            $table->boolean('is_public');
            $table->string('path_surat');
            $table->softDeletes();
            $table->foreign('id_jenis')->references('id_jenis')->on('m_jenis_surat');
            $table->foreign('id_sub_jenis')->references('id_sub_jenis')->on('m_sub_jenis_surat');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
